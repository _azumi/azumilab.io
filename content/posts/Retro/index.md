---
title: "TryHackMe - Retro Writeup"
summary: "Writeup of Retro room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "wordpress", "windows"]
showSummary: true
date: 2022-12-13
draft: false
---

## Introduction
Firstly, we scan the machine with `nmap`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nmap -A 10.10.80.190
Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-13 08:42 CET
Nmap scan report for 10.10.80.190
Host is up (0.036s latency).
Not shown: 998 filtered tcp ports (no-response)
PORT     STATE SERVICE       VERSION
80/tcp   open  http          Microsoft IIS httpd 10.0
|_http-title: IIS Windows Server
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
3389/tcp open  ms-wbt-server Microsoft Terminal Services
| rdp-ntlm-info: 
|   Target_Name: RETROWEB
|   NetBIOS_Domain_Name: RETROWEB
|   NetBIOS_Computer_Name: RETROWEB
|   DNS_Domain_Name: RetroWeb
|   DNS_Computer_Name: RetroWeb
|   Product_Version: 10.0.14393
|_  System_Time: 2022-12-13T07:46:19+00:00
|_ssl-date: 2022-12-13T07:46:24+00:00; 0s from scanner time.
| ssl-cert: Subject: commonName=RetroWeb
| Not valid before: 2022-12-12T07:41:08
|_Not valid after:  2023-06-13T07:41:08
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit
Nmap done: 1 IP address (1 host up) scanned in 246.02 seconds
```

As you can see, there is an **IIS** (Internet Information Services).
![Screenshot_2022-12-13_11-39-22.png](img/Screenshot_2022-12-13_11-39-22.png)

So, we use `gobuster` to enumerate directories:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ gobuster dir --url http://10.10.147.38 -b 404,400,500,503 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt --no-error
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.147.38
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   500,503,404,400
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/12/13 11:37:19 Starting gobuster in directory enumeration mode
===============================================================
/retro                (Status: 301) [Size: 149] [--> http://10.10.147.38/retro/]
Progress: 9247 / 220561 (4.19%)^C
[!] Keyboard interrupt detected, terminating.
===============================================================
2022/12/13 11:40:23 Finished
===============================================================
```

We found the `/retro` directory:
![Screenshot_2022-12-13_11-42-08.png](img/Screenshot_2022-12-13_11-42-08.png)

After that, we can enumerate subdirectories. 
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ gobuster dir --url http://10.10.147.38/retro -b 404,400,500,503 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt --no-error
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.147.38/retro
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404,400,500,503
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/12/13 11:40:52 Starting gobuster in directory enumeration mode
===============================================================
/wp-content           (Status: 301) [Size: 160] [--> http://10.10.147.38/retro/wp-content/]
/wp-includes          (Status: 301) [Size: 161] [--> http://10.10.147.38/retro/wp-includes/]                                                                                          
/wp-admin             (Status: 301) [Size: 158] [--> http://10.10.147.38/retro/wp-admin/]
Progress: 14004 / 220561 (6.35%)^C
[!] Keyboard interrupt detected, terminating.
===============================================================
2022/12/13 11:43:13 Finished
===============================================================
```

We can see that there is a Wordpress CMS. We'll further enumerate with a wordpress wordlist:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ gobuster dir --url http://10.10.147.38/retro -b 404,400,500,503 -w /usr/share/wordlists/SecLists/Discovery/Web-Content/CMS/wordpress.fuzz.txt --no-error
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.147.38/retro
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/SecLists/Discovery/Web-Content/CMS/wordpress.fuzz.txt
[+] Negative Status codes:   404,400,500,503
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/12/13 11:53:51 Starting gobuster in directory enumeration mode
===============================================================
/wp-admin/admin-footer.php (Status: 200) [Size: 2]
/readme.html          (Status: 200) [Size: 7447]
/license.txt          (Status: 200) [Size: 19935]
[...]
```

Then let's scan it in order to enumerate users and find vulnerabilities:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ wpscan -e vp,u --url http://10.10.147.38/retro/
_______________________________________________________________
         __          _______   _____
         \ \        / /  __ \ / ____|
          \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
           \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
            \  /\  /  | |     ____) | (__| (_| | | | |
             \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.8.22
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
_______________________________________________________________

[+] URL: http://10.10.147.38/retro/ [10.10.147.38]
[+] Started: Tue Dec 13 11:49:10 2022

Interesting Finding(s):

[+] Headers
 | Interesting Entries:
 |  - Server: Microsoft-IIS/10.0
 |  - X-Powered-By: PHP/7.1.29
 | Found By: Headers (Passive Detection)
 | Confidence: 100%

[+] XML-RPC seems to be enabled: http://10.10.147.38/retro/xmlrpc.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%
 | References:
 |  - http://codex.wordpress.org/XML-RPC_Pingback_API
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_ghost_scanner/
 |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/wordpress_xmlrpc_dos/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_xmlrpc_login/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_pingback_access/

[+] WordPress readme found: http://10.10.147.38/retro/readme.html
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] The external WP-Cron seems to be enabled: http://10.10.147.38/retro/wp-cron.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 60%
 | References:
 |  - https://www.iplocation.net/defend-wordpress-from-ddos
 |  - https://github.com/wpscanteam/wpscan/issues/1299

[+] WordPress version 5.2.1 identified (Insecure, released on 2019-05-21).
 | Found By: Rss Generator (Passive Detection)
 |  - http://10.10.147.38/retro/index.php/feed/, <generator>https://wordpress.org/?v=5.2.1</generator>
 |  - http://10.10.147.38/retro/index.php/comments/feed/, <generator>https://wordpress.org/?v=5.2.1</generator>

[+] WordPress theme in use: 90s-retro
 | Location: http://10.10.147.38/retro/wp-content/themes/90s-retro/
 | Latest Version: 1.4.10 (up to date)
 | Last Updated: 2019-04-15T00:00:00.000Z
 | Readme: http://10.10.147.38/retro/wp-content/themes/90s-retro/readme.txt
 | Style URL: http://10.10.147.38/retro/wp-content/themes/90s-retro/style.css?ver=5.2.1
 | Style Name: 90s Retro
 | Style URI: https://organicthemes.com/retro-theme/
 | Description: Have you ever wished your WordPress blog looked like an old Geocities site from the 90s!? Probably n...
 | Author: Organic Themes
 | Author URI: https://organicthemes.com
 |
 | Found By: Css Style In Homepage (Passive Detection)
 |
 | Version: 1.4.10 (80% confidence)
 | Found By: Style (Passive Detection)
 |  - http://10.10.147.38/retro/wp-content/themes/90s-retro/style.css?ver=5.2.1, Match: 'Version: 1.4.10'

[+] Enumerating Vulnerable Plugins (via Passive Methods)

[i] No plugins Found.

[+] Enumerating Users (via Passive and Aggressive Methods)
 Brute Forcing Author IDs - Time: 00:00:04 <=============> (10 / 10) 100.00% Time: 00:00:04

[i] User(s) Identified:

[+] wade
 | Found By: Author Posts - Author Pattern (Passive Detection)
 | Confirmed By:
 |  Wp Json Api (Aggressive Detection)
 |   - http://10.10.147.38/retro/index.php/wp-json/wp/v2/users/?per_page=100&page=1
 |  Author Id Brute Forcing - Author Pattern (Aggressive Detection)
 |  Login Error Messages (Aggressive Detection)

[+] Wade
 | Found By: Rss Generator (Passive Detection)
 | Confirmed By: Login Error Messages (Aggressive Detection)

[!] No WPScan API Token given, as a result vulnerability data has not been output.
[!] You can get a free API token with 25 daily requests by registering at https://wpscan.com/register

[+] Finished: Tue Dec 13 11:49:24 2022
[+] Requests Done: 24
[+] Cached Requests: 36
[+] Data Sent: 6.468 KB
[+] Data Received: 38.619 KB
[+] Memory used: 239.137 MB
[+] Elapsed time: 00:00:14
```

We found `wade` user. We'll try to bruteforce the login page. 
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ wpscan -v --usernames wade -P /usr/share/wordlists/rockyou.txt --url http://10.10.147.38/retro/wp-login.php
_______________________________________________________________
         __          _______   _____
         \ \        / /  __ \ / ____|
          \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
           \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
            \  /\  /  | |     ____) | (__| (_| | | | |
             \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.8.22
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
_______________________________________________________________

[+] URL: http://10.10.147.38/retro/wp-login.php/ [10.10.147.38]
[+] Started: Tue Dec 13 12:18:07 2022

Interesting Finding(s):

[+] Headers
 | Interesting Entries:
 |  - Server: Microsoft-IIS/10.0
 |  - X-Powered-By: PHP/7.1.29
 | Found By: Headers (Passive Detection)
 | Confidence: 100%
```

Unfortunately, that was unsuccessful.

By going on the `Ready Player One` page, we found a note leaved by `wade`. Maybe a password.
![7b3ea4b67be805c001b9c2d87b7ad3d6.png](img/7b3ea4b67be805c001b9c2d87b7ad3d6.png)

## Exploitation
We can authenticate on Wordpress as `wade` user with these credentials. Let's try through `rdp` if the credentials are the same:
```bash
──(parallels㉿kali-linux-2022-2)-[~]
└─$ xfreerdp /v:10.10.43.41 /u:"Wade" /p:"parzival" /drive:.,kali-share +clipboard
[13:50:55:974] [332784:332785] [WARN][com.freerdp.crypto] - Certificate verification failure 'self-signed certificate (18)' at stack position 0
[13:50:55:974] [332784:332785] [WARN][com.freerdp.crypto] - CN = RetroWeb
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] - @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] - @           WARNING: CERTIFICATE NAME MISMATCH!           @
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] - @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] - The hostname used for this connection (10.10.43.41:3389) 
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] - does not match the name given in the certificate:
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] - Common Name (CN):
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] -    RetroWeb
[13:50:55:974] [332784:332785] [ERROR][com.freerdp.crypto] - A valid certificate for the wrong name should NOT be trusted!
Certificate details for 10.10.43.41:3389 (RDP-Server):
        Common Name: RetroWeb
        Subject:     CN = RetroWeb
        Issuer:      CN = RetroWeb
        Thumbprint:  61:79:48:54:04:5e:e9:32:83:69:8d:be:c8:5e:d1:aa:6c:74:11:36:42:2b:c8:94:75:4c:10:6f:14:c3:7e:f5
The above X.509 certificate could not be verified, possibly because you do not have
the CA certificate in your certificate store, or the certificate has expired.
Please look at the OpenSSL documentation on how to add a private CA to the store.
Do you trust the above certificate? (Y/T/N) Y
[13:51:00:516] [332784:332785] [INFO][com.freerdp.gdi] - Local framebuffer format  PIXEL_FORMAT_BGRX32
[13:51:00:516] [332784:332785] [INFO][com.freerdp.gdi] - Remote framebuffer format PIXEL_FORMAT_BGRA32
```

That's a success ! We got the first flag on `user.txt`.
![66bd0b819209cc47f5273950f910c23b.png](img/66bd0b819209cc47f5273950f910c23b.png)

Then, we found on `Chrome` that a report on `CVE-2019-1388` is added to the bookmark.

This CVE exploit tend to abuse the UAC windows Certificate Dialog to execute the certificate issuer link as an NT Authority User and open a browser that is under NT Authority User. Then we can use that to prompt a shell as a NT Authority User.

We found  `hhupd` on the `Recycle bin` and we run it as `Administrator`:
![4ceee9e26f3f5bb196326c0ac55374fc.png](img/4ceee9e26f3f5bb196326c0ac55374fc.png)

We click on `Show more details` and `Show information about the publisher’s certificate`:
![0a28c354387289ee2f803005aed2186b.png](img/0a28c354387289ee2f803005aed2186b.png)

Then we click on the link on `Issued by`:
![8d7abc6b9d3699ea0f7992cb8e5d55f1.png](img/8d7abc6b9d3699ea0f7992cb8e5d55f1.png)

Finally, we click on `CTRL+S` in order to locate the `C:\Windows\System32\` and then run the `cmd` program:
![ff97ba0374adfdfa7f1d2b9431fc9dd4.png](img/ff97ba0374adfdfa7f1d2b9431fc9dd4.png) 

