---
title: "Retex OSCP 2023"
summary: "Retex of the Offensive Security Certified Professional Certification"
categories: ["retex"]
tags: ["oscp"]
showSummary: true
date: 2023-07-30
draft: false
---

## Introduction
In this article, I'm going to give you my feedback on the OSCP (Offensive Security Certified Professional). In my opinion, it's the **most popular certification** and the most sought-after by recruiters in the field of offensive security.

The **OSCP Course & Cert Exam Bundle** includes:
- The PEN-200 course and online lab
- One attempt for the proctored 24-hour 

## Financing
For French workers, there is a solution to help finance professional training. It's called the **CPF** (Compte Personnel de Formation). 

The principle is simple: if you have an employment contract, your account is credited with a certain amount each year. 

As an apprentice since 2018, I had over €2,000 and was therefore able to finance the **OSCP Course & Cert Exam Bundle**, which was offered in the training catalog. 

The course was offered by CERTyou, the European specialist in professional certification. I was reassured by the various reviews I could find on the internet about them, given that CPF scams are commonplace.

CERTyou simply ordered the bundle for me and then sent me the access details. They were very professional during our exchanges.

However, the Course & Cert Exam Bundle is priced at $1599 on the Offsec website, while CERTyou offers it at €2400. It's common to see inflated training prices in the CPF catalog.

In the end, I had to pay €220 out of pocket, which was still better than paying the ultimate price on the Offsec website.
![5f1c432c62650befaa5789bca7c51968.png](img/5f1c432c62650befaa5789bca7c51968.png)

## Background
To provide some background, I'm at the end of my apprenticeship as a cyber defense engineer. I worked as a DevSecOps engineer during my 3-year apprenticeship.

Prior to this certification, I had trained on several platforms:
- I have around 5000 pts on Root-Me
- Pwned 19 boxes on HackTheBox
- Finished Dante Pro Lab on HackTheBox
- And completed TryHackMe's Junior Pentester and Offensive Security paths.

## Why this certification ?
As explained in the introduction, it's the **most popular certification** and the most sought-after by recruiters in the field of offensive security.

In my case, as my experience is not directly related to offensive security, the only evidence of my skills in this field were my achievements on training platforms.

By passing this certification, I obtained a way to attest to my skills and open doors to a career focused on offensive security.

## PEN-200–2023 Course
The best way to describe the course is to download the syllabus [here](https://www.offsec.com/courses/pen-200/download/syllabus).

From my point of view, the course is very complete and up to date. It is perfect for a beginner with basic knowledge in system administration and scripting.

In my case, I took a good part of the course to make sure I didn’t miss anything. But I didn’t find any real value over TryHackMe’s Learnings Paths.

In the end, I followed less than 50% of the course, including reading the text, videos and exercises.

## PEN-200–2023 Labs
There are two types of Challenge Labs. The first three are called **scenarios**. Each scenario consists of a set of networked machines and a short background story that puts those machines in context. Your goal is to obtain access to a Domain Administrator account on an Active Directory domain, and compromise as many machines on the network as possible.

Some machines will be **dependent on information, credentials, or capabilities** that will be found on other machines. And some machines may not even be (intentionally) exploitable until after the Domain Controller is compromised.

The second type of Challenge Lab consists of an **OSCP-like experience**. They are each composed of six OSCP machines. The intention of these Challenges is to provide a **mock-exam** experience that closely reflects a similar level of difficulty to that of the actual OSCP exam.

Personally, I've only done the OSCP-like labs. I felt comfortable enough after finishing them.

## Bonus Points
I didn't get the 10 bonus points because I hadn't finished enough lab machines and hadn't completed 80% of the exercises for each module.

## Exam
I scheduled my exam about 3 weeks in advance, and during this time I hardly practiced at all, just read and organized my notes.

I chosed to start the exam at 11:00 AM to take the time to wake up and take a walk outside.

As a reminder, **70 points are required for certification** (provided the report is coherent). This meant I had to complete the AD set (40 points) and get at least half the flags from the three standalone machines (10 points per flag).

**The D-DAY**

Frankly, I started stressing about 1 hour before the exam. But once I'd logged on to the portal, I concentrated on carrying out the various checks with the proctor, and then I was completely focused on my objective.

Here's the timeline of my exam:
- **12:35 PM** - *0 points*: For over an hour, I was stuck on the foothold of the first machine in the AD set. I finally managed to gain user access and elevated my privileges in the aftermath.
- **1:15 PM** - *0 points*: Lateral movement to the second machine in the AD set and obtain administrator privileges.
- **1:30 PM** - *40 points*: Lateral movement to the domain controller and obtain administrator privileges. I then took an hour's break for lunch.
- **3:20 PM** - *50 points*: User access on the last standalone machine. I then got stuck for 3 hours, and I must admit I was a bit stressed at the time.
- **6:15 PM** - *60 points*: User access on the second standalone machine. I was stuck for another two hours before taking a break of over an hour to walk around and eat.
- **9:40 PM** - *70 points*: Privilege escalation on the last standalone machine.
- **10:15 PM** - *80 points*: User access on the first standalone machine. Then I started checking my notes and screenshots before going to sleep.
- I couldn't sleep so I watched a series until 4 a.m.
-  **4:40 AM** - *90 points*: Privilege escalation on the second standalone machine.
-  **6:13 AM** - *100 points*: Privilege escalation on the first standalone machine. I then took 45 minutes to check my notes and screenshots.

## Report
I started working on the report around 3pm the next day and submitted it at midnight.

I used the [template](https://github.com/noraj/OSCP-Exam-Report-Template-Markdown) created by Noraj. This is a markdown template designed to be converted to LaTeX with pandoc.

## Results
I finally received my certification 6 days later !

![7be8781b482457b5a8fe1909e66eb658.png](img/7be8781b482457b5a8fe1909e66eb658.png)

## Conclusion
I didn't find the exam all that technically challenging, especially the Active Directory part. In the end, the hardest part was managing stress and keeping a cool head.

Taking breaks is really a game changer, every time I took a break I got a flag within minutes.

All in all, it was a great experience to take this exam, and I came away reassured about my skills and ready for the next challenge!

## Resources
- https://help.offsec.com/hc/en-us/articles/360040165632-OSCP-Exam-Guide
- https://johnjhacking.com/blog/oscp-reborn-2023/
- https://www.youtube.com/watch?v=xyqJmJiLbik
- https://gabb4r.gitbook.io/oscp-notes/
- https://liodeus.github.io/2020/09/18/OSCP-personal-cheatsheet.html